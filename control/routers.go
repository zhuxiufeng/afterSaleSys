package control

import (
	"asystem/utils"
	"github.com/gin-gonic/gin"
)

var (
	R *gin.Engine
)

func Routers() {
	// 客户端路由
	R.GET("/login", cSign)
	R.GET("/cLogout", cLogout)
	R.POST("/login", cSign)
	R.GET("/registry", registryUser)
	R.POST("/registry", registryUser)
	cr := R.Group("/")
	{
		cr.GET("/", clientMiddleware, index)
		cr.GET("/getAllWuLiu", getAllWuLiu)
		cr.GET("/getUserInfo", getUserInfo)
		cr.GET("/getAllSale", getSaleDat)
		cr.GET("/getMaterType", getMaterType)
		cr.GET("/getMaterDat", getMaterDat)
		cr.GET("/getProvinceDat", getProvinceDat)
		cr.GET("/getCityDat", getCityDat)
		cr.GET("/getUserAddress", getUserAddress)
		cr.POST("/insertAddrToSql", insertAddrToSql)
		cr.POST("/addWorkDat", addWorkDat)
		cr.GET("/getWorkOrder", getWorkOrder)
		cr.GET("/getRepairWork", getRepairWorkDat)
		cr.GET("/searchWorkOrder", getSearchWorkDat)
		cr.GET("/verify", codeGetMaterClientDat)
		cr.POST("/verify/createOrder", verifyCreateWorkOrder)
		// 获取工单信息
		cr.GET("/getWorkOrderDat", getWorkOrderDat)
		// 阿里支付相关URL
		cr.GET("/alipay", aliPay)
		cr.GET("/pay_success", aliPayCallback)
		cr.POST("/alipay_notify", aliPayNotify)
	}

	// 后台管理路由
	R.GET("/sign", loginHandler)
	R.POST("/sign", loginHandler)
	R.GET("/quit", logOutHandler)
	gr := R.Group("/admin")
	{
		gr.GET("/", myMiddleware, adminIndex)
		gr.GET("/search", codeSearchDat)
		gr.GET("/getAllWork", getAllWorkData)
		// 用户管理 router
		gr.GET("/getAllUser", getAllUsers)
		gr.GET("/userType", getAllUserType)
		gr.POST("/add_user", addUser)
		gr.PUT("/change_user", changeUserData)
		gr.POST("/del_user", delUserData)
		// 客户管理 router
		gr.GET("/getAllClient", getAllClientData)
		gr.POST("/add_client", addClientData)
		gr.GET("/getSalePeople", getSalePeople)
		gr.PUT("/change_client", changeClientDat)
		gr.POST("/del_client", delClientData)
		// 物流管理 router
		gr.GET("/getAllWuLiu", getAllWuLiuData)
		gr.POST("/addWuLiu", addWuLiuData)
		gr.POST("/del_wuLiu", delWuLiuData)
		gr.PUT("/changeWuLiu", changeWuLiuData)
		// BomName 管理
		gr.GET("/getBomName", getAllBomName)
		gr.POST("/addBomName", addBomNameDat)
		gr.POST("/del_bomName", delBomName)
		gr.PUT("/changeBomName", updateBomName)
		// 物料管理
		gr.GET("/getAllMater", getMaterAll)
		gr.GET("/getAllMaterType", getAllMaterType)
		gr.POST("/add_mater", addMater)
		gr.PUT("/change_mater", changeMater)
		gr.POST("/del_mater", deleteMater)
		// 打印机管理
		gr.GET("/getAllPrinter", getPrinterDat)
		gr.POST("/add_printer", addPrinter)
		gr.PUT("/change_print", UpdatePrinter)
		gr.POST("/del_printer", delPrinterDat)
		// 产品组装功能
		gr.GET("/getAllMaterIdName", getAllMaterIdName)
		gr.GET("/getAllBomName", getAllBomName)
		gr.GET("/getProductInfo", getMaterModel)
		gr.GET("/getMaterNotInOneAndTwo", getMaterNotOneAndTwo)
		gr.POST("/goods_up", saveBomToSql)
		// 条码打印功能
		gr.GET("/PrintGetAllMater", getAllIsBomMater)
		gr.GET("/PrintGetAllClient", getAllClientData)
		gr.GET("/PrintGetAllUser", getAllSaleAndBusiness)
		gr.GET("/PrintGetAllVersion", getAllBomName)
		gr.GET("/PrintGetAllPrinter", getPrinterDat)
		gr.POST("/print_code", printCodeControl)
		// 待办事项管理
		gr.GET("/getUpcoming", getUpcomingHandler)
		gr.GET("/getUpcomingDetail", getUpcomingDetail)
		gr.PUT("/changeWorkProcessStat", changeWorkProcessStat)
		// 条码绑定
		gr.GET("/getAllVersion", getAllBomName)
		gr.GET("/getSonCodeDosage", getSonCodeDosage)
		gr.POST("/bindCode", bindCodeInfo)
		// 业务工单管理
		gr.GET("/getAllBusWorkOrder", getAllBusWorkOrder)
		gr.GET("/getWorkData", getBusWorkOrder)
		gr.GET("/getOutFirm", getOutFirm)
		gr.GET("/getUsers", getWxUsers)
		gr.PUT("/changeWork", changeWork)
		gr.PUT("/changeWorkState", changeWorkState)
		gr.PUT("/sendWorkState", sendWorkState)
		gr.POST("/changeWorkAndIstPay", changeWorkStateAndIstPay)
		// 扫描二维码出库功能路由
		gr.GET("/scanQrCode", myMiddleware, scanQrCode)
		gr.POST("/scanQrCode", myMiddleware, scanQrCode)
		gr.GET("/querySonCodeNum", myMiddleware, querySonCodeNum)
		// 文件上传功能
		gr.POST("/uploadFile", UploadFileToSql)
		// 重新打印单个条码路由
		gr.GET("/rePrintCode", rePrintCode)
		// 搜索物料功能
		gr.GET("/searchMater", searchMaterInfo)
		// 工单搜索功能URL
		gr.POST("/searchWorkInfo", searchWorkInfo)
		gr.GET("/warehouse", myMiddleware, warehouse)
		gr.POST("/warehouse", myMiddleware, warehouse)
		// 轮播图URL
		gr.GET("/add_new_product", myMiddleware, addNewProduct)
		gr.POST("/add_new_product", myMiddleware, addNewProduct)
	}
}

// myMiddleware 后台管理中间件
func myMiddleware(c *gin.Context) {
	se := utils.InitSession(c, "admin")
	data := se.GetSession(se.SesKey.SerSessKey)
	if data == "" {
		c.Request.URL.Path = "/sign"
		R.HandleContext(c)
		return
	}
	c.Next()
}

// clientMiddleware 客户页面中间件
func clientMiddleware(c *gin.Context) {
	se := utils.InitSession(c, "client")
	get := se.GetSession(se.SesKey.CliSessKey)
	if get == "" {
		c.Request.URL.Path = "/login"
		R.HandleContext(c)
		return
	}
	c.Next()
}
