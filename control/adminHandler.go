package control

import (
	"asystem/cfg"
	"asystem/db"
	"asystem/utils"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"math/rand"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

const (
	LVL1 = iota + 1
	LVL2
	LVL3
	HostType  = 0
	CodeHeadZ = "WM"
	CodeHeadD = "DP"
)

type dats struct {
	ProductId      string   `json:"productId"`    // 成品Id
	ProductModel   string   `json:"productModel"` // 成品型号
	BomNameId      string   `json:"bomName"`      // Bom 名称Id
	ChildrenIds    []string `json:"childrenIds"`  // 所有子件Id
	ChildrenDosage []string `json:"childrenNums"` // 所有子件用量
}

// 后台管理首页
func adminIndex(c *gin.Context) {
	// 获取用户类型，用于前端做判断根据不同用户显示不同菜单
	se := utils.InitSession(c, "admin")
	data := se.GetSession(se.SesKey.SerSessKey)
	seMap, err := se.SliceUint8ToMap(data)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.HTML(200, "template/serIndex.html", gin.H{"title": "链力电子", "tid": seMap.Tid})
}

func getAllWorkData(c *gin.Context) {
	c.String(200, "sdknskdvbskdbvsdbh")
}

// 获取所有用户数据
func getAllUsers(c *gin.Context) {
	users, err := db.GetAllUsers()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": users, "errMsg": ""})
}

// getAllUserType 获取所有用户类型数据
func getAllUserType(c *gin.Context) {
	userTypeList, err := db.GetAllUserType()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": userTypeList, "errMsg": ""})
}

// addUser 添加用户操作
func addUser(c *gin.Context) {
	account := c.PostForm("login_account")
	userName := c.PostForm("userName")
	passwd := c.PostForm("user_passwd")
	userTypeId := c.PostForm("user_type_id")
	tid, err := strconv.Atoi(userTypeId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": "提交的用户类型数据格式不正确，请检查重填后再试"})
		return
	}
	if account == "" || userName == "" || passwd == "" || userTypeId == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "用户信息不允许留空，请检查重填后再试"})
		return
	}
	hasPasswd := utils.HasPasswd(passwd)
	users := &db.Users{
		Account:  account,
		UserName: userName,
		Passwd:   hasPasswd,
		TId:      tid,
	}
	if err := db.InsertUserDataToSql(users); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "添加成功", "errMsg": ""})
}

// 修改用户操作
func changeUserData(c *gin.Context) {
	var user *db.Users
	uId := c.PostForm("userId")
	uid, err := strconv.Atoi(uId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的用户Id 数据类型错误，错误代码：%v", err)})
		return
	}
	account := c.PostForm("userAccount")
	username := c.PostForm("userName")
	pwd := c.PostForm("userPwd")
	typeId := c.PostForm("userTypeId")
	tid, err := strconv.Atoi(typeId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的用户类型 Id 数据类型错误，错误代码：%v", err)})
		return
	}
	if pwd != "" {
		Hpwd := utils.HasPasswd(pwd)
		user = &db.Users{
			Uid:      uid,
			Account:  account,
			UserName: username,
			Passwd:   Hpwd,
			TId:      tid,
		}
	} else {
		user = &db.Users{
			Uid:      uid,
			Account:  account,
			UserName: username,
			Passwd:   "",
			TId:      tid,
		}
	}
	if err = db.ChangeUserData(user); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// 删除用户操作
func delUserData(c *gin.Context) {
	// todo 删除用户前先从 session 中获取用户类型数据，如果用户类型不等于 1 则提示没权限
	if err := utils.CheckPermission(c); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	uid := c.PostForm("userId")
	userId, err := strconv.Atoi(uid)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的用户 Id 数据类型错误，错误代码: %v", err)})
		return
	}
	account := c.PostForm("account")
	username := c.PostForm("username")
	user := &db.Users{
		Uid:      userId,
		Account:  account,
		UserName: username,
	}
	if err = db.DelUserData(user); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// getAllClientData 获取所有客户信息
func getAllClientData(c *gin.Context) {
	clientList, err := db.GetAllClients()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": clientList, "errMsg": ""})
}

// /getSalePeople 添加/修改客户时获取销售业务人员信息
func getSalePeople(c *gin.Context) {
	sales, err := db.GetSalePeople()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": sales, "errMsg": ""})
}

// addClientData 添加客户信息
func addClientData(c *gin.Context) {
	clientName := c.PostForm("client_name")
	clientPhone := c.PostForm("client_phone")
	if err := utils.CheckPhone(clientPhone); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	clientAddr := c.PostForm("client_addr")
	if clientName == "" || clientPhone == "" || clientAddr == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("客户信息不允许留空!!!")})
		return
	}
	client := &db.Client{
		ClientName: clientName,
		Phone:      clientPhone,
		Addr:       &clientAddr,
	}
	fmt.Println(*(client.Addr), "<======================================================")
	if err := db.InsertClientDatToSql(client); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	// 插入数据地址到客户地址表
	if err := db.AddClientInsertClientAddrToSql(client); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// changeClientDat 修改客户数据
func changeClientDat(c *gin.Context) {
	cliId := c.PostForm("client_id")
	cliName := c.PostForm("client_name")
	cliPhone := c.PostForm("client_phone")
	cliAddr := c.PostForm("client_addr")
	if cliId == "" || cliName == "" || cliPhone == "" || cliAddr == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("更新失败，更新的客户信息不允许留空！")})
		return
	}
	cid, err := strconv.Atoi(cliId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("更新失败，提交的客户数据格式有误：%v", err)})
		return
	}
	client := &db.Client{
		Cid:        cid,
		ClientName: cliName,
		Phone:      cliPhone,
		Addr:       &cliAddr,
	}
	if err = db.UpdateClientDat(client); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}

	addr, err := db.GetClientAddr(client)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	// 修改用户数据时如果客户地址为空则插入新地址，如果有则更新
	if addr != "" {
		if err = db.AddClientUpdateClientAddrToSql(client); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
	} else {
		if err = db.AddClientInsertClientAddrToSql(client); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// delClientData 删除客户数据
func delClientData(c *gin.Context) {
	if err := utils.CheckPermission(c); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	cliId := c.PostForm("client_id")
	cid, err := strconv.Atoi(cliId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("更新失败，提交的客户数据格式有误：%v", err)})
		return
	}
	cliName := c.PostForm("client_name")
	cliPhone := c.PostForm("client_phone")
	cliAddr := c.PostForm("client_addr")
	client := &db.Client{
		Cid:        cid,
		ClientName: cliName,
		Phone:      cliPhone,
		Addr:       &cliAddr,
	}
	if err = db.DelClientData(client); err != nil {
		c.JSON(200, gin.H{
			"success": "",
			"errMsg":  fmt.Sprintf("%v", err),
		})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// getAllWuLiuData 获取所有物流数据
func getAllWuLiuData(c *gin.Context) {
	logList, err := db.GetLogTicsDat()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": logList, "errMsg": ""})
}

// addWuLiuData 添加物流数据
func addWuLiuData(c *gin.Context) {
	logName := c.PostForm("wuLiuName")
	buttPeo := c.PostForm("buttPeople")
	phone := c.PostForm("buttPhone")
	if logName == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("添加的物流公司名称不允许留空!!!")})
		return
	}
	log := &db.Logistics{
		Lname: logName,
		Butt:  buttPeo,
		Phone: phone,
	}
	if err := db.AddLogTicToSql(log); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// changeWuLiuData 修改物流数据
func changeWuLiuData(c *gin.Context) {
	logId := c.PostForm("wuLiuId")
	logName := c.PostForm("wuLiuName")
	butt := c.PostForm("buttPeople")
	bPhone := c.PostForm("buttPhone")
	if err := utils.CheckPhone(bPhone); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	if logId == "" || logName == "" || butt == "" || bPhone == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "修改的物流信息内容填写不完整!"})
		return
	}
	lid, err := strconv.Atoi(logId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的物流信息数据格式有误，检查后重试：%v", err)})
		return
	}
	logtic := &db.Model{Logistics: &db.Logistics{
		Lid:   lid,
		Lname: logName,
		Butt:  butt,
		Phone: bPhone,
	}}
	if err = db.UpLodTicData(logtic); err != nil {
		fmt.Println(err, "***********************************************************************")
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// delWuLiuData 删除物流信息
func delWuLiuData(c *gin.Context) {
	// 删除物流前检查操作权限
	if err := utils.CheckPermission(c); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	logId := c.PostForm("wuLiuId")
	lid, err := strconv.Atoi(logId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的物流信息数据格式有误，检查后重试：%v", err)})
		return
	}
	logName := c.PostForm("wuLiuName")
	buttPeo := c.PostForm("buttPeople")
	phone := c.PostForm("buttPhone")
	mod := &db.Model{Logistics: &db.Logistics{
		Lid:   lid,
		Lname: logName,
		Butt:  buttPeo,
		Phone: phone,
	}}
	if err = db.DelLogTicData(mod); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// getAllBomName 获取所有Bom名称
func getAllBomName(c *gin.Context) {
	bNames, err := db.GetAllBomName()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": bNames, "errMsg": ""})
}

// addBomNameDat 添加bom名称
func addBomNameDat(c *gin.Context) {
	name := c.PostForm("name")
	if name == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "添加失败，您还没有输入任何内容！"})
		return
	}
	bName := &db.Model{BomNameTab: &db.BomNameTab{
		BName: name,
	}}
	if err := db.AddBomNameDat(bName); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// updateBomName 更新bom名称
func updateBomName(c *gin.Context) {
	bid, err := strconv.Atoi(c.PostForm("bid"))
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的数据格式有误，检查后重试：%v", err)})
		return
	}
	name := c.PostForm("name")
	if name == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "您还没有输入任何数据哦~！"})
		return
	}
	mod := &db.Model{BomNameTab: &db.BomNameTab{
		BnId:  bid,
		BName: name,
	}}
	if err := db.UpdateBomName(mod); err != nil {
		c.JSON(200, gin.H{"success": "", "errNsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// delBomName 删除bom名称
func delBomName(c *gin.Context) {
	if err := utils.CheckPermission(c); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	bmId, err := strconv.Atoi(c.PostForm("bmId"))
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的数据格式有误，检查后重试：%v", err)})
		return
	}
	bName := c.PostForm("bmName")
	if bName == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "您还没有输入任何数据哦~！"})
		return
	}
	modObj := &db.Model{BomNameTab: &db.BomNameTab{
		BnId:  bmId,
		BName: bName,
	}}
	if err := db.DeleteBomName(modObj); err != nil {
		c.JSON(200, gin.H{"success": "", "errNsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// getMaterALl 获取所有物料数据
func getMaterAll(c *gin.Context) {
	matres, err := db.GetAllMater()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": matres, "errMsg": ""})
}

func getAllMaterType(c *gin.Context) {
	materTypes, err := db.GetALlMaterType()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": materTypes, "errMsg": ""})
}

// addMater 添加物料
func addMater(c *gin.Context) {
	materName := c.PostForm("mater_name")
	brand := c.PostForm("brand")
	materModel := c.PostForm("mater_model")
	if materModel == "" || brand == "" || materName == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "添加的物料信息不允许留空~！"})
		return
	}
	mtId, err := strconv.Atoi(c.PostForm("mTypeId"))
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的数据格式有误，检查后重试：%v", err)})
		return
	}
	model := &db.Model{Mater: &db.Mater{
		Mname:   materName,
		Brand:   brand,
		Model:   materModel,
		MTypeId: mtId,
	}}
	if err := db.AddMaterDat(model); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// changeMater 修改物料数据控制器
func changeMater(c *gin.Context) {
	materId := c.PostForm("mater_id")
	mid, err := strconv.Atoi(materId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的数据格式有误，检查后重试：%v", err)})
		return
	}
	mtId, err := strconv.Atoi(c.PostForm("mTypeId"))
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的数据格式有误，检查后重试：%v", err)})
		return
	}
	magetName := c.PostForm("mater_name")
	brand := c.PostForm("mater_brand")
	mModel := c.PostForm("mater_model")
	if materId == "" || magetName == "" || brand == "" || mModel == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "修改的物料信息内容填写不完整!"})
		return
	}
	model := &db.Model{Mater: &db.Mater{
		Mid:     mid,
		Mname:   magetName,
		Brand:   brand,
		Model:   mModel,
		MTypeId: mtId,
	}}
	if err = db.UpdateMater(model); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// deleteMater 删除物料数据控制函数
func deleteMater(c *gin.Context) {
	if err := utils.CheckPermission(c); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	materId := c.PostForm("materId")
	mid, err := strconv.Atoi(materId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的数据格式有误，检查后重试：%v", err)})
		return
	}
	mName := c.PostForm("materName")
	mBrand := c.PostForm("materBrand")
	mModel := c.PostForm("materModel")
	if materId == "" || mName == "" || mBrand == "" || mModel == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "删除的物料信息内容不允许为空!"})
		return
	}
	model := &db.Model{Mater: &db.Mater{
		Mid:   mid,
		Mname: mName,
		Brand: mBrand,
		Model: mModel,
	}}
	if err = db.DelMaterDat(model); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// getPrinterDat 获取打印机数据控制函数
func getPrinterDat(c *gin.Context) {
	printers, err := db.PrinterSqlFunc("get", nil)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": printers, "errMsg": ""})
}

// addPrinter 添加打印机数据
func addPrinter(c *gin.Context) {
	pName := c.PostForm("print_name")
	pAddr := c.PostForm("print_addr")
	btAddr := c.PostForm("bt_server_addr")
	if pAddr == "" || pName == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "添加的打印机信息内容不允许留空!"})
		return
	}
	dats := &db.Model{Printer: &db.Printer{
		Pname:     pName,
		PNetAddr:  pAddr,
		BTSerAddr: btAddr,
	}}
	if _, err := db.PrinterSqlFunc("insert", dats); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("添加的打印机失败，错误代码：%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// UpdatePrinter 更新打印机数据控住函数
func UpdatePrinter(c *gin.Context) {
	pid, err := strconv.Atoi(c.PostForm("print_id"))
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的数据格式有误，检查后重试：%v", err)})
		return
	}
	pname := c.PostForm("print_name")
	paddr := c.PostForm("print_addr")
	bAddr := c.PostForm("bt_server")
	if pname == "" || paddr == "" || bAddr == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "修改的打印机信息内容填写不完整或有内容未填写!"})
		return
	}
	dats := &db.Model{Printer: &db.Printer{
		Pid:       pid,
		Pname:     pname,
		PNetAddr:  paddr,
		BTSerAddr: bAddr,
	}}
	if _, err = db.PrinterSqlFunc("update", dats); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("更新失败，错误代码：%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// delPrinterDat 删除打印机数据控制函数
func delPrinterDat(c *gin.Context) {
	pid, err := strconv.Atoi(c.PostForm("printId"))
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的数据格式有误，检查后重试：%v", err)})
		return
	}
	pname := c.PostForm("printName")
	paddr := c.PostForm("printAddr")
	baddr := c.PostForm("BTServerAddr")
	if pname == "" || paddr == "" || baddr == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "删除的打印机内容不允许为空!"})
		return
	}
	dats := &db.Model{Printer: &db.Printer{
		Pid:       pid,
		Pname:     pname,
		PNetAddr:  paddr,
		BTSerAddr: baddr,
	}}
	if _, err = db.PrinterSqlFunc("del", dats); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("删除失败，错误代码：%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// getAllMaterIdName 产品组装获取所有为成名或半成品的物料名称
func getAllMaterIdName(c *gin.Context) {
	maters, err := db.GetAllMaterIdName()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": maters, "errMsg": ""})
}

// getMaterModel 产品组装根据下拉选择框获取产品型号
func getMaterModel(c *gin.Context) {
	pid, err := strconv.Atoi(c.Query("product_id"))
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("提交的数据格式有误，检查后重试：%v", err)})
		return
	}
	model, err := db.GetMaterModel(pid)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": model, "errMsg": ""})
}

// getMaterNotOneAndTwo 产品组装获取不包含成品和半成品的物料
func getMaterNotOneAndTwo(c *gin.Context) {
	var datMap map[string]interface{} = make(map[string]interface{}, 0)
	mates, err := db.GetMaterNotOneAndTwo()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	datMap["mates"] = mates
	//if len(mates) < 100 {
	//	datMap["length"] = math.Floor(float64(len(mates)) / 2)
	//} else if len(mates) < 20 {
	datMap["length"] = len(mates)
	//} else {
	//	datMap["length"] = math.Floor(float64(len(mates)) / 20)
	//}
	c.JSON(200, gin.H{"success": datMap, "errMsg": ""})
}

// saveBomToSql 产品组装保存组装BOM 到数据库 =================================================
func saveBomToSql(c *gin.Context) {
	// m_id 物料Id,fm_id父级Id,mater_lv层级,dosage用量,bom_name_id配置名称Id
	var dat *dats = &dats{}
	err := c.BindJSON(dat)
	datByte, err := json.Marshal(dat)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("添加失败，错误代码：%v", err)})
		return
	}
	err = json.Unmarshal(datByte, dat)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("添加失败，错误代码：%v", err)})
		return
	}
	fmt.Println(dat.ProductId, dat.ProductModel, dat.BomNameId, dat.ChildrenIds, dat.ChildrenDosage)
	mid, err := utils.StrToInt(dat.ProductId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	bmId, err := utils.StrToInt(dat.BomNameId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	// 保存成品 Id 到数据库
	model := &db.Model{MaterBom: &db.MaterBom{
		MId:       mid,
		MaterLv:   LVL1,
		BomNameId: bmId,
	}}
	if err = db.InsertDatToMaterBom(model); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	// 查询刚存入数据库的成品Id 在 bom 表中的编号用来作为子件的父级Id
	mbId, err := db.GetProductBomMbId(mid)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	// 跳出循环标志符
	outLop := 0
	inLop := 0
	for i, id := range dat.ChildrenIds {
		outLop++
		childMid, err := utils.StrToInt(id)
		if err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		chNum, err := utils.StrToInt(dat.ChildrenDosage[i])
		if err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		// 根据子件Id 查询 mater 表确定该子件的类型，如果是2半成品，则根据该子件的Id查询该半成品在 Bom 表中所关联的所有子件，如果查询到了则设置该子件的 lvl += 1 = 2，并设置该子件下所有物料的 lvl += 1 = 3,
		// 在 bom 表中如果没有查询到为半成品的子件则所有的层级为设置为 lvl = 2, 有值的时候存储一次子件为半成品的Id 并设置 lvl = 2， 无值的时候 lvl 全部设置为 2
		mids, err := db.GetChildType(childMid)
		if err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		// 保存第二级的物料
		model = &db.Model{MaterBom: &db.MaterBom{
			MId:       childMid,
			FmId:      mbId,
			MaterLv:   LVL2,
			Dosage:    chNum,
			BomNameId: bmId,
		}}
		if err = db.InsertDatToMaterBom(model); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		//continue
		// 如果查询到的数据大于 0 则说明当前存入子件为半成品的数据,则先存入一条半成品数据，然后在循环查询出来的子件依次插入数据库并设置层级为 3,否则全部依次插入数据库并设置层级全部为 2
		if len(mids) > 0 {
			for _, val := range mids {
				inLop++
				model = &db.Model{MaterBom: &db.MaterBom{
					MId:       val.MId,
					FmId:      val.FmId,
					MaterLv:   LVL3,
					Dosage:    val.Dosage,
					BomNameId: bmId,
				}}
				if err = db.InsertDatToMaterBom(model); err != nil {
					c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
					return
				}
				if inLop == len(mids) {
					break
				}
			}
			c.JSON(200, gin.H{"success": "success", "errMsg": ""})
			// todo 这里应该结束执行吗？
			//return
		}
		if outLop == len(dat.ChildrenIds) {
			break
		}
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// getAllSaleAndBusiness 获取所有销售和业务人员
func getAllSaleAndBusiness(c *gin.Context) {
	users, err := db.GetAllSaleAndBusiness()
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": users, "errMsg": ""})
}

// getAllIsBomMater 打印条码时根据选择的配置信息获取相关的配置物料以供选择关联的物料信息
func getAllIsBomMater(c *gin.Context) {
	verId := c.Query("ver_id")
	bid, err := utils.StrToInt(verId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	maters, err := db.GetIsBomTypeMater(bid)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	if len(maters) > 0 {
		c.JSON(200, gin.H{"success": maters, "errMsg": ""})
		return
	}
	c.JSON(200, gin.H{"success": "", "errMsg": "当前配置还没有组装过的物料清单，请先进行产品组装操作或重新选择其它配置~!"})
}

// printCodeControl 二维码打印控制器
func printCodeControl(c *gin.Context) {
	//wg := &sync.WaitGroup{}
	//prinChan := make(chan bool, 1) // 用于控制并发数量
	codeText := ""
	codeStar := c.PostForm("codeHead")
	cfgNameId := c.PostForm("cfgVer")
	materId := c.PostForm("mater")
	cliId := c.PostForm("client")
	saleId := c.PostForm("sale")
	date := c.PostForm("date")
	timeUnix, err := utils.StrToUnix(date)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	number := c.PostForm("num")
	count, err := utils.StrToInt(number)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	printer := c.PostForm("printer")

	// 拼接条码数据
	barcode := codeStar + "," + cfgNameId + "," + materId + "," + cliId + "," + saleId + "," + strconv.Itoa(timeUnix) + ","
	countStr := ""
	for i := 0; i < count; i++ {
		randNum := rand.Intn(9999)
		switch len(strconv.Itoa(randNum)) {
		case 3:
			countStr = "0" + strconv.Itoa(randNum)
		case 2:
			countStr = "00" + strconv.Itoa(randNum)
		case 1:
			countStr = "000" + strconv.Itoa(randNum)
		default:
			countStr = strconv.Itoa(randNum)
		}
		// 根据条码查询当前条码是否存在，存在则进入下次循环
		if db.GetCodeBid(barcode + countStr) {
			// 拼接条码
			codeText += barcode + countStr + "\n"
			// 保存条码信息到数据库
			if err = utils.CodeToSql(barcode, countStr); err != nil {
				c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
				return
			}
		} else {
			continue
		}
	}
	if err = utils.PrintCodeFunc(codeText, printer); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// getUpcomingHandler 获取所有待办事项控制器
func getUpcomingHandler(c *gin.Context) {
	stateId := c.Query("statId")
	upcomingSlice, err := db.GetAllUpcomingDat("", stateId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": upcomingSlice, "errMsg": ""})
}

// getUpcomingDetail 获取待办事项详细信息
func getUpcomingDetail(c *gin.Context) {
	workId := c.Query("link_Id")
	upcomingSlice, err := db.GetAllUpcomingDat(workId, "")
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": upcomingSlice, "errMsg": ""})
}

// changeWorkProcessStat 审核后修改工单的 process 状态为 1
func changeWorkProcessStat(c *gin.Context) {
	workId := c.PostForm("workId")
	if err := db.UpdateWorkProcess(workId); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// getSonCodeDosage 根据主条码获取子条码用量
func getSonCodeDosage(c *gin.Context) {
	/*
		条码数据说明：
			序号头：codeSplt[0]
			配置Id: codeSplt[1]
			物料Id：codeSplt[2]
			客户Id: codeSplt[3]ss
			用户Id：codeSplt[4]
			时间戳：codeSplt[5]
			随机数：codeSplt[6]
	*/
	cfgId := c.Query("cfgId")
	gongwei := c.Query("gongwei")
	hostCode := c.Query("hostCode")
	// 用来确定使用那条查询语句
	codeList := strings.Split(hostCode, ",")
	fmt.Println(codeList, "====", cfgId, "=======", hostCode)
	if gongwei == "0" {
		sonAge, err := db.GetSonDosage(gongwei, hostCode)
		if err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		c.JSON(200, gin.H{"success": sonAge, "errMsg": ""})
	} else if gongwei == "1" {
		sonAge, err := db.GetSonDosage(gongwei, hostCode)
		if err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		c.JSON(200, gin.H{"success": sonAge, "errMsg": ""})
	} else {
		sonAge, err := db.GetSonDosage(gongwei, hostCode)
		if err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		c.JSON(200, gin.H{"success": sonAge, "errMsg": ""})
	}
}

// bindCodeInfo 绑定条码功能函数
func bindCodeInfo(c *gin.Context) {
	type datJson struct {
		EditType         int      `json:"editType" from:"editType"`
		HostCode         string   `json:"hostCode" form:"hostCode"`
		AnnexHostCodeDat string   `json:"annexHostCodeDat" form:"annexHostCodeDat"`
		DiskCodeList     []string `json:"diskCodeList" form:"diskCodeList"`
		PowerCodeList    []string `json:"powerCodeList" form:"powerCodeList"`
		ShowCodeList     []string `json:"showCodeList" form:"showCodeList"`
	}
	data := &datJson{}
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("操作失败，错误代码：%v", err)})
		return
	}
	// hostCode 等于空说明是绑定主板套装条码,不等于空说明是在绑定整机套装条码
	if data.HostCode == "" {
		if err := bindCodeToCodeBom(HostType, data.AnnexHostCodeDat); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		if len(data.DiskCodeList) > 0 {
			for _, cpu := range data.DiskCodeList {
				if err := bindCodeToCodeBom(HostType, cpu); err != nil {
					c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
					return
				}
			}
		} else {
			c.JSON(200, gin.H{"success": "", "errMsg": "操作失败未填写【cpu】条码哦~！"})
			return
		}
		// 可能会不需要绑定散热器条码,因此此处价格判断
		if len(data.PowerCodeList) > 0 {
			for _, fan := range data.PowerCodeList {
				if err := bindCodeToCodeBom(HostType, fan); err != nil {
					c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
					return
				}
			}
		}
		if len(data.ShowCodeList) > 0 {
			for _, mem := range data.ShowCodeList {
				if err := bindCodeToCodeBom(HostType, mem); err != nil {
					c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
					return
				}
			}
		} else {
			c.JSON(200, gin.H{"success": "", "errMsg": "操作失败,未填写【内存】条码哦~！"})
			return
		}
	} else {
		if data.EditType != 2 {
			if err := bindCodeToCodeBom(LVL1, data.HostCode); err != nil {
				c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
				return
			}
			// 附件主条码操作
			if data.AnnexHostCodeDat != "" {
				if err := bindCodeToCodeBom(LVL1, data.AnnexHostCodeDat); err != nil {
					c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
					return
				}
			} else {
				c.JSON(200, gin.H{"success": "", "errMsg": "未扫入附件主条码(主板条码哦~！)"})
				return
			}
			// 硬盘主条码操作
			if len(data.DiskCodeList) > 0 {
				for _, disk := range data.DiskCodeList {
					if err := bindCodeToCodeBom(LVL1, disk); err != nil {
						c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
						return
					}
				}
			} else {
				c.JSON(200, gin.H{"success": "", "errMsg": "未扫入硬盘条码哦"})
				return
			}
			// 电源条码操作
			if len(data.PowerCodeList) > 0 {
				for _, power := range data.PowerCodeList {
					if err := bindCodeToCodeBom(LVL1, power); err != nil {
						c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
						return
					}
				}
			} else {
				c.JSON(200, gin.H{"success": "", "errMsg": "未扫入硬盘条码哦"})
				return
			}
			// 显卡主条码操作
			if len(data.ShowCodeList) > 0 {
				for _, show := range data.ShowCodeList {
					if err := bindCodeToCodeBom(LVL1, show); err != nil {
						c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
						return
					}
				}
			}
		} else {
			if err := bindCodeToCodeBom(LVL2, data.HostCode); err != nil {
				c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
				return
			}
			for _, duli := range data.DiskCodeList {
				if err := bindCodeToCodeBom(LVL2, duli); err != nil {
					c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
					return
				}
			}
		}
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// getAllBusWorkOrder 获取所有业务工单数据
func getAllBusWorkOrder(c *gin.Context) {
	datSlices, err := db.GetBusWorkOrder("")
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": datSlices, "errMsg": ""})
}

// getBusWorkOrder 根据工单状态获取业务工单数据
func getBusWorkOrder(c *gin.Context) {
	workId := c.Query("workId")
	datSlices, err := db.GetBusWorkOrder(workId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": datSlices, "errMsg": ""})
}

// getWxPeople 获取所有外发公司名称
func getOutFirm(c *gin.Context) {
	firm, err := db.GetOutFirm("")
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": firm, "errMsg": ""})
}

// getWxUsers 获取所有维修人员数据
func getWxUsers(c *gin.Context) {
	users, err := db.GetWxUsers(c.Query("userType"))
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": users, "errMsg": ""})
}

// changeWork 修改工单状态
func changeWork(c *gin.Context) {
	eitType := c.PostForm("editType")
	workType = c.PostForm("workType")
	workId := c.PostForm("workId")
	if eitType == "" || workId == "" {
		c.JSON(200, gin.H{"success": "", "errMsg": "操作类型或工单号不允许为空，操作失败~！"})
		return
	}
	switch eitType {
	case "1":
		// 接收操作,将 work 表中的 state 字段更新为 1
		if workType != "维修工单" {
			if err := db.UpdateWorkState(eitType, workId, "", "", ""); err != nil {
				c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
				return
			}
		}
	case "4":
		// 派单操作(派内修单)
		if err := db.UpdateWorkState(eitType, workId, "", "", ""); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// changeWorkState 派单操作修改工单状态
func changeWorkState(c *gin.Context) {
	et := c.PostForm("editType")
	wId := c.PostForm("workId")
	switch et {
	case "2":
		// 派内修工单
		pId := c.PostForm("peopleId")
		if err := db.UpdateWorkState(et, wId, pId, "", ""); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
	case "3":
		// 派外发工单
		oId := c.PostForm("outFirm")
		oNum := c.PostForm("outFirmNum")
		if err := db.UpdateWorkState(et, wId, "", oId, oNum); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
	case "5":
		// 确认发货
		//"workId": $(val).siblings(".del_work_info").children().children("li").eq(liLen - liLen).children("span").text(),
		if err := db.UpdateWorkState(et, wId, "", "", ""); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		eId := c.PostForm("kuaidiId")
		eNum := c.PostForm("kuaidiNum")
		if eId == "" || eNum == "" {
			c.JSON(200, gin.H{"success": "", "errMsg": "发出物流或快递单号不允许为空，操作失败~！"})
			return
		}
		wid, err := utils.StrToInt(wId)
		if err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("后台服务器错误，给您带来不便敬请谅解，错误代码：%v", err)})
			return
		}
		eid, err := utils.StrToInt(eId)
		if err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("后台服务器错误，给您带来不便敬请谅解，错误代码：%v", err)})
			return
		}
		retWorks := &db.ReturnWork{
			ExpressId:  eid,
			WorkId:     wid,
			RetNum:     0,
			ExpressNum: eNum,
			CrateDate:  time.Now().Format("2006-01-02 03:04:05"),
		}
		if err = db.InertDatToRetWork(retWorks); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// changeWorkStateAndIstToPay 维修收费工单接收操作修改工单状态以及将输入插入到pay表
func changeWorkStateAndIstPay(c *gin.Context) {
	type retDat struct {
		Wid      string `json:"wid"`
		Cost     string `json:"cost"`
		Model    string `json:"model"`
		Cname    string `json:"cname"`
		WorkType string `json:"workType"`
		EditType int    `json:"editType"`
	}
	result := &retDat{}
	if err := c.ShouldBindJSON(result); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("操作错误，错误代码：%v", err)})
		return
	}
	// 1. 根据 wid 在 work 表中查询 销售Id 以及 客户Id 和 物料Id
	datas, err := db.GetWorkLinkedId(result.Wid)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}

	// 更新工单状态为 1,并修改付款状态为 1
	if err = db.UpdateWorkState(strconv.Itoa(result.EditType), result.Wid, "", "", ""); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}

	// 将支付此案次插入 pay 表
	amount, err := strconv.Atoi(result.Cost)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	wid, err := strconv.Atoi(result.Wid)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	payInfo := &db.Model{Pay: &db.Pay{
		Amount:   amount,
		WorkId:   wid,
		SaleId:   datas[0].UId,
		ClientId: datas[0].CId,
		MaterId:  datas[0].MId,
	}}
	if err = db.InsertDtToPay(payInfo); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// sendWorkState 发货给客户控制器函数
func sendWorkState(c *gin.Context) {
	editType := c.PostForm("editType")
	workId := c.PostForm("workId")
	wid, err := utils.StrToInt(workId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	kuaidiId := c.PostForm("kuaidiId")
	expressId, err := utils.StrToInt(kuaidiId)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	retCount := c.PostForm("retCount")
	retNum, err := utils.StrToInt(retCount)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	expressNum := c.PostForm("kuaidiNum")
	if err = db.UpdateWorkState(editType, workId, "", "", ""); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	retDat := &db.ReturnWork{
		ExpressId:  expressId,
		WorkId:     wid,
		RetNum:     retNum,
		ExpressNum: expressNum,
		CrateDate:  time.Now().Format("2006-01-02 03:04:05"),
	}
	if err = db.InertDatToRetWork(retDat); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// codeSearchDat 后台搜索功能控制器
func codeSearchDat(c *gin.Context) {
	/*
		条码数据说明：
			序号头：codeSplt[0]
			配置Id: codeSplt[1]
			物料Id：codeSplt[2]
			客户Id: codeSplt[3]
			用户Id：codeSplt[4]
			时间戳：codeSplt[5]
			随机数：codeSplt[6]
	*/
	code := c.Query("searchKey")
	// 根据条码查查询该条码关联的所有条码对应的物料
	ret, err := db.GetCodeCheckCfg(code)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	retMap := make(map[string]interface{})
	if len(ret) > 0 {
		retMap["schCode"] = code
		retMap["ret"] = ret
		// 将条码上的时间戳转换为时间
		for _, val := range retMap["ret"].([]*db.CodeCheckTmpTable) {
			timeFat, err := utils.StrToTimeFat(strings.Split(val.BarCode, ",")[5])
			if err != nil {
				c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
				return
			}
			val.ProTime = strings.Split(timeFat, " ")[0]
			if val.OutLibDate == nil {
				val.OutLibDate = new(string)
			}
		}
	} else {
		ret, err = db.GetCodeMaterInfo(code)
		retMap["schCode"] = code
		retMap["ret"] = ret
		if err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		if len(ret) > 0 {
			timeFat, err := utils.StrToTimeFat(strings.Split(code, ",")[5])
			if err != nil {
				c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
				return
			}
			ProTime := strings.Split(timeFat, " ")[0]
			ret[0].ProTime = ProTime
		} else {
			c.JSON(200, gin.H{"success": "未查询到任何数据", "errMsg": ""})
			return
		}
	}
	// 根据客户Id 查询客户资料
	c.JSON(200, gin.H{"success": retMap, "errMsg": ""})
}

// scanQrCode 仓库扫描二维码更新出厂时间方法
func scanQrCode(c *gin.Context) {
	// 获取用户数据
	se := utils.InitSession(c, "admin")
	seDat := se.GetSession(se.SesKey.SerSessKey)
	mapDat, err := se.SliceUint8ToMap(seDat)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("操作失败，错误代码：%v", err)})
		return
	}
	if c.Request.Method == "POST" {
		var warrantDate int
		type reqDat struct {
			LinkOrder string   `json:"linkOrder"`   // 关联的工单
			ScanRet   []string `json:"scanRet"`     // 扫描结果
			WarType   string   `json:"warrantType"` // 保修时间
		}
		dat := &reqDat{}
		if err := c.ShouldBindJSON(&dat); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("发货失败，错误代码：%v", err)})
			return
		}
		switch dat.WarType {
		case "1":
			warrantDate = 30
		case "2":
			warrantDate = 180
		case "3":
			warrantDate = 365
		}

		for i, val := range dat.ScanRet {
			if utils.IsCodeHead(val) {
				model := &db.Model{Code: &db.Code{
					WarrantyTime: warrantDate,
					BarCode:      val,
					OutLibDate:   time.Now().Format("2006-01-02 03:04:05"),
					LinkOrder:    dat.LinkOrder,
					OutLibUserId: mapDat.Tid,
				}}
				if strings.HasPrefix(val, CodeHeadZ) || strings.HasPrefix(val, CodeHeadD) {
					aryCodes, err := db.GetCodeComBar(val)
					if err != nil {
						c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
						return
					}
					if len(aryCodes) > 0 {
						for _, code := range aryCodes {
							sonModel := &db.Model{Code: &db.Code{
								WarrantyTime: warrantDate,
								BarCode:      code,
								OutLibDate:   time.Now().Format("2006-01-02 03:04:05"),
								LinkOrder:    dat.LinkOrder,
								OutLibUserId: mapDat.Tid,
							}}
							if _, err = db.CodeSqlOpFunc("update", sonModel); err != nil {
								c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
								return
							}
						}
					} else {
						if _, err = db.CodeSqlOpFunc("update", model); err != nil {
							c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
							return
						}
					}
				}
				if _, err = db.CodeSqlOpFunc("update", model); err != nil {
					c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
					return
				}
			} else {
				c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("部分出货成功，其中扫入的第 %v 条码数据错误，请刷新该页面后重新进行出货操作", i+1)})
				return
			}
		}
		c.JSON(200, gin.H{"success": "出货成功.....", "errMsg": ""})
		return
	}
	c.HTML(200, "template/scan_qr.html", gin.H{"title": "链力", "name": mapDat.UserName})
}

// UploadFileToSql 导入基础物料数据到数据库
func UploadFileToSql(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("导入失败，错误代码：%v", err)})
		return
	}
	extFile := path.Ext(file.Filename)
	if extFile != ".xlsx" {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("文件格式错误，仅支持“.xlsx”格式的文件")})
		return
	}
	// 生成新的文件名
	fileNameInt := time.Now().Unix()
	fileNameStr := strconv.FormatInt(fileNameInt, 10)
	fileNewName := fileNameStr + extFile
	// 在指定目录下创建按照年月日创建目录结构
	fileCfg, err := cfg.LoadCfg(cfg.FILEPATH)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("读取配置文件错误，导入失败：%v", err)})
		return
	}
	filePath, err := utils.MkdirFilePath(fileCfg.FilePath.Path)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	// 拼接出新的文件目录
	newPath := filepath.Join(filePath, "./", fileNewName)
	// 保存文件到目录中
	if err = c.SaveUploadedFile(file, newPath); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("保存文件到指定目录错误，错误代码：%v", err)})
		return
	}
	// 开始读取文件将内容写入数据库，成功后删除文件
	if err = utils.OpenFileToSql(newPath); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	if err = os.RemoveAll(fileCfg.FilePath.Path + "/" + time.Now().Format("2006")); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("数据导入成功，但文件删除失败，错误代码：%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "success", "errMsg": ""})
}

// querySonCodeNum 通过主条码获取子条码数量
func querySonCodeNum(c *gin.Context) {
	code := c.Query("code")
	ret, err := db.GetCodeCheckCfg(code)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("获取出库总数量出错，错误代码：%v", err)})
		return
	}
	outLibNum := len(ret)
	c.JSON(200, gin.H{"success": outLibNum, "code": code, "errMsg": ""})
}

// rePrintCode 重打损坏的条码控制器
func rePrintCode(c *gin.Context) {
	code := c.Query("code")
	printer := c.Query("printer")
	if err := utils.PrintCodeFunc(code, printer); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("条码打印失败，错误代码：%v", err)})
		return
	}
	c.JSON(200, gin.H{"success": "条码打印成功", "errMsg": ""})
}

// searchMaterInfo 物料搜索功能函数
func searchMaterInfo(c *gin.Context) {
	schMater := c.Query("mater")
	if maters, err := db.GetSearchMaterInfo(schMater); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsh": fmt.Sprintf("%v", err)})
		return
	} else {
		c.JSON(200, gin.H{"success": maters, "errMsg": ""})
	}
}

// searchWorkInfo 工单搜索功能函数
func searchWorkInfo(c *gin.Context) {
	type requestData struct {
		SerType   int    `json:"ser_type"`
		StartDate string `json:"start_date"`
		EndDate   string `json:"end_date"`
		IptData   string `json:"ipt_data"`
	}
	var reqJsonDat requestData
	if err := c.ShouldBindJSON(&reqJsonDat); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("failed to context ShouldBindJSON:%v", err)})
		return
	}
	if dats, err := db.GetSearchWorkInfo(reqJsonDat.SerType, reqJsonDat.StartDate, reqJsonDat.EndDate, reqJsonDat.IptData); err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("failed to db GetSearchWorkInfo:%v", err)})
		return
	} else {
		c.JSON(200, gin.H{"success": dats, "errMsg": ""})
	}
}

func warehouse(c *gin.Context) {
	if c.Request.Method == "POST" {
		type requestDat struct {
			SerType   int    `json:"ser_type"`
			StartDate string `json:"start_date"`
			EndDate   string `json:"end_date"`
			IptDat    string `json:"ipt_dat"`
		}
		var reqDat requestDat
		if err := c.ShouldBindJSON(&reqDat); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("查询失败，提交的查询条件有误，错误代码：%v", err)})
			return
		}
		dats, err := db.GetWarehouseDat(reqDat.SerType, reqDat.StartDate, reqDat.EndDate, reqDat.IptDat)
		if err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		c.JSON(200, gin.H{"success": dats, "count": len(dats), "errMsg": ""})
		return
	}
	// 获取用户类型，用于前端做判断根据不同用户显示不同菜单
	se := utils.InitSession(c, "admin")
	data := se.GetSession(se.SesKey.SerSessKey)
	seMap, err := se.SliceUint8ToMap(data)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.HTML(200, "template/warehouse.html", gin.H{"title": "链力", "tid": seMap.Tid})
}

// addNewProduct 增加新品推荐
func addNewProduct(c *gin.Context) {
	if c.Request.Method == "POST" {
		type newProduct struct {
			ImgUrl  string `json:"img_url"`
			JumpUrl string `json:"jump_url"`
			Remark  string `json:"remark"`
		}
		var reDat = &newProduct{}
		if err := c.ShouldBindJSON(reDat); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("获取数据错误，错误代码：%v", err)})
		}
		model := &db.Model{NewProduct: &db.NewProduct{
			ImgUrl:  reDat.ImgUrl,
			JumpUrl: reDat.JumpUrl,
			Remark:  &reDat.Remark,
		}}
		if err := db.InsertNreProductUrlToSql(model); err != nil {
			c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
			return
		}
		c.JSON(200, gin.H{"success": "success", "errMsg": ""})
		return
	}
	// 获取用户类型，用于前端做判断根据不同用户显示不同菜单
	se := utils.InitSession(c, "admin")
	data := se.GetSession(se.SesKey.SerSessKey)
	seMap, err := se.SliceUint8ToMap(data)
	if err != nil {
		c.JSON(200, gin.H{"success": "", "errMsg": fmt.Sprintf("%v", err)})
		return
	}
	c.HTML(200, "template/addNewProduct.html", gin.H{"title": "链力科技", "name": seMap.UserName})
}
