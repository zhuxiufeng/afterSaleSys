module asystem/wxPush/src

go 1.17

replace asystem/wxPush/util => ../util

require asystem/wxPush/util v0.0.0-00010101000000-000000000000

require (
	github.com/goinggo/mapstructure v0.0.0-20140717182941-194205d9b4a9 // indirect
	gopkg.in/ini.v1 v1.63.2 // indirect
)
