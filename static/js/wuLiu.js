// 修改物流按钮事件
function change_wuLiu(val) {
    $(".wuLiu_exhibit,.wuLiu_add,.wuLiu_send_con").css("display", "none");
    $(".wuLiu_change").css("display", "block");
    $("#wuLiuId").val($(val).parent("td").siblings().eq(0).text());
    $("#wuLiuName").val($(val).parent("td").siblings().eq(1).text());
    $("#buttPeople").val($(val).parent("td").siblings().eq(2).text());
    $("#buttPhone").val($(val).parent("td").siblings().eq(3).text());
}

// 删除物流按钮事件
function del_wuLiu(val) {
    $(".wuLiu_send_con").css("display", "block");
    $("#del_wuLiu_id").text($(val).parent("td").siblings("td").eq(0).text());
    $("#del_wuLiu_name").text($(val).parent("td").siblings("td").eq(1).text());
    $("#del_wuLiu_phone").text($(val).parent("td").siblings("td").eq(2).text());
    $("#del_wuLiu_addr").text($(val).parent("td").siblings("td").eq(3).text());
}

// 添加物流按钮事件
function addWuLiu(val) {
    $(".wuLiu_exhibit,.wuLiu_send_con").css("display", "none");
    $(".wuLiu_add").css("display", "block");
}

// 添加物流提交按钮
function add_wuLiu_submit() {
    postSubmit("/admin/addWuLiu", {
        "wuLiuName":$("#wuLiu_name").val(),
        "buttPeople":$("#butt_people").val(),
        "buttPhone":$("#butt_phone").val(),
    }, postCallFunc);
}

// 修改物流提交按钮
function change_wuLiu_submit() {
    putFunc("/admin/changeWuLiu", {
        "wuLiuId": $("#wuLiuId").val(),
        "wuLiuName": $("#wuLiuName").val(),
        "buttPeople": $("#buttPeople").val(),
        "buttPhone": $("#buttPhone").val(),
    }, putCallback)
}

