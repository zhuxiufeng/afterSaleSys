// 添加BOM名称按钮事件
function addBomName() {
    $(".bomName_exhibit,.bomName_send_con").css("display", "none");
    $(".bomName_add").css("display", "block");
}

// 添加bom名称提交按钮事件
function add_bomName_submit() {
    postSubmit("/admin/addBomName", {
        "name":$("#addBomName").val(),
    }, postCallFunc);
}

// 修改 BomName 按钮事件
function change_bomName(val) {
    $(".bomName_exhibit,.bomName_add,.bomName_send_con").css("display", "none");
    $(".bomName_change").css("display", "block");
    $("#bm_id").val($(val).parent("td").siblings().eq(0).text());
    $("#bm_name").val($(val).parent("td").siblings().eq(1).text());
}

// 修改 bomName 提交按钮事件
function change_bomName_submit() {
    putFunc("/admin/changeBomName", {
        "bid": $("#bm_id").val(),
        "name": $("#bm_name").val(),
    }, putCallback)
}

// 删除BomName 按钮事件
function del_bomName(val) {
    $(".bomName_send_con").css("display", "block");
    $("#del_bomName_id").text($(val).parent("td").siblings("td").eq(0).text());
    $("#del_bomName_name").text($(val).parent("td").siblings("td").eq(1).text());
}

// 删除 bomName 提交事件
function del_bomName_info(val) {
    delDataEdit("/admin/del_bomName", {
        "bmId": $("#del_bomName_id").text(),
        "bmName": $("#del_bomName_name").text(),
    }, deleteCallBack);
}