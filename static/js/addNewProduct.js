let img_tishi = "/static/img/sl.png"
$(()=>{
    // 实时监听图片链接输入框
    $("#img_url").change(()=>{
        if ($('#img_url').val() !== "") {
            $("#image_show").attr("src", $('#img_url').val());
        } else {
            $("#image_show").attr("src", img_tishi);
        }
    });
});

function isHeard(url) {
    return !(url.substring(0, 4) !== "http" || url.substring(0, 5) !== "https");
}

submit_data = () => {
    let img_url = $("#img_url").val();
    let jump_url = $("#jump_url").val();
    if (img_url === "") {
        $(".err").css("display", "block");
        $(".err > span").empty().text("图片链接地址不允许留空，请重新输入").delay(6000).fadeOut();
        return;
    } else if (jump_url === "") {
        $(".err").css("display", "block");
        $(".err > span").css("display", "block").empty().text("购买链接地址不允许留空，请重新输入").delay(6000).fadeOut();
        return;
    }
    let remark = $("#remark").val();
    if (!isHeard(img_url)) {
        $(".err").css("display", "block");
        $(".err > span").empty().text("输入的图片链接地址格式不正确，应以 http或https 开头，请重新输入").delay(6000).fadeOut();
        return;
    } else if (!isHeard(jump_url)) {
        $(".err").css("display", "block");
        $(".err > span").css("display", "block").empty().text("输入的购买链接地址格式不正确，应以 http或https 开头，请重新输入").delay(6000).fadeOut();
        return;
    }
    $.ajax({
        type: "post",
        url: "",
        dataType: "json",
        data: JSON.stringify({
            "img_url": img_url,
            "jump_url": jump_url,
            "remark": remark
        }),
        success: (dat) => {
            if (dat["success"] !== "") {
                $("#img_url").val("");
                $("#jump_url").val("");
                $("#remark").val("");
                $("#image_show").attr("src", img_tishi);
            } else {
                $(".err").css("display", "block");
                $(".err > span").css("display", "block").empty().text(dat["errMsg"]).delay(6000).fadeOut();
            }
        },
    });
}