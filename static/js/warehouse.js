function searchShipping() {
    let serType = 0;
    let startDate = $("#houseStartDate").val();
    let endDate = $("#houseEndDate").val();
    let iptData = $(".warehouse_input").val();
    if (iptData === "" && startDate === "" && endDate === "") {
        alert("未选择过滤条件或未输入需要搜索的内容");
        return;
    } else if (startDate === "" && endDate === "") {
        serType = 2;  // 按搜索内容进行搜索
        $(".warehouseNum > strong").empty().text("单号 【" + iptData + "】 出货总数：");
    } else if (iptData === "") {
        serType = 1;  // 按时间进行过滤搜索
        $(".warehouseNum > strong").empty().text(startDate.split(" ")[0] + " 至 " + endDate.split(" ")[0] + " 时间段出货总数：");
    } else {
        $(".warehouseNum > strong").empty().text(startDate.split(" ")[0] + " 至 " + endDate.split(" ")[0] + " 时间段单号 【" + iptData + "】 出货总数：");
    }
    $.ajax({
        type: "POST",
        url: "/admin/warehouse",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            "ser_type": serType,
            "start_date": startDate,
            "end_date": endDate,
            "ipt_dat": iptData,
        }),
        success: (vat) => {
            if (vat["success"] !== "") {
                $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr, .print_add, .print_change, .print_mgr,.goods_up,.code_mgr,.work_mgr,.wuLiu_mgr,.todo_mgr,.bomName_mgr,.search_mgr").css("display", "none");
                $(".warehouse_panel").css("display", "block");
                let retHml = ``
                $(".warehouseNum > span").empty().text(vat["count"]);
                for (var dat in vat["success"]) {
                    retHml += `
                        <tr>
                            <td>`+vat["success"][dat]["link_order"]+`</td>
                            <td>`+vat["success"][dat]["mname"]+`</td>
                            <td>`+vat["success"][dat]["brand"]+`</td>
                            <td>`+vat["success"][dat]["model"]+`</td>
                            <td>`+vat["success"][dat]["outlib_date"]+`</td>
                            <td>`+vat["success"][dat]["barcode"]+`</td>
                        </tr>
                    `
                    $("#house_scrollTable > div > .tbody > .table_data").empty().append(retHml);
                }
            } else {
                $(".houseTishi > span").css("display", "block").empty().text(vat["errMsg"]).delay(6000).fadeOut();
            }
        }
    });
}