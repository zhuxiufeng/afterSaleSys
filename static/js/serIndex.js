let flag = 0;

const BTAddr = "http://192.168.3.200:8081/Integration/printCode/Execute"

// 判断数据是否为数字方法
function isNumber(value) {
    var patrn = /^(-)?\d+(\.\d+)?$/;
    if (patrn.exec(value) === null || value === "") {
        return false
    } else {
        return true
    }
}

// 退出登录操作
function quitLogin() {
    $.ajax({
        type: "GET",
        url: "/quit",
        dataType: "json",
        data: {},
        success: (val) => {
            if (val["success"] !== "") {
                $.removeCookie('LIANLISESSIONID20220603',{ path: '/'});
                window.location.href="/sign";
            } else {
                alert(val["errMsg"]);
            }
        }
    });
}

// 切换用户操作方法
function switchUser() {
    $.removeCookie('LIANLISESSIONID20220603',{ path: '/'});
}

function clickBtn() {
    if (flag === 0) {
        $(".user_menu").css("display", "block")
        flag++;
    } else {
        $(".user_menu").css("display", "none")
        flag--;
    }
}

// 点击收缩展开左侧菜单
function shrinkBtn() {
    if (flag === 0) {
        $(".menu_box").css("display", "none");
        $(".msg_box").css("width", "100%");
        // 无法更改图标
        $(".shrink_ico").removeClass("glyphicon glyphicon-resize-small").addClass("glyphicon glyphicon-resize-full");
        flag++;
    } else {
        $(".menu_box").css("display", "block");
        $(".msg_box").css("width", "83.33333333%");
        $(".shrink_ico").removeClass("glyphicon glyphicon-resize-full").addClass("glyphicon glyphicon-resize-small");
        flag--;
    }
}

// 点击左侧菜单隐藏显示二级菜单
function showHide(val) {
    if (flag === 0) {
        $(val).removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down").css("color", "darkorange").next().css("display", "block");
        flag++;
    } else {
        $(val).removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right").css("color", "white").next().css("display", "none");
        flag--;
    }
    $("#path_two,#path_one").empty();
    // $("#path_one").empty().text(" / " + $(val).parent().text().split(" ")[0]);

    // 点击某个菜单展开折叠时删除该菜单所有同辈标签下的二级菜单的 active 属性
    $(val).parent().siblings().children().children().removeClass("active");
    $(".menu_list_ul2_1").removeClass("active");
}

// 点击二级菜单事件
function checkSonMenu(val) {
    $(val).parent().parent().parent().siblings().children().children("a").removeClass("active");
    $(val).parent().siblings().removeClass("active");  // 查找所有兄弟标签并删除 active 类
    $(val).parent().addClass("active");  // 查找父级元素并添加 active 类
    $("#path_one").empty().text($(val).parent().parent().parent().text().split(" ")[0]);
    $("#path_two").empty().text(" > " + $(val).text());
    switch ($(val).text()) {
        case ("用户管理"):
            $(".user_change,.user_add,.client_mgr,.mater_mgr,.print_mgr,.code_mgr,.goods_up,.todo_mgr,.work_mgr,.wuLiu_mgr,.bind_mgr,.search_mgr,.warehouse_panel").css("display", "none");
            $(".user_exhibit, .user_mgr").css("display", "block");
            // 发起 ajax GET 请求获取用户数据，如何接收 ajax 中返回的值？
            requestGetAllData("/admin/getAllUser", getCallBackFunc);
            break;
        case ("客户管理"):
            // $(".user_mgr, .client_add, .client_change, .mater_mgr").css("display", "none");
            $(".user_mgr, .client_add, .client_change, .mater_mgr,.print_mgr,.code_mgr,.goods_up,.todo_mgr,.work_mgr,.wuLiu_mgr,.bind_mgr,.bomName_mgr,.search_mgr,.warehouse_panel").css("display", "none");
            $(".client_mgr, .client_exhibit").css("display", "block");
            requestGetAllData("/admin/getAllClient", getCallBackFunc);
            break;
        case ("物流公司"):
            $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr,.print_mgr, .print_add, .print_change,.code_mgr,.goods_up,.todo_mgr,.work_mgr,.wuLiu_add,.wuLiu_change,.bind_mgr,.bomName_mgr,.search_mgr,.warehouse_panel").css("display", "none");
            $(".wuLiu_mgr, .wuLiu_exhibit").css("display", "block");
            requestGetAllData("/admin/getAllWuLiu", getCallBackFunc);
            break;
        case ("物料管理"):
            $(".user_mgr, .client_mgr, .mater_add, .mater_change,.print_mgr,.code_mgr,.goods_up,.todo_mgr,.work_mgr,.wuLiu_mgr,.bind_mgr,.bomName_mgr,.search_mgr,.warehouse_panel").css("display", "none");
            $(".mater_mgr, .mater_exhibit, .serMater").css("display", "block");
            $(".search_mater_input").val("");
            requestGetAllData("/admin/getAllMater", getCallBackFunc);
            break;
        case ("产品组装"):
            $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr, .print_add, .print_change, .print_mgr,.code_mgr,.todo_mgr,.work_mgr,.wuLiu_mgr,.bind_mgr,.bomName_mgr,.search_mgr,.warehouse_panel").css("display", "none");
            $(".goods_up").css("display", "block");
            requestGetAllData("/admin/getAllMaterIdName", getCallBackFunc);
            requestGetAllData("/admin/getAllBomName", getCallBackFunc);
            requestGetAllData("/admin/getMaterNotInOneAndTwo", getCallBackFunc);
            break;
        case ("打印机管理"):
            $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr, .print_add, .print_change,.code_mgr,.goods_up,.todo_mgr,.work_mgr,.wuLiu_mgr,.bind_mgr,.bomName_mgr,.search_mgr,.warehouse_panel").css("display", "none");
            $(".print_mgr, .print_exhibit").css("display", "block");
            requestGetAllData("/admin/getAllPrinter", getCallBackFunc);
            break;
        case ("条码打印"):
            $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr, .print_add, .print_change, .print_mgr,.goods_up,.todo_mgr,.work_mgr,.wuLiu_mgr,.bind_mgr,.bomName_mgr,.search_mgr,.warehouse_panel").css("display", "none");
            $(".code_mgr").css("display", "block");
            // 点击条码打印菜单，应获取所有产品信息和所有客户信息以及所有销售员信息
            requestGetAllData("/admin/PrintGetAllPrinter", getCallBackFunc);
            requestGetAllData("/admin/PrintGetAllClient", getCallBackFunc);
            requestGetAllData("/admin/PrintGetAllUser", getCallBackFunc);
            requestGetAllData("/admin/PrintGetAllVersion", getCallBackFunc); // 从产品 Bom 表获取所有版本信息
            break;
        case ("配置名称"):
            console.log($(val).text());
            $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr, .print_add, .print_change, .print_mgr,.goods_up,.code_mgr,.work_mgr,.wuLiu_mgr,.todo_mgr,.bind_mgr,.bomName_change,.bomName_add,.search_mgr,.warehouse_panel").css("display", "none");
            $(".bomName_mgr,.bomName_exhibit").css("display", "block");
            requestGetAllData("/admin/getBomName", getCallBackFunc);
            break;
        case ("业务工单"):
            $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr, .print_add, .print_change, .print_mgr,.goods_up,.code_mgr,.todo_mgr,.wuLiu_mgr,.bind_mgr,.bomName_mgr,.search_mgr,.warehouse_panel").css("display", "none");
            $(".work_mgr").css("display", "block");
            $("#startDate,#endDate,.search_work_input").val("");
            requestGetAllData("/admin/getAllBusWorkOrder", getCallBackFunc)
            break;
        case ("待办事项"):
            $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr, .print_add, .print_change, .print_mgr,.goods_up,.code_mgr,.work_mgr,.wuLiu_mgr,.bind_mgr,.bomName_mgr,.search_mgr,.warehouse_panel").css("display", "none");
            $(".todo_mgr").css("display", "block");
            requestGetAllData("/admin/getUpcoming", getCallBackFunc);
            break;
        case ("条码绑定"):
            console.log($(val).text());
            $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr, .print_add, .print_change, .print_mgr,.goods_up,.code_mgr,.work_mgr,.wuLiu_mgr,.todo_mgr,.bomName_mgr,.search_mgr,.warehouse_panel").css("display", "none");
            $(".bind_mgr").css("display", "block");
            requestGetAllData("/admin/getAllVersion", getCallBackFunc); // 从产品 Bom 表获取所有版本信息
            break;
        case ("出货统计"):
            $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr, .print_add, .print_change, .print_mgr,.goods_up,.code_mgr,.work_mgr,.wuLiu_mgr,.todo_mgr,.bomName_mgr,.search_mgr,.bind_mgr").css("display", "none");
            $(".warehouse_panel").css("display", "block");
            break;
        case ("工单统计"):
            console.log($(val).text());
            break;
        case ("返修统计"):
            console.log($(val).text());
            break;
        default:
            alert("非法操作~@i@!");
            break;
    }
}

// 用户信息展示表修改按钮事件
function change_user(val) {
    $(".user_exhibit").css("display", "none");
    $(".user_change").css("display", "block");
    $("#user_id").val($(val).parent().siblings().eq(0).text());
    $("#user_account").val($(val).parent().siblings().eq(1).text());
    $("#user_name").val($(val).parent().siblings().eq(2).text());
    $.ajax({
        type: "GET",
        url: "/admin/userType",
        dataType: "json",
        contentType: "application/json",
        data: {},
        success: (ret) => {
            if (ret["success"] !== "") {
                // 开始渲染添加用户功能下拉选择框
                var htmlStr = `<option value=""></option>`;
                for (var type in ret["success"]) {
                    htmlStr += `
                        <option value="` + ret["success"][type]["UtId"] + `">` + ret["success"][type]["type_name"] + `</option>
                    `
                }
                $("#changeUserType").empty().append(htmlStr);
            } else {
                alert(ret["errMsg"]);
            }
        }
    });
    // console.log($(val).parent().siblings().eq(3).text());
}

// 客户展示界面修改按钮事件
function change_client(val) {
    $(".client_exhibit").css("display", "none");
    $(".client_change").css("display", "block");
    $("#client_id").val($(val).parent().siblings().eq(0).text());
    $("#client_name").val($(val).parent().siblings().eq(1).text());
    $("#clientPhone").val($(val).parent().siblings().eq(2).text());
    $("#clientAddr").val($(val).parent().siblings().eq(3).text());
    $.ajax({
        type: "GET",
        url:"/admin/getSalePeople",
        dataType: "json",
        data:{},
        success: (dat) => {
            if (dat["success"] !== "") {
                // 开始渲染修改客户功能下拉选择框
                    // 开始渲染添加客户功能下拉选择框
                    var strHtml = `<option value=""></option>`
                    for (var sale in dat["success"]) {
                        strHtml += `
                            <option value="`+dat["success"][sale]["uid"]+`">`+dat["success"][sale]["user_name"]+`</option>
                    `
                    }
                    $("#pro_work_change").empty().append(strHtml);
            } else {
                alert(dat["errMsg"]);
            }
        }
    });
}

// 物料展示界面修改按钮事件
function change_mater(val) {
    requestGetAllData("/admin/getAllMaterType", getCallBackFunc)
    $(".mater_exhibit").css("display", "none");
    $(".mater_change").css("display", "block");
    $("#mater_id").val($(val).parent().siblings().eq(0).text());
    $("#mater_name").val($(val).parent().siblings().eq(1).text());
    $("#mater_brand").val($(val).parent().siblings().eq(2).text());
    $("#mater_model").val($(val).parent().siblings().eq(3).text());
    $("#mater_cfg").val($(val).parent().siblings().eq(4).text());
}

// 打印机展示界面修改按钮事件
function change_print(val) {
    $(".print_exhibit").css("display", "none");
    $(".print_change").css("display", "block");
    $("#print_id").val($(val).parent().siblings().eq(0).text());
    $("#print_Name").val($(val).parent().siblings().eq(1).text());
    $("#print_addr").val($(val).parent().siblings().eq(2).text());
    $("#bt_server").val($(val).parent().siblings().eq(3).text());
}

// 用户展示页面删除用户按钮事件
function del_user(val) {
    // $(".user_exhibit").css("display", "none");
    $(".user_send_con").css("display", "block");
    $("#del_user_id").text($(val).parent().siblings().eq(0).text());
    $("#del_user_account").text($(val).parent().siblings().eq(1).text());
    $("#del_user_name").text($(val).parent().siblings().eq(2).text());
    // console.log($(val).parent().siblings().eq(0).text());
    // console.log($(val).parent().siblings().eq(1).text());
    // console.log($(val).parent().siblings().eq(2).text());
    // console.log($(val).parent().siblings().eq(3).text());
}

// 客户展示页面删除用户按钮事件
function del_client(val) {
    $(".client_send_con").css("display", "block");
    $("#del_client_id").text($(val).parent().siblings().eq(0).text());
    $("#del_client_name").text($(val).parent().siblings().eq(1).text());
    $("#del_client_phone").text($(val).parent().siblings().eq(2).text());
    $("#del_client_addr").text($(val).parent().siblings().eq(3).text());
}

// 物料展示页面删除用户按钮事件
function del_mater(val) {
    $(".mater_send_con").css("display", "block");
    $("#del_mater_id").text($(val).parent().siblings().eq(0).text());
    $("#del_mater_name").text($(val).parent().siblings().eq(1).text());
    $("#del_mater_brand").text($(val).parent().siblings().eq(2).text());
    $("#del_mater_model").text($(val).parent().siblings().eq(3).text());
    $("#del_mater_cfg").text($(val).parent().siblings().eq(4).text());
}

// 打印机展示页面删除用户按钮事件
function del_printer(val) {
    $(".print_send_con").css("display", "block");
    $("#del_print_id").text($(val).parent().siblings().eq(0).text());
    $("#del_print_name").text($(val).parent().siblings().eq(1).text());
    $("#del_print_addr").text($(val).parent().siblings().eq(2).text());
    $("#del_bt_addr").text($(val).parent().siblings().eq(3).text());
}

// 修改用户提交按钮事件
function change_user_submit() {
    putFunc("/admin/change_user", {
        "userId": $("#user_id").val(),
        "userAccount": $("#user_account").val(),
        "userName": $("#user_name").val(),
        "userPwd": $("#user_pwd").val(),
        "userTypeId": $("#changeUserType").val(),
    }, putCallback);
}

// 修改客户按钮提交事件
function change_client_submit() {
    putFunc("/admin/change_client", {
        "client_id": $("#client_id").val(),
        "client_name": $("#client_name").val(),
        "client_phone": $("#clientPhone").val(),
        "client_addr": $("#clientAddr").val(),
    }, putCallback);
}

// 修改物料按钮提交事件
function change_mater_submit() {
    putFunc("/admin/change_mater", {
        "mater_id": $("#mater_id").val(),
        "mater_name": $("#mater_name").val(),
        "mater_brand": $("#mater_brand").val(),
        "mater_model": $("#mater_model").val(),
        "mTypeId" : $("#che_bom_name_mater").val(),
    }, putCallback);
}

// 修改打印机按钮提交事件
function change_print_submit() {
    putFunc("/admin/change_print", {
        "print_id": $("#print_id").val(),
        "print_name": $("#print_Name").val(),
        "print_addr": $("#print_addr").val(),
        "bt_server": $("#bt_server").val(),
    }, putCallback);
}

// 添加用户按钮事件
function addUser() {
    $(".user_add").css("display", "block");
    $(".user_change,.user_exhibit").css("display", "none");
    $.ajax({
        type: "GET",
        url: "/admin/userType",
        dataType: "json",
        contentType: "application/json",
        data: {},
        success: (ret) => {
            if (ret["success"] !== "") {
                // 开始渲染添加用户功能下拉选择框
                var htmlStr = `<option value=""></option>`;
                for (var type in ret["success"]) {
                    htmlStr += `
                        <option value="` + ret["success"][type]["UtId"] + `">` + ret["success"][type]["type_name"] + `</option>
                    `
                }
                $("#userType").empty().append(htmlStr);
            } else {
                alert(ret["errMsg"]);
            }
        }
    });
}

// 添加客户按钮事件
function addClient() {
    $(".client_add").css("display", "block");
    $(".client_change,.client_exhibit").css("display", "none");
    $.ajax({
        type: "GET",
        url:"/admin/getSalePeople",
        dataType: "json",
        data:{},
        success: (dat) => {
            if (dat["success"] !== "") {
                // 开始渲染添加客户功能下拉选择框
                var strHtml = `<option value=""></option>`
                for (var sale in dat["success"]) {
                    strHtml += `
                        <option value="`+dat["success"][sale]["uid"]+`">`+dat["success"][sale]["user_name"]+`</option>
                    `
                }
                $("#pro_work").empty().append(strHtml);
            } else {
                alert(dat["errMsg"]);
            }
        }
    })
}

// 添加物料按钮事件
function addMater() {
    requestGetAllData("/admin/getAllMaterType", getCallBackFunc)
    $(".mater_add").css("display", "block");
    $(".mater_change,.mater_exhibit").css("display", "none");
    $.ajax({
        type: "GET",
        url: "/admin/getAllMaterType",
        dataType: "json",
        data: {},
        success: (dat) => {
            if (dat["success"] !== "") {
                var mtHtml = `
                    <option value="" disabled selected hidden>若添加的物料为成品或半成品请选择配置名称，基础物料则无需选择</option>
                    <option value=""></option>
                `
                for (var mate in dat["success"]) {
                    mtHtml += `
                        <option value="`+dat["success"][mate]["mt_id"]+`">`+dat["success"][mate]["MaterTypeName"]+`</option>
                    `
                }
                $("#add_bom_name_mater").empty().append(mtHtml);
            } else {
                alert(dat["errMsg"]);
            }
        }
    })
}

// 添加打印机按钮事件
function addPrint() {
    $(".print_add").css("display", "block");
    $(".print_change,.print_exhibit").css("display", "none");
}

// 添加用户提交按钮事件
function add_user_submit() {
    if ($("#user_passwd").val() !== $("#user_repasswd").val()) {
        $("#add_user_error").css("display", "block").empty().html("两次输入的密码不匹配，请重新输入!").delay(6000).fadeOut();
        return
    }
    postSubmit("/admin/add_user", {
        "login_account": $("#login_account").val(),
        "userName": $("#userName").val(),
        "user_passwd": $('#user_passwd').val(),
        "user_repasswd": $('#user_repasswd').val(),
        "user_type_id": $("#userType").val(),
    }, postCallFunc);
}

// 添加客户提交按钮事件
function add_client_submit() {
    postSubmit("/admin/add_client", {
        "client_name": $("#clientName").val(),
        "client_phone": $("#client_phone").val(),
        "client_addr": $("#client_addr").val(),
    }, postCallFunc);
}

// 添加物料提交按钮事件
function add_mater_submit() {
    if ($('#materName').val() !== "" || $('#brand').val() !== "" || $('#materModel').val() !== "") {
        postSubmit("/admin/add_mater", {
            "mater_name": $("#materName").val(),
            "brand": $("#brand").val(),
            "mater_model": $("#materModel").val(),
            "mTypeId" : $("#add_bom_name_mater").val(),
        }, postCallFunc);
    } else {
        alert("物料名称、物料品牌、规格型号不允许为空，返回充填！");
    }
}

// 添加打印机提交按钮事件
function add_print_submit() {
    postSubmit("/admin/add_printer", {
        "print_name": $("#printName").val(),
        "print_addr": $("#printAddr").val(),
        "bt_server_addr": $("#BTServer").val(),
    }, postCallFunc);
}

// 点击弹窗关闭按钮事件
$(".close").click(function () {
    switch ($(this).parent().attr("class")) {
        case ("user_send_info"):
            $(".user_send_con").css("display", "none");
            break;
        case ("client_send_info"):
            $(".client_send_con").css("display", "none");
            break;
        case ("mater_send_info"):
            $(".mater_send_con").css("display", "none");
            break;
        case ("print_send_info"):
            $(".print_send_con").css("display", "none");
            break;
        case ("upcoming_send_info"):
            $(".upcoming_send_con").css("display", "none");
            break;
        case ("work_send_info"):
            $(".work_send_con").css("display", "none");
            break;
        case ("wuLiu_send_info"):
            $(".wuLiu_send_con").css("display", "none");
            break;
        case ("bomName_send_info"):
            $(".bomName_send_con").css("display", "none");
            break;
        case ("rePrint_send_info"):
            $(".rePrint_send_con").css("display", "none");
            break;
        default:
            alert("非法操作~~~");
            break;
    }
    // 删除后重新获取所有用户数据进行展示
});

// 删除弹窗确认按钮事件
function del_info(val) {
    switch ($(val).siblings("div").attr("class")) {
        case ("del_user_info"):
            console.log("删除用户数据操作~~~!");
            delDataEdit("/admin/del_user", {
                "userId": $("#del_user_id").text(),
                "account": $("#del_user_account").text(),
                "username": $("#del_user_name").text(),
            }, deleteCallBack)
            break;
        case ("del_client_info"):
            console.log("删除客户数据操作~~~!");
            delDataEdit("/admin/del_client", {
                "client_id": $("#del_client_id").text(),
                "client_name": $("#del_client_name").text(),
                "client_phone": $("#del_client_phone").text(),
                "client_addr": $("#del_client_addr").text(),
            }, deleteCallBack);
            break;
        case ("del_mater_info"):
            console.log("删除物料操作~~~!");
            delDataEdit("/admin/del_mater", {
                "materId": $("#del_mater_id").text(),
                "materName": $("#del_mater_name").text(),
                "materBrand": $("#del_mater_brand").text(),
                "materModel": $("#del_mater_model").text(),
            }, deleteCallBack);
            break;
        case ("del_print_info"):
            console.log("删除打印机配置操作~~~！");
            delDataEdit("/admin/del_printer", {
                "printId": $("#del_print_id").text(),
                "printName": $("#del_print_name").text(),
                "printAddr": $("#del_print_addr").text(),
                "BTServerAddr": $("#del_bt_addr").text(),
            }, deleteCallBack)
            break;
        case "del_work_info":
            switch ($(val).text()) {
                case "确认派单":
                    console.log("委派工单操作~~~!");
                    // alert($('#weixiu').val())
                    var reqDat = {}
                    var outFirm = $(val).siblings(".del_work_info").children("ul").children("#outFirm").children("select[name='outFirm']").val();
                    // 确认派单操作后，根据工单 ID 更新工单状态为 2, 并且为事件表中新增一条数据关联到对应的维修人员ID
                    if ($(val).siblings(".del_work_info").children("ul").children("li").children("select[name='weixiu']").val() === "") {
                        alert("维修人员信息不允许为空，请重新选择后提交！");
                        return;
                    } else if (outFirm === "") {
                        alert("外发企业信息不允许留空，请重新填写后提交！");
                        return;
                    }
                    if ($('#weixiu').val() === "88") {
                        // 表示外发单
                        reqDat = {
                            "editType": 3,
                            "workId": $(val).siblings(".del_work_info").children("ul").children("li").eq(0).children("span").text(),
                            "peopleId": $(val).siblings(".del_work_info").children("ul").children("li").children("select[name='weixiu']").val(),
                            "outFirm": outFirm,
                            "outFirmNum": $(".out_firm_num").val(),
                        }
                    } else {
                        // 表示内修单
                        reqDat = {
                            "editType": 2,
                            "workId": $(val).siblings(".del_work_info").children("ul").children("li").eq(0).children("span").text(),
                            "peopleId": $(val).siblings(".del_work_info").children("ul").children("li").children("select[name='weixiu']").val(),
                            "outFirm": "",
                            "outFirmNum": "",
                        }
                    }
                    console.log(reqDat);
                    $.ajax({
                        type: "PUT",
                        url: "/admin/changeWorkState",
                        dataType: "json",
                        data: reqDat,
                        success: (dat) => {
                            if (dat["success"] !== "") {
                                // $(".work_send_con").css("display", "none");
                                window.location.href = "/admin";
                                // 重新查找渲染工单页面???
                            } else {
                                showWorkErr(dat["errMsg"]);
                            }
                        },
                    });
                    break;
                case "确认发货":
                    console.log("确认发货操作~~~!");
                    var liLen = $(val).siblings(".del_work_info").children().children("li").length;
                    var kudidiOld = $(val).siblings(".del_work_info").children().children("li").eq(liLen - 3).children("select").val();
                    var kuaidiNum = $(val).siblings(".del_work_info").children().children("li").eq(liLen - 2).children("input").val();
                    var retCount = $(val).siblings(".del_work_info").children().children("li").eq(liLen - 1).children("input").val();
                    if (kudidiOld === "" || kuaidiNum === "" || retCount === "") {
                        alert("错误！!快递公司和快递单号以及发出数量都不允许为空，请返回重新填写!");
                        return;
                    }
                    $.ajax({
                        type: "PUT",
                        url: "/admin/sendWorkState",
                        dataType: "json",
                        data: {
                            "editType": 5,
                            "workId": $(val).siblings(".del_work_info").children().children("li").eq(liLen - liLen).children("span").text(),
                            "kuaidiId": kudidiOld,
                            "kuaidiNum": kuaidiNum,
                            "retCount": retCount,
                        },
                        success: (ddt) => {
                            if (ddt["success"] !== "") {
                                window.location.href="/admin";
                            } else {
                                alert(ddt["errMsg"]);
                            }
                        },
                    });
                    break;
                case "确认收货":
                    console.log("确认收货操作~~~!")
                    let cost = $("#cost").val();
                    if (!isNumber(cost)) {
                        alert("输入的费用数据格式错误");
                        return;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/admin/changeWorkAndIstPay",
                        dataType: "json",
                        data: JSON.stringify({
                            "editType": 1,
                            "wid": $('#del_work_id').text(),
                            "model": $('#del_work_model').text(),
                            "cName": $('#del_yewu').text(),
                            "workType": $("#work_type").text(),
                            "cost": cost
                        }),
                        success:(result) => {
                            if (result["success"] !== "") {
                                window.location.href = "/admin";
                            } else {
                                alert(result["errMsg"]);
                            }
                        }
                    });
                    break;
                default:
                    console.log("删除业务工单操作~~~！");
                    delDataEdit("/admin/del_work", {
                        "workId": $("#del_work_id").text(),
                        "mateId": $("#del_work_name").text(),
                        "mateModel": $("#del_work_model").text(),
                        "KDNum": $("#del_kuaidci_num").text(),
                        "cName": $("#del_clientName").text(),
                        "YW": $("#del_yewu").text(),
                    }, deleteCallBack);
            }
            break;
        case ("del_wuLiu_info"):
            console.log("删除物流操作~~~！");
            delDataEdit("/admin/del_wuLiu", {
                "wuLiuId": $("#del_wuLiu_id").text(),
                "wuLiuName": $("#del_wuLiu_name").text(),
                "buttPeople": $("#del_wuLiu_phone").text(),
                "buttPhone": $("#del_wuLiu_addr").text(),
            }, deleteCallBack)
            break;
        default:
            alert("非法操作!");
            break;
    }
}

// 搜索按钮功能事件
function searchDat() {
    $(".search_mgr,.search_exhibit").css("display", "block");
    $(".user_mgr, .client_mgr, .mater_add,.bomName_mgr, .mater_change, .mater_mgr, .print_add, .print_change, .print_mgr,.goods_up,.code_mgr,.work_mgr,.wuLiu_mgr,.todo_mgr,.bind_mgr,.bomName_change,.bomName_add,.warehouse_panel").css("display", "none");
    $.ajax({
        type:"GET",
        url: "/admin/search",
        dataType: "json",
        data: {"searchKey":$(".search_input").val()},
        success: (ret) => {
            if (ret["success"] !== "") {
                var sHtml = ``
                for (var val in ret["success"]["ret"]) {
                    if (ret["success"]["ret"][val]["barcode"] === ret["success"]["schCode"]) {
                        sHtml += `
                             <tr>
                                <td style="background-color: #CCFF99;user-select: text;">` + ret["success"]["ret"][val]["mid"] + `</td>
                                <td style="background-color: #CCFF99;user-select: text;">` + ret["success"]["ret"][val]["mname"] + `</td>
                                <td style="background-color: #CCFF99;user-select: text;">` + ret["success"]["ret"][val]["brand"] + `</td>
                                <td style="background-color: #CCFF99;user-select: text;">` + ret["success"]["ret"][val]["model"] + `</td>
                                <td style="background-color: #CCFF99;user-select: text;">` + ret["success"]["ret"][val]["pro_time"] + `</td>
                                <td style="background-color: #CCFF99;user-select: text;">` + ret["success"]["ret"][val]["barcode"] + `</td>
                                <td style="background-color: #CCFF99;user-select: text;">` + ret["success"]["ret"][val]["outlib_date"] + `</td>
                                <td style="background-color: #CCFF99;user-select: text;"><span onclick="searchPelBtn(this)" class="searchPelBtn">重打条码</span></td>
                            </tr>
                        `
                    } else {
                        sHtml += `
                             <tr>
                                <td style="user-select: text;">` + ret["success"]["ret"][val]["mid"] + `</td>
                                <td style="user-select: text;">` + ret["success"]["ret"][val]["mname"] + `</td>
                                <td style="user-select: text;">` + ret["success"]["ret"][val]["brand"] + `</td>
                                <td style="user-select: text;">` + ret["success"]["ret"][val]["model"] + `</td>
                                <td style="user-select: text;">` + ret["success"]["ret"][val]["pro_time"] + `</td>
                                <td style="user-select: text;">` + ret["success"]["ret"][val]["barcode"] + `</td>
                                <td style="user-select: text;">` + ret["success"]["ret"][val]["outlib_date"] + `</td>
                                <td style="user-select: text;"><span onclick="searchPelBtn(this)" class="searchPelBtn">重打条码</span></td>
                            </tr>
                        `
                    }
                }
                $("#search_scrollTable > div > .tbody > .table_data").empty().append(sHtml);
                $('.search_input').val("").focus();
            } else {
                $(".searchTishi > span").empty().text(ret["errMsg"]).css("display", "block").delay(6000).fadeOut();
            }
        }
    });
}

// 上传文件按钮事件
function uploadFile() {
    let formData = new FormData();
    const fileList = $("#fileInput")[0].files;
    if (fileList.length < 1) {
        $("#fileTishi").css("display", "block").empty().text("尚未选择任何模板！").delay(6000).fadeOut();
        return;
    }
    for (const item of fileList) {
        formData.append("file", item);
    }
    $.ajax({
        type: "POST",
        url: "/admin/uploadFile",
        contentType:false,
        processData:false,
        data: formData,
        success: (rep) => {
            if (rep["success"] !== "") {
                $("#fileTishi").css("display", "block").empty().text("导入成功").delay(6000).fadeOut();
            } else {
                $("#fileTishi").css("display", "block").empty().text(rep["errMsg"]).delay(6000).fadeOut();
            }
        }
    })
}

// 重打条码按钮事件
function searchPelBtn(val) {
    $(".rePrint_send_con").css("display", "block");
    $("#del_rePrint_name").empty().text($(val).parent("td").siblings("td").eq(1).text());
    $("#del_rePrint_model").empty().text($(val).parent("td").siblings("td").eq(2).text());
    $("#del_rePrint_num").empty().text($(val).parent("td").siblings("td").eq(3).text());
    $("#del_rePrintName").empty().text($(val).parent("td").siblings("td").eq(4).text());
    $("#rePrint_type").empty().text($(val).parent("td").siblings("td").eq(5).text());
    $("#rePrint_yewu").empty().text($(val).parent("td").siblings("td").eq(6).text());
    $.ajax({
        type: "get",
        url: "/admin/PrintGetAllPrinter",
        dataType: "json",
        data: {},
        success: (dat) => {
            if (dat["success"] !== "") {
                let printHtml = `
                    <option value=""></option>
                `
                for (let i in dat["success"]) {
                    printHtml += `
                        <option value="`+dat["success"][i]["pid"]+`">`+dat["success"][i]["p_name"]+`</option>
                    `
                    $("#printerName").empty().append(printHtml);
                }
            } else {
                alert(dat["success"]);
            }
        }
    });
}

// rePrint_info 重打条码弹框确认打印按钮事件
function rePrint_info(v) {
    $(".rePrint_send_con").css("display", "none");
    if ($('#printerName').val() === "") {
        alert("尚未指定打印机!");
        return;
    }
    $(".searchTishi > span").empty().text("已将数据数据到打印服务器打印中，请稍等......").css("display", "block").delay(8000).fadeOut();
    $.ajax({
        type: "get",
        url: "/admin/rePrintCode",
        dataType: "json",
        data: {"code": $("#rePrint_type").text(), "printer": $("#printerName").val()},
        success: (dat) => {
            if (dat["success"] !== "") {
                $(".searchTishi > span").empty().text(dat["success"]).css("display", "block").delay(4000).fadeOut();
            } else {
                $(".searchTishi > span").empty().text(dat["errMsg"]).css("display", "block").delay(6000).fadeOut();
            }
        }
    });
}

// 物料管理页面搜索按钮
function searchMaterDat() {
    $.ajax({
        type: "get",
        url: "/admin/searchMater",
        dataType: "json",
        data: {"mater": $(".search_mater_input").val()},
        success: (val) => {
            if (val["success"] !== "") {
                $(".search_mater_input").val("");
                let materHtml = ``
                for (let mater in val["success"]) {
                    materHtml += `
                    <tr>
                        <td>` + val["success"][mater]["mid"] + `</td>
                        <td>` + val["success"][mater]["mname"] + `</td>
                        <td>` + val["success"][mater]["brand"] + `</td>
                        <td>` + val["success"][mater]["model"] + `</td>
                        <td>` + val["success"][mater]["mater_type_name"] + `</td>
                        <td><span class="change_tab" onclick="change_mater(this)">修改</span> | <span
                                class="del_tab" onclick="del_mater(this)">删除</span></td>
                    </tr>
                    `
                }
                $(".mater_exhibit > #mater_scrollTable > div > .tbody > .table_data").empty().append(materHtml);
            } else {
                $(".materErr > #materErrShow").css("display", "block").empty().text(val["errMsg"]).delay(6000).fadeOut();
            }
        }
    });
}

// 实时监测物料展示面板搜索输入框输入框变化
$('.search_mater_input').change(() => {
    if ($(".search_mater_input").val() === "") {
        requestGetAllData("/admin/getAllMater", getCallBackFunc);
    }
});

// 点击按钮清空输入框内容
rmIptDat = () => {
    $(".search_input").val("").focus()
}

// 监控搜索输入框数据变化
$(()=>{
    $(".search_input").on("input propertychange", ()=>{
        setTimeout(()=>{
            if ($(".search_input").val().length >= 27) {
                searchDat();
            }
        },500);
    });
});