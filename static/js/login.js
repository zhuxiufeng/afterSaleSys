$(".button_box").click(() => {
    $.ajax({
        type: "POST",
        url: "/sign",
        data: {
            "name": $(".input_box[type='text']").val(),
            "passwd": $(".input_box[type='password']").val(),
            "learnMe": $(".learn > input[name='learnMe']").val(),
        },
        success: function (dat) {
            if (dat["success"]) {
                // $.cookie();
                // $.cookie('LIANLISESSIONID20220603','',{path:'/admin'});
                window.location.href = "/admin";
            }
            if (dat["errMsg"]) {
                $(".err_box > span").css("display", "block").empty().html(dat["errMsg"]).delay(6000).fadeOut();
            }
        },
    });
});