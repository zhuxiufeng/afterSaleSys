function choose_work(btn) {
    switch ($(btn).text()) {
        case "返厂中":
            $(".return_work,.recv_work,.repair_work,.all_work").css("display", "none");
            $(".send_work").css("display", "block");
            getWorkData(0,workCallBackFunc);
            break;
        case "已接收":
            $(".send_work,.return_work,.repair_work,.all_work").css("display", "none");
            $(".recv_work").css("display", "block");
            getWorkData(1,workCallBackFunc);
            break;
        case "维修中":
            $(".send_work,.recv_work,.return_work,.all_work").css("display", "none");
            $(".repair_work").css("display", "block");
            getWorkData(2, workCallBackFunc);
            break;
        case "待返客":
            $(".send_work,.recv_work,.repair_work,.all_work").css("display", "none");
            $(".return_work").css("display", "block");
            getWorkData(4, workCallBackFunc);
            break;
        case "所    有":
            $(".send_work,.recv_work,.repair_work,.return_work").css("display", "none");
            $(".all_work").css("display", "block");
            getWorkData("",workCallBackFunc);
            break;
        default:
            alert("非法操作~~~！");
    }
}

// 根据业务工单状态获取工单数据
function getWorkData(workId, callBack) {
    $.ajax({
        type: "GET",
        url: "/admin/getWorkData",
        contentType: "application/json",
        dataType: "json",
        data: {"workId": workId},
        success: function (dat) {
            if  (dat["success"] !== "") {
                callBack(workId, dat)
            } else {
                $("#work_err").empty().css("display", "block").html(dat["errMsg"]).delay(6000).fadeOut();
            }
        }
    });
}

// getWorkData 请求方法回调函数
function workCallBackFunc(dat, ret) {
    if (dat === 0) {
        // 开始渲染 $(".send_work > div > table > .table_data") 下的表格数据
        var sendWork = ``;
        for (var send in ret["success"]) {
            sendWork += `
                        <tr>
                            <td>` + ret["success"][send]["wid"] + `</td>
                            <td>` + ret["success"][send]["mname"] + `</td>
                            <td>` + ret["success"][send]["model"] + `</td>
                            <td>` + ret["success"][send]["issue_describe"] + `</td>
                            <td>` + ret["success"][send]["mater_num"] + `</td>
                            <td>` + ret["success"][send]["create_date"] + `</td>
                            <td>` + ret["success"][send]["log_num"] + `</td>
                            <td>` + ret["success"][send]["work_type"] + `</td>
                            <td>` + ret["success"][send]["client_name"] + `</td>
                            <td>` + ret["success"][send]["user_name"] + `</td>
                            <td>` + ret["success"][send]["work_state"] + `</td>`
                            if (ret["success"][send]["work_type"] === "返修工单") {
                                sendWork += `<td>---</td>`
                            } else {
                                sendWork += `<td>` + ret["success"][send]["pay_status"] + `</td>`
                            }
                            sendWork += `
                            <td>` + ret["success"][send]["out_firm_name"] + `</td>
                            <td>` + ret["success"][send]["o_number"] + `</td>
                            <td>` + ret["success"][send]["crate_date"] + `</td>
                            <td>` + ret["success"][send]["express_num"] + `</td>`
                            // <!-- 此处渲染时需要判断状态并显示不同的操作内容，如 0-接收、1-派单、2-退仓、3-发货、4-完成 -->
                            sendWork += `<td><span class="change_tab" onclick="etid_btn(this)">接收</span> | <span
                                    class="del_tab" onclick="del_work(this)">删除</span></td>
                        </tr>
                    `
        }
        $(".send_work > div > .tbody > .table_data").empty().append(sendWork);
    } else if (dat === 1) {
        // 开始渲染 $(".recv_work > div > table > .table_data") 下的表格数据
        var recvWork = ``;
        for (var recv in ret["success"]) {
            recvWork += `
                        <tr>
                            <td>` + ret["success"][recv]["wid"] + `</td>
                            <td>` + ret["success"][recv]["mname"] + `</td>
                            <td>` + ret["success"][recv]["model"] + `</td>
                            <td>` + ret["success"][recv]["issue_describe"] + `</td>
                            <td>` + ret["success"][recv]["mater_num"] + `</td>
                            <td>` + ret["success"][recv]["create_date"] + `</td>
                            <td>` + ret["success"][recv]["log_num"] + `</td>
                            <td>` + ret["success"][recv]["work_type"] + `</td>
                            <td>` + ret["success"][recv]["client_name"] + `</td>
                            <td>` + ret["success"][recv]["user_name"] + `</td>
                            <td>` + ret["success"][recv]["work_state"] + `</td>`
                            if (ret["success"][recv]["work_type"] === "返修工单") {
                                recvWork += `<td>---</td>`
                            } else {
                                recvWork += `<td>` + ret["success"][recv]["pay_status"] + `</td>`
                            }
                            recvWork += `
                            <td>` + ret["success"][recv]["out_firm_name"] + `</td>
                            <td>` + ret["success"][recv]["o_number"] + `</td>
                            <td>` + ret["success"][recv]["crate_date"] + `</td>
                            <td>` + ret["success"][recv]["express_num"] + `</td>`
                            // <!-- 此处渲染时需要判断状态并显示不同的操作内容，如 0-接收、1-派单、2-退仓、3-发货、4-完成 -->
                            recvWork += `
                                <td><span class="change_tab" onclick="etid_btn(this)">派单</span> | <span class="del_tab" onclick="del_work(this)">删除</span></td>
                        </tr>`
        }
        $(".recv_work > div > .tbody > .table_data").empty().append(recvWork);
    } else if (dat === 2 || dat === 3) {
        var repairWork = ``;
        for (var repair in ret["success"]) {
            repairWork += `
                        <tr>
                            <td>` + ret["success"][repair]["wid"] + `</td>
                            <td>` + ret["success"][repair]["mname"] + `</td>
                            <td>` + ret["success"][repair]["model"] + `</td>
                            <td>` + ret["success"][repair]["issue_describe"] + `</td>
                            <td>` + ret["success"][repair]["mater_num"] + `</td>
                            <td>` + ret["success"][repair]["create_date"] + `</td>
                            <td>` + ret["success"][repair]["log_num"] + `</td>
                            <td>` + ret["success"][repair]["work_type"] + `</td>
                            <td>` + ret["success"][repair]["client_name"] + `</td>
                            <td>` + ret["success"][repair]["user_name"] + `</td>
                            <td>` + ret["success"][repair]["work_state"] + `</td>`
                            if (ret["success"][repair]["work_type"] === "返修工单") {
                                repairWork += `<td>---</td>`
                            } else {
                                repairWork += `<td>` + ret["success"][repair]["pay_status"] + `</td>`
                            }
                            repairWork += `
                            <td>` + ret["success"][repair]["out_firm_name"] + `</td>
                            <td>` + ret["success"][repair]["o_number"] + `</td>
                            <td>` + ret["success"][repair]["crate_date"] + `</td>
                            <td>` + ret["success"][repair]["express_num"] + `</td>`
                            // <!-- 此处渲染时需要判断状态并显示不同的操作内容，如 0-接收、1-派单、2-退仓、3-发货、4-完成 -->
                            repairWork += `
                                <td><span class="change_tab" onclick="etid_btn(this)">退仓</span> | <span
                                    class="del_tab" onclick="del_work(this)">删除</span></td>
                        </tr>
                    `
        }
        $(".repair_work > div > .tbody > .table_data").empty().append(repairWork);
    }else if (dat === 4) {
        var returnWork = ``;
        for (var reqt in ret["success"]) {
            returnWork += `
                        <tr>
                            <td>` + ret["success"][reqt]["wid"] + `</td>
                            <td>` + ret["success"][reqt]["mname"] + `</td>
                            <td>` + ret["success"][reqt]["model"] + `</td>
                            <td>` + ret["success"][reqt]["issue_describe"] + `</td>
                            <td>` + ret["success"][reqt]["mater_num"] + `</td>
                            <td>` + ret["success"][reqt]["create_date"] + `</td>
                            <td>` + ret["success"][reqt]["log_num"] + `</td>
                            <td>` + ret["success"][reqt]["work_type"] + `</td>
                            <td>` + ret["success"][reqt]["client_name"] + `</td>
                            <td>` + ret["success"][reqt]["user_name"] + `</td>
                            <td>` + ret["success"][reqt]["work_state"] + `</td>`
                            if (ret["success"][reqt]["work_type"] === "返修工单") {
                                returnWork += `<td>---</td>`
                            } else {
                                returnWork += `<td>` + ret["success"][reqt]["pay_status"] + `</td>`
                            }
                            returnWork += `
                            <td>` + ret["success"][reqt]["out_firm_name"] + `</td>
                            <td>` + ret["success"][reqt]["o_number"] + `</td>
                            <td>` + ret["success"][reqt]["crate_date"] + `</td>
                            <td>` + ret["success"][reqt]["express_num"] + `</td>`
                            // <!-- 此处渲染时需要判断状态并显示不同的操作内容，如 0-接收、1-派单、2-退仓、3-发货、4-完成 -->
                            returnWork += `
                                    <td><span class="change_tab" onclick="etid_btn(this)">发货</span> | <span
                                    class="del_tab" onclick="del_work(this)">删除</span></td>
                        </tr>
                    `
        }
        $(".return_work > div > .tbody > .table_data").empty().append(returnWork);
    } else {
        var allWork = ``;
        for (var work in ret["success"]) {
            allWork += `
                        <tr>
                            <td>` + ret["success"][work]["wid"] + `</td>
                            <td>` + ret["success"][work]["mname"] + `</td>
                            <td>` + ret["success"][work]["model"] + `</td>
                            <td>` + ret["success"][work]["issue_describe"] + `</td>
                            <td>` + ret["success"][work]["mater_num"] + `</td>
                            <td>` + ret["success"][work]["create_date"] + `</td>
                            <td>` + ret["success"][work]["log_num"] + `</td>
                            <td>` + ret["success"][work]["work_type"] + `</td>
                            <td>` + ret["success"][work]["client_name"] + `</td>
                            <td>` + ret["success"][work]["user_name"] + `</td>
                            <td>` + ret["success"][work]["work_state"] + `</td>`
                            if (ret["success"][work]["work_type"] === "返修工单") {
                                allWork += `<td>---</td>`
                            } else {
                                allWork += `<td>` + ret["success"][work]["pay_status"] + `</td>`
                            }
                            allWork += `
                            <td>` + ret["success"][work]["out_firm_name"] + `</td>
                            <td>` + ret["success"][work]["o_number"] + `</td>
                            <td>` + ret["success"][work]["crate_date"] + `</td>
                            <td>` + ret["success"][work]["express_num"] + `</td>`
                            if (ret["success"][work]["state"] === 0) {
                                allWork += `<td><span class="change_tab" onclick="etid_btn(this)">接收</span> | <span
                                                    class="del_tab" onclick="del_work(this)">删除</span></td>`
                            } else if (ret["success"][work]["state"] === 1) {
                                allWork += `<td><span class="change_tab" onclick="etid_btn(this)">派单</span> | <span
                                                    class="del_tab" onclick="del_work(this)">删除</span></td>`
                            } else if (ret["success"][work]["state"] === 2 || ret["success"][work]["state"] === 3) {
                                allWork += `<td><span class="change_tab" onclick="etid_btn(this)">退仓</span> | <span
                                                    class="del_tab" onclick="del_work(this)">删除</span></td>`
                            } else if (ret["success"][work]["state"] === 4) {
                                allWork += `<td><span class="change_tab" onclick="etid_btn(this)">发货</span> | <span
                                                    class="del_tab" onclick="del_work(this)">删除</span></td>`
                            } else {
                                allWork += `<td><span class="change_tab" style="color: #999;">已完结</span></td>`
                            }
                            allWork += `</tr>`
        }
        $(".all_work > div > .tbody > .table_data").empty().append(allWork);
    }
}

$(()=>{
    // 监听派单界面指定维修人员选择框，如果选择的是外发，则发起 get 请求获取所有外发公司名单
    $("body").on("change", "#weixiu", ()=>{
        if ($('#weixiu').val() === "88") {
            $.ajax({
                type: "GET",
                url: "/admin/getOutFirm",
                dataType: "json",
                data: {},
                success: (ret) => {
                    if (ret["success"] !== "") {
                        var wxHtml = `
                            <option value=""></option>
                        `;
                        for (var people in ret["success"]) {
                            wxHtml += `
                                <option value="`+ret["success"][people]["oid"]+`">`+ret["success"][people]["out_firm_name"]+`</option>
                            `;
                        }
                        // wxHtml += `<option value="`+ret["success"].length + 1 +`">外发</option>`;
                        $("#out_firm").empty().append(wxHtml);
                    } else {
                        showWorkErr(ret["errMsg"]);
                    }
                }
            });
        }
    });
});

function getWxUsersAjax(url, callWxBackFunc) {
    var userLen = 0;
    $.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        data: {
            "userType": 6,
        },
        success: (rets) => {
            callWxBackFunc(rets);
            uLen = rets["success"].length;
        }
    });
}

function getWxUserCallBackFunc(dat) {
    if (dat["success"] !== "") {
        var wxuHml = `
                            <option value=""></option>
                        `;
        for (var user in dat["success"]) {
            wxuHml += `
                            <option value="`+dat["success"][user]["uid"]+`">`+dat["success"][user]["user_name"]+`</option>
                        `
        }
        wxuHml += `<option value="88">外发</option>`
        $("#weixiu").empty().append(wxuHml);
    } else {
        alert(dat["errMsg"]);
    }
}

// 操作工单按钮事件方法
function etid_btn(val) {
    if ($(val).parent("td").siblings().eq(0).text() === null) {
        showWorkErr("非法操作~~~！");
        return;
    }
    switch ($(val).text()) {
        case "派单":
            $("#outFirm,#outFirmNum").css("display", "none");
            // 发起 get 请求获取所有人员信息
            getWxUsersAjax("/admin/getUsers", getWxUserCallBackFunc)
            $(".work_send_mask").css("background", "white", "opacity", "0.2");
            $(".work_send_con").css("display", "block");
            $("#work_warning").empty().html("您即将对以下工单进行派单操作：").css("color", "black");
            $(".work_send_info > button").empty().html("确认派单").css({"background": "Azure", "color": "black"});
            $("#del_work_id").text($(val).parent("td").siblings().eq(0).text());
            $("#del_work_name").text($(val).parent("td").siblings().eq(1).text());
            $("#del_work_model").text($(val).parent("td").siblings().eq(2).text());
            $("#del_kuaidci_num").text($(val).parent("td").siblings().eq(6).text());
            $("#del_clientName").text($(val).parent("td").siblings().eq(5).text());
            $("#work_type").text($(val).parent("td").siblings().eq(7).text());
            $("#del_yewu").text($(val).parent("td").siblings().eq(8).text());
            $(".weixiu,.kuaidi,.kuaidiId").remove();
            // 添加标签，处处应先从数据库获取人员类型为维修人员的信息，然后在此处循环展示
            $(".del_work_info > ul").append(`
                <li class="list-group-item weixiu"><strong>指定维修人员：</strong>
                    <select name="weixiu" id="weixiu"></select>
                </li>
            `);
            // 监控选择框的内容，如等于 4 则添加外发公司输入框
            $("select[name='weixiu']").change(()=>{
                if ($('#weixiu').val() === "88") {
                    $(".del_work_info > ul").append(`
                        <li class="list-group-item" id="outFirm">
                            <strong>外发企业名称：</strong>
                            <select name="outFirm" id="out_firm"></select>
                        </li>
                        <li class="list-group-item" id="outFirmNum">
                            <strong>外发单号：</strong>
                            <input type="text" placeholder="请输入外发物流单号" class="out_firm_num">
                        </li>
                    `);
                } else {
                    $("#outFirm, #outFirmNum").remove();
                }
            });
            break;
        case "发货":
            // todo 此处应现获取所有物流公司内容
            $(".weixiu,.kuaidi,.kuaidiId,#outFirm").remove();
            $(".work_send_mask").css("background", "white", "opacity", "0.2");
            $(".work_send_con").css("display", "block");
            $("#work_warning").empty().html("您即将对以下工单进行发货操作：").css("color", "black");
            $(".work_send_info > button").empty().html("确认发货").css({"background": "Azure", "color": "black"});
            $("#del_work_id").text($(val).parent("td").siblings().eq(0).text());
            $("#del_work_name").text($(val).parent("td").siblings().eq(1).text());
            $("#del_work_model").text($(val).parent("td").siblings().eq(2).text());
            $("#del_kuaidci_num").text($(val).parent("td").siblings().eq(6).text());
            $("#del_clientName").text($(val).parent("td").siblings().eq(5).text());
            $("#work_type").text($(val).parent("td").siblings().eq(7).text());
            $("#del_yewu").text($(val).parent("td").siblings().eq(8).text());
            $(".del_work_info > ul").append(`
                <li class="list-group-item kuaidi"><strong>指定快递公司：</strong>
                    <select name="kuaidi" id="kuaidi">
                        <option value=""></option>
                        <option value="1">顺丰</option>
                        <option value="2">韵达</option>
                        <option value="3">圆通</option>
                        <option value="4">达达</option>
                    </select>
                </li>
                <li class="list-group-item kuaidiId"><strong>输入快递单号：</strong>
                    <input type="text" placeholder="请输入快递单号" name="kuaidiNum">
                </li>
                <li class="list-group-item retNum"><strong>输入发出数量：</strong>
                    <input type="text" placeholder="输入发出数量" name="retNum">
                </li>
            `);
            break;
        default:
            let reqData  = {}
            if ($(val).text() === "接收") {
                let workType = $(val).parent("td").siblings("td").eq(7).text();
                //  TODO 此处判断接收的订单类型如果为服务工单则需弹框让售后人员输入维修价格,然后返会给客户工单信息中
                if (workType === "维修工单") {
                    $(".weixiu,.kuaidi,.kuaidiId,#outFirm").remove();
                    $(".work_send_mask").css("background", "white", "opacity", "0.2");
                    $(".work_send_con").css("display", "block");
                    $("#work_warning").empty().html("此工单为维修工单收货前请确认已于客户协商过维修费用等相关问题：").css("color", "black");
                    $(".work_send_info > button").empty().html("确认收货").css({"background": "Azure", "color": "black"});
                    $("#del_work_id").text($(val).parent("td").siblings().eq(0).text());
                    $("#del_work_name").text($(val).parent("td").siblings().eq(1).text());
                    $("#del_work_model").text($(val).parent("td").siblings().eq(2).text());
                    $("#del_kuaidci_num").text($(val).parent("td").siblings().eq(6).text());
                    $("#del_clientName").text($(val).parent("td").siblings().eq(5).text());
                    $("#work_type").text($(val).parent("td").siblings().eq(7).text());
                    $("#del_yewu").text($(val).parent("td").siblings().eq(8).text());
                    $(".del_work_info > ul").append(
                        `
                        <li class="list-group-item kuaidiId"><strong>维修费用：</strong>
                            <input type="text" id="cost" placeholder="请输入该商品本次的维修费用" name="kuaidiNum">
                        </li>
                        `
                    );
                    return;
                }
                reqData["editType"] = 1;
                reqData["workType"] = workType;
                reqData["workId"]= $(val).parent("td").siblings().eq(0).text();
            } else {
                reqData["editType"] = 4;
                reqData["workType"] = "";
                reqData["workId"]= $(val).parent("td").siblings().eq(0).text();
            }
            $.ajax({
                type: "PUT",
                url: "/admin/changeWork",
                // contentType: "application/json",
                dataType: "json",
                data: reqData,
                success: (dat) => {
                    if (dat["success"] !== "") {
                        window.location.href="/admin";
                    } else {
                        showWorkErr(dat["errMsg"]);
                    }
                }
            });
    }
}

function showWorkErr(str) {
    alert(str);
}

// 删除工单按钮事件方法
function del_work(val) {
    $(".weixiu,.kuaidi,.kuaidiId").remove();
    $(".work_send_con").css("display", "block");
    $(".work_send_mask").css("background", "darkorange", "opacity", "0.2");
    $("#work_warning").empty().html("警告！！！您即将删除以下内容信息：").css("color", "darkorange");
    $(".work_send_info > button").empty().html("确认删除").css({"background": "", "color": ""});
    if ($(val).parent("td").siblings().eq(0).text() === "") {
        showWorkErr("非法操作~~~");
        return;
    }
    $("#del_work_id").text($(val).parent("td").siblings().eq(0).text());
    $("#del_work_name").text($(val).parent("td").siblings().eq(1).text());
    $("#del_work_model").text($(val).parent("td").siblings().eq(2).text());
    $("#del_kuaidci_num").text($(val).parent("td").siblings().eq(4).text());
    $("#del_clientName").text($(val).parent("td").siblings().eq(5).text());
    $("#work_type").text($(val).parent("td").siblings().eq(7).text());
    $("#del_yewu").text($(val).parent("td").siblings().eq(8).text());
    // $.ajax({
    //     type: "DELETE",
    //     url: "/admin/delWork",
    //     dataType: "json",
    //     contentType: "application/json",
    //     data: {"workId": $(val).parent("td").siblings().eq(0).text()},
    //     success: (dat) => {
    //         if (dat["success"] !=="") {
    //             // 重新发送数据给前端，并在前端渲染展示工单信息
    //         } else {
    //             showErr(dat["errMsg"]);
    //         }
    //     }
    // });
}

// 工单展示页面搜索按钮事件
function searchWorkDat() {
    let serType = 0;
    let startDate = $("#startDate").val();
    let endDate = $("#endDate").val();
    let iptData = $(".search_work_input").val();
    if (iptData === "" && startDate === "" && endDate === "") {
        alert("未选择过滤条件或未输入需要搜索的内容");
        return;
    } else if (startDate === "" && endDate === "") {
        serType = 2;  // 按搜索内容进行搜索
    } else if (iptData === "") {
        serType = 1;  // 按时间进行过滤搜索
    }
    $.ajax({
        type: "post",
        url: "/admin/searchWorkInfo",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            "start_date": $('#startDate').val(),
            "end_date": $('#endDate').val(),
            "ipt_data": $('.search_work_input').val(),
            "ser_type": serType
        }),
        success:(rst) => {
            if (rst["success"] !== "") {
                $("#startDate,#endDate,.search_work_input").val("");
                var srcWork = ``;
                for (var work in rst["success"]) {
                    srcWork += `
                        <tr>
                            <td>` + rst["success"][work]["wid"] + `</td>
                            <td>` + rst["success"][work]["mname"] + `</td>
                            <td>` + rst["success"][work]["model"] + `</td>
                            <td>` + rst["success"][work]["issue_describe"] + `</td>
                            <td>` + rst["success"][work]["mater_num"] + `</td>
                            <td>` + rst["success"][work]["create_date"] + `</td>
                            <td>` + rst["success"][work]["log_num"] + `</td>
                            <td>` + rst["success"][work]["work_type"] + `</td>
                            <td>` + rst["success"][work]["client_name"] + `</td>
                            <td>` + rst["success"][work]["user_name"] + `</td>
                            <td>` + rst["success"][work]["work_state"] + `</td>
                            <td>` + rst["success"][work]["pay_status"] + `</td>
                            <td>` + rst["success"][work]["out_firm_name"] + `</td>
                            <td>` + rst["success"][work]["o_number"] + `</td>
                            <td>` + rst["success"][work]["crate_date"] + `</td>
                            <td>` + rst["success"][work]["express_num"] + `</td>`
                    if (rst["success"][work]["state"] === 0) {
                        srcWork += `<td><span class="change_tab" onclick="etid_btn(this)">接收</span> | <span
                                                    class="del_tab" onclick="del_work(this)">删除</span></td>`
                    } else if (rst["success"][work]["state"] === 1) {
                        srcWork += `<td><span class="change_tab" onclick="etid_btn(this)">派单</span> | <span
                                                    class="del_tab" onclick="del_work(this)">删除</span></td>`
                    } else if (rst["success"][work]["state"] === 2 || rst["success"][work]["state"] === 3) {
                        srcWork += `<td><span class="change_tab" onclick="etid_btn(this)">退仓</span> | <span
                                                    class="del_tab" onclick="del_work(this)">删除</span></td>`
                    } else if (rst["success"][work]["state"] === 4) {
                        srcWork += `<td><span class="change_tab" onclick="etid_btn(this)">发货</span> | <span
                                                    class="del_tab" onclick="del_work(this)">删除</span></td>`
                    } else {
                        srcWork += `<td><span class="change_tab" style="color: #999;">已完结</span></td>`
                    }
                    srcWork += `</tr>`
                }
                $(".all_work > div > .tbody > .table_data").empty().append(srcWork);
            } else {
                alert(rst["errMsg"]);
            }
        }
    });
}