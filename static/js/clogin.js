// 使用正则验证用书输入的手机号格式是否正确
function isPhone(value) {
    var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
    if (value === "" || value.length !== 11 || !mobile.test(value)) {
        return false
    } else {
        return true
    }
}

$(".button_box").click(() => {
    var username = $(".input_box[name='username']").val()
    if (username === "") {
        $(".err_box > span").css("display", "block").empty().html("登录手机号不允许为空，请重新填写，谢谢!").delay(6000).fadeOut();
    }
    if (!isPhone(username)) {
        $(".err_box > span").css("display", "block").empty().html("抱歉！您输入的联系方式格式有误，请重新填写，谢谢!").delay(6000).fadeOut();
        return
    }
    $.ajax({
        type: "POST",
        url: "/login",
        dataType: "json",
        data: {
            "username": username,
        },
        success: function (dat) {
            if (dat["success"] !== "") {
                window.location.href = "/";
            }
            if (dat["errMsg"]) {
                $(".err_box > span").css("display", "block").empty().html(dat["errMsg"]).delay(6000).fadeOut();
            }
        },
    });
});