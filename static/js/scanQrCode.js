let btnFlag = 0;

// 监听条码输入框事件
let flag = 0;
let reqCode = "";
$(() => {
    $("#scanRet").on("input propertychange", () => {
        setTimeout(() => {
            let lastCodeHeard = ""
            let lastSecond = ""
            var textDom = $('#scanRet');
            let scanNum = $("#scanNum");
            let outLibNum = $("#outLib");
            if (textDom.val() !== " ") {
                // console.log(textDom.val().trim().split(" "))
                textDom.val(textDom.val().trim() + " ");
                var datNum = textDom.val().trim().split(" ").length;
                // 获取最后一个输入的数据
                let lastInput = "";
                // console.log(lastInput)
                if (datNum >= 2) {
                    lastInput = textDom.val().trim().split(" ")[datNum - 1];
                    console.log(lastInput.length);
                    // 获取最后一个输入数据的第一个字符
                    lastCodeHeard = lastInput.substring(0, 1);
                    // 获取倒数第二个数据
                    lastSecond = textDom.val().trim().split(" ").slice(0, datNum - 1)[textDom.val().trim().split(" ").slice(0, datNum - 1).length - 1];
                    if (lastCodeHeard === "L" && lastInput !== lastSecond || lastCodeHeard === "W" && lastInput !== lastSecond || lastCodeHeard === "D" && lastInput !== lastSecond) {
                        scanNum.text(datNum);
                        // 如果当前的出库总数大于输入框的总数，说明有条码有子条码数量，则在原有数量的基础上再加上新扫入的条码数量，否则与扫入的条码数量保持一致
                        if (Number(outLibNum.text()) > datNum) {
                            if (lastCodeHeard === "L" && lastInput.length >= 31) {
                                outLibNum.text(Number(outLibNum.text()) + 1);
                            }
                        } else {
                            outLibNum.text(datNum);
                        }
                        CodeReqSonCodeNum(lastInput);
                    } else {
                        // outLibNum.text(Number(outLibNum.text()) + 1);
                        textDom.val(textDom.val().trim().split(" ").slice(0, datNum - 1).join(" ") + " ");
                    }
                } else {
                    lastInput = textDom.val().trim().split(" ")[datNum - 1];
                    lastCodeHeard = lastInput.substring(0, 1)
                    if (lastCodeHeard === "L" || lastCodeHeard === "W" || lastCodeHeard === "D") {
                        flag += 1;
                        if (flag === 1) {
                            scanNum.text(Number(scanNum.text()) + 1);
                            if (lastCodeHeard === "L") {
                                outLibNum.text(Number(outLibNum.text()) + 1);
                            }
                            CodeReqSonCodeNum(lastInput);
                        }
                    } else {
                        // outLibNum.text(Number(outLibNum.text()) + 1);
                        textDom.val("");
                    }
                }
            } else {
                textDom.val("");
                scanNum.empty().text("");
            }
        }, 600);
    })
});

// 如果出库的物料是外箱或整机，开始查询该条码下绑定的所有子条码数量
function CodeReqSonCodeNum(code) {
    let oldOutNum = Number($('#outLib').text());
    // let oldScanNum = Number($('#scanNum').text());
    // 如果本次请求与上次请求的条码一样则说明为重复扫入则不发起请求获取字条码数量
    if (code !== reqCode) {
        let codeHeard = code.substring(0, 1);
        if (codeHeard === "W" || codeHeard === "D") {
            $.ajax({
                type: "GET",
                url: "/admin/querySonCodeNum",
                dataType: "json",
                data: {"code": code},
                success: (dat) => {
                    if (dat["success"] !== "") {
                        reqCode = dat["code"];
                        if (dat["success"] > 0) {
                            $("#outLib").text(oldOutNum + dat["success"]);
                        }
                    } else {
                        $(".errInfo").css("display", "block");
                        $(".errInfo > span").css("display", "block").empty().text(dat["errMsg"]).delay(10000).fadeOut();
                    }
                }
            });
        }
    }
}

function formSubmit() {
    var aryDat = $("#scanRet").val().split(" ")
    if (aryDat.length === 1 || $('#linkOrder').val() === "" ) {  // || $('#WarrantyType').val() === ""
        $(".errInfo").css("display", "block");
        $(".errInfo > span").css("display", "block").empty().text("以上信息不允许留空，请重新填写").delay(6000).fadeOut();
        setTimeout(() => {
            $(".errInfo").css("display", "none");
            $(".errInfo > span").css("display", "none");
        }, 6000);
        return;
    }
    $.ajax({
        type: "POST",
        url: "",
        dataType: "json",
        data: JSON.stringify({
            "linkOrder": $("#linkOrder").val(),
            "scanRet": aryDat.slice(0, aryDat.length - 1),
            // "warrantType": $("#WarrantyType").val(),  todo 如保修期不同放开该注释即可
            "warrantType": "3",  // todo 默认保修期为一年，如保修期不同注释此行
        }),
        success: (ret) => {
            if (ret["success"] !== "") {
                $("#linkOrder").val("");
                $("#scanRet").val("");
                $("#outLib").text("");
                $("#WarrantyType").val("");
                $('#scanNum').text("");
                $(".errInfo").css("display", "block");
                $(".errInfo > span").css("display", "block").css("display", "block").empty().text(ret["success"]).delay(6000).fadeOut();
                setTimeout(() => {
                    $(".errInfo").css("display", "none");
                    $(".errInfo > span").css("display", "none");
                }, 6000);
            } else {
                $(".errInfo").css("display", "block");
                $(".errInfo > span").css("display", "block").empty().text(ret["errMsg"]).delay(10000).fadeOut();
                setTimeout(() => {
                    $(".errInfo").css("display", "none");
                    $(".errInfo > span").css("display", "none");
                }, 6000);
            }
        }
    });
}

// mobileLogin 登录按钮
function mobileLogin() {
    console.log("===========");
}

//
function showHideMenu() {
    if (btnFlag === 0) {
        $(".userMenuPanel").css("display", "block");
        btnFlag++;
    } else {
        $(".userMenuPanel").css("display", "none");
        btnFlag--;
    }
}

// 退出登录操作
function quitLogin() {
    $.ajax({
        type: "GET",
        url: "/quit",
        dataType: "json",
        data: {},
        success: (val) => {
            if (val["success"] !== "") {
                $.removeCookie('LIANLISESSIONID20220603', {path: '/'});
                window.location.href = "/sign";
            } else {
                alert(val["errMsg"]);
            }
        }
    });
}