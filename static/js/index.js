let workType = 0; // 用来记录工单类型 1 表示为返修工单，2表示为维修工单，3表示为服务申请
const SourceAddr = window.location.href;

// 点击菜单按钮事件
function workMenu(val) {
    $(val).addClass("menu_active");
    $(val).siblings("li").removeClass("menu_active");
    $(".createWork").css("display", "none");
    $(".sale_pan").css("display", "block");
    $(".bottom").css("position", "fixed");
    $(".userMenuPanel").css("display", "none");
}

// 创建工单事件
function createWorkOrder() {
    $(".sale_pan").css("display", "none");
    $(".createWork").css("display", "block");
    $(".bottom").removeAttr("style");
    // 获取所有物流公司和销售人员信息
    getAjaxFunc("/getAllWuLiu", {}, getCallBackFunc);
    getAjaxFunc("/getAllSale", {}, getCallBackFunc);
    // todo 点击创建工单应先获取用户地址，参数为用户名，问题：用户名怎么获取？/
    getAjaxFunc("/getUserAddress", {}, getCallBackFunc);
}

// 搜索工单按钮事件
function searchWorkOrder(val) {
    // 发起 get 请求获取工单信息
    $.ajax({
        type: "GET",
        url: "/searchWorkOrder",
        dataType: "json",
        data: {
            "wid": $(val).prev("input[name='sreachMsg']").val(),
        },
        success: (dat) => {
            if (dat["success"] !== "") {
                // 开始渲染工单表格数据
                if (dat["success"] !== null) {
                    var tabHtml = ``
                    for (var v in dat["success"]) {
                        if (dat["success"][v]["state"] === 0) {
                            tabHtml += `
                                <tr>
                                    <td>` + dat["success"][v]["log_num"] + `</td>
                                    <td>` + dat["success"][v]["work_type"] + `</td>
                                    <td>` + dat["success"][v]["mname"] + `</td>
                                    <td>` + dat["success"][v]["issue_describe"] + `</td>
                                    <td>` + "返厂途中" + `</td>
                                    <td>` + dat["success"][v]["create_date"] + `</td>
                                </tr>
                            `
                        } else if (dat["success"][v]["state"] === 1) {
                            tabHtml += `
                                <tr>
                                    <td>` + dat["success"][v]["log_num"] + `</td>
                                    <td>` + dat["success"][v]["work_type"] + `</td>
                                    <td>` + dat["success"][v]["mname"] + `</td>
                                    <td>` + dat["success"][v]["issue_describe"] + `</td>
                                    <td>` + "检测中" + `</td>
                                    <td>` + dat["success"][v]["create_date"] + `</td>
                                </tr>
                            `
                        } else if (dat["success"][v]["state"] === 2 || dat["success"][v]["state"] === 3) {
                            tabHtml += `
                                <tr>
                                    <td>` + dat["success"][v]["log_num"] + `</td>
                                    <td>` + dat["success"][v]["work_type"] + `</td>
                                    <td>` + dat["success"][v]["mname"] + `</td>
                                    <td>` + dat["success"][v]["issue_describe"] + `</td>
                                    <td>` + "维修中" + `</td>
                                    <td>` + dat["success"][v]["create_date"] + `</td>
                                </tr>
                            `
                        } else if (dat["success"][v]["state"] === 4 || dat["success"][v]["state"] === 5) {
                            tabHtml += `
                                <tr>
                                    <td>` + dat["success"][v]["express_num"] + `</td>
                                    <td>` + dat["success"][v]["work_type"] + `</td>
                                    <td>` + dat["success"][v]["mname"] + `</td>
                                    <td>` + dat["success"][v]["issue_describe"] + `</td>
                                    <td>` + "返还中" + `</td>
                                    <td>` + dat["success"][v]["cate_date"] + `</td>
                                </tr>
                            `
                        }
                    }
                    $("#myWorkTable").css("display", "block");
                    $(".tableData > #myWorkTable > div > .tbody > .table_data").empty().append(tabHtml);
                    $(".tishi").css("display", "none");
                } else {
                    $("#myWorkTable").css("display", "none");
                    $(".tishi").css("display", "block");
                }
            } else {
                alert(dat["errMsg"]);
                $(".tishi").css("display", "block");
            }
        },
    });
}

// 菜单按钮事件
function menuBtn(val) {
    var orderId = 0;
    $(val).addClass("workOrderActive");
    $(val).siblings().removeClass("workOrderActive");
    switch ($(val).text()) {
        case "待收货":
            orderId = 0;
            break;
        case "已收货":
            orderId = 1;
            break;
        case "维修中":
            orderId = 2;
            break;
        case "返途中":
            orderId = 3;
            break;
        case "已完成":
            orderId = 4;
            break;
        case "已取消":
            orderId = 5;
            break;
        case "全部":
            orderId = 8;
            break;
    }
    $.ajax({
        type: "GET",
        url: "/getWorkOrder",
        dataType: "json",
        data: {
            "statId": orderId,
        },
        success: (dat) => {
            if (dat["success"] !== "") {
               if (dat["success"] !== null) {
                   // $("#myWorkTable").css("display", "block")
                   // $(".tishi").css("display", "none")
                   // 开始渲染工单表格数据
                   var tabHtml = ``
                   for (var v in dat["success"]) {
                       console.log(dat["success"][v]["state"])
                        if (dat["success"][v]["state"] === 0) {
                            tabHtml += `
                                <tr>
                                    <td>` + dat["success"][v]["log_num"] + `</td>
                                    <td>` + dat["success"][v]["work_type"] + `</td>
                                    <td>` + dat["success"][v]["mname"] + `</td>
                                    <td>` + dat["success"][v]["issue_describe"] + `</td>
                                    <td>` + "返厂途中" + `</td>
                                    <td>` + dat["success"][v]["create_date"] + `</td>
                                </tr>
                            `
                        } else if (dat["success"][v]["state"] === 1) {
                            tabHtml += `
                                <tr>
                                    <td>` + dat["success"][v]["log_num"] + `</td>
                                    <td>` + dat["success"][v]["work_type"] + `</td>
                                    <td>` + dat["success"][v]["mname"] + `</td>
                                    <td>` + dat["success"][v]["issue_describe"] + `</td>
                                    <td>` + "检测中" + `</td>
                                    <td>` + dat["success"][v]["create_date"] + `</td>
                                </tr>
                            `
                        } else if (dat["success"][v]["state"] === 2 || dat["success"][v]["state"] === 3) {
                            tabHtml += `
                                <tr>
                                    <td>` + dat["success"][v]["log_num"] + `</td>
                                    <td>` + dat["success"][v]["work_type"] + `</td>
                                    <td>` + dat["success"][v]["mname"] + `</td>
                                    <td>` + dat["success"][v]["issue_describe"] + `</td>
                                    <td>` + "维修中" + `</td>
                                    <td>` + dat["success"][v]["create_date"] + `</td>
                                </tr>
                            `
                        } else if (dat["success"][v]["state"] === 4 || dat["success"][v]["state"] === 5) {
                            tabHtml += `
                                <tr>
                                    <td>` + dat["success"][v]["express_num"] + `</td>
                                    <td>` + dat["success"][v]["work_type"] + `</td>
                                    <td>` + dat["success"][v]["mname"] + `</td>
                                    <td>` + dat["success"][v]["issue_describe"] + `</td>
                                    <td>` + "返还中" + `</td>
                                    <td>` + dat["success"][v]["cate_date"] + `</td>
                                </tr>
                            `
                        }
                   }
                   $(".repairWorkTable").css("display", "none");
                   $("#myWorkTable").css("display", "block");
                   $(".tableData > #myWorkTable > div > .tbody > .table_data").empty().append(tabHtml);
                   $(".tishi").css("display", "none");
               } else {
                   $("#myWorkTable").css("display", "none");
                   $(".tishi").css("display", "block");
               }
            } else {
                alert(dat["errMsg"]);
                $(".tishi").css("display", "block");
            }
        }
    });
}

// 选择工单类型按钮事件
function chooseWorkType(val) {
    $(val).removeClass("chooseBtn").addClass("liActive");
    $(val).siblings("li").removeClass("liActive").addClass("chooseBtn");
    switch ($(val).text()) {
        case "返修工单":
            workType = 1;
            break;
        case "维修工单":
            workType = 2;
            break;
        case "服务申请":
            workType = 3;
            break;
    }
}

// 添加设备按钮事件
function addDeviceBtn() {
    $(".device_send_con").css("display", "block");
    getAjaxFunc("/getMaterType", {}, getCallBackFunc);
}

// 编辑设备按钮事件
function editDevice(val) {

}

// 删除设备按钮事件
function delDevice(val) {

}

// 添加地址按钮事件
function addAddress() {
    $(".addressConfirmBtn").empty().text("确认");
    $(".address_send_con").css("display", "block");
    getAjaxFunc("/getProvinceDat", {}, getCallBackFunc)
    getAjaxFunc("/getUserInfo", {}, getCallBackFunc)
}

// 监听选择销售人员下拉框根据选择的销售人员显示不同的接收人员信息
$(() => {
    // 监听创建工单页面选择销售人员下拉框事件
    $("#saleName").change(() => {
        if ($('#saleName').val() === "") {
            $(".addrShow").css("display", "none");
        } else {
            $(".addrShow").css("display", "block");
        }
        $('.addrShow').children("strong").empty().html($("#saleName").val().split("-")[1]);
    });

    // 监听添加设备弹框设备类型下拉框事件，并根据选择的类型发起 ajax 请求获取相应的物料名称
    $("#deviceType").change(() => {
        getAjaxFunc("/getMaterDat", {"mt_id": $('#deviceType').val().split("-")[0]}, getCallBackFunc);
    });

    // 监听添加地址弹框省份下拉框事件，并根据选择的类型发起 ajax 请求获取相应的城市名称
    $("#province").change(() => {
        getAjaxFunc("/getCityDat", {"pro_id": $('#province').val().split("-")[0]}, getCallBackFunc)
    });

    // 监听添加地址地址信息鼠标移入移出事件
    $("#user_name").on("mouseover", ".userAddAddr", () => {
        $(".userAddAddr > .editUser").css("display", "block");
    });
    $('#user_name').on("mouseout", ".userAddAddr", () => {
        $(".userAddAddr > .editUser").css("display", "none");
    });
});

// 创建工单页面提交按钮事件
function workSubmit(val) {
    var data = {};
    var workInfo = {};
    var wuLiaoType = [];
    var wuLiaoName = [];
    var wuLiaoNum = [];
    var faultOrigin = [];
    // 获取各种数据提交数据
    var wuLiuId = $("#wuliu").val();
    if (wuLiuId === "") {
        alert("添加工单失败，您还没有选择物流公司，请检查~！");
        return;
    }
    var expressNum = $("#expressNum").val();
    if (expressNum === "") {
        alert("添加工单失败，您还没有选择物流单号，请检查~！");
        return;
    }
    var address = $(".addrName").text() + $(".addrProvince").text() + $(".addrCity").text() + $(".addrInfo").text()+$(".addrPhone").text();
    if (address === "") {
        alert("添加工单失败，您的收货地址为空，为避免不必要的损失请返回添加~！");
        return;
    }
    var tr = $(".products").children("div").children("div").children(".tbody").children(".table_data").children("tr");
    data["wuLiuId"] = wuLiuId;
    data["expressNum"] = expressNum;
    data["addr"] = address;
    for (var va = 0; va < tr.length; va++) {
        wuLiaoType.push(tr.eq(va).children("td").eq(0).text());
        wuLiaoName.push(tr.eq(va).children("td").eq(1).children("span").text());
        faultOrigin.push(tr.eq(va).children("td").eq(2).text());
        wuLiaoNum.push(tr.eq(va).children("td").eq(3).text());
        workInfo["type"] = wuLiaoType;
        workInfo["name"] = wuLiaoName;
        workInfo["num"] = wuLiaoNum;
        workInfo["fault"] = faultOrigin;
    }
    if (wuLiaoType.length === 0 || wuLiaoName.length === 0 || wuLiaoNum.length === 0 || faultOrigin.length === 0) {
        alert("添加工单失败~您还没有添加任何需要返修的设备,请添加设备~!");
        return;
    }
    if ($('#saleName').val().split("-").length === 1) {
        alert("添加工单失败~您还没选择业务/销售人员,请进行选择~!");
        return;
    }
    data["workInfo"] = workInfo;
    data["workType"] = workType;
    if (workType === 0) {
        alert("添加失败，您还没有选择工单类型，请返回顶部选择~!");
        return;
    }
    data["sale_id"] = Number($("#saleName").val().split("-")[0]);
    $.ajax({
        type: "POST",
        url: "/addWorkDat",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(data),
        success: (dat)=> {
            if (dat["success"] !== "") {
                // 直接刷新页面;
                window.location.reload();
            } else {
                alert(dat["errMsg"]);
            }
        }
    });
}

// 使用正则判断用户输入的数据是否为数字类型数字
function isNumber(value) {
    var patrn = /^(-)?\d+(\.\d+)?$/;
    if (patrn.exec(value) == null || value === "") {
        return false
    } else {
        return true
    }
}

// 使用正则验证用书输入的手机号格式是否正确
function isPhone(value) {
    var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
    if (value === "" || value.length !== 11 || !mobile.test(value)) {
        return false
    } else {
        return true
    }
}

// 添加设备弹框确认按钮
function confirmBtn(val) {
    switch ($(val).text()) {
        case "确认":
            var totalCount = 0;
            var deviceType = $("#deviceType").val().split("-")[1];
            var deviceNameId = $("#deviceName").val().split("_")[0];
            var deviceName = $("#deviceName").val().split("_")[1];
            var count = $("#deviceCount").val();
            var origin = $("#deviceOrigin").val();
            if (deviceType === "" || deviceName === "" || count === "" || origin === "") {
                alert("添加的设备信息不允许留空，请重新填写，谢谢!");
                return
            }
            if (!isNumber(count)) {
                alert("抱歉！您输入的设备数量数据类型有误，请重新填写，谢谢!");
                return
            }
            totalCount = parseInt($(".text-strong").text()) + parseInt(count);
            var tabHtml = `
                <tr>
                        <td>` + deviceType + `</td>
                        <td><span style="display: none">`+deviceNameId+`</span>`+deviceName+`</td>
                        <td>` + origin + `</td>
                        <td>` + count + `</td>
<!--                        <td>-->
<!--                            <i class="glyphicon glyphicon-pencil editDevice" onclick="editDevice(this)"></i>-->
<!--                            &nbsp;<i class="glyphicon glyphicon-trash delDevice" onclick="delDevice(this)"></i>-->
<!--                        </td>-->
                    </tr>
            `
            $(".products > div > div > .tbody > .table_data").append(tabHtml);
            $(".text-strong").text(totalCount);
            $('#deviceType').val("");
            $('#deviceName').val("");
            $('#deviceCount').val("");
            $('#deviceOrigin').val("");
            $('.device_send_con').css("display", "none");
            break;
        case "取消":
            $(".device_send_con").css("display", "none");
            break;
    }
}

// 添加地址按钮事件
function addressConfirmBtn(val) {
    switch ($(val).text()) {
        case "确认":
            var userInfo = ""
            var userName = $("#username").val();
            if (userName === "") {
                alert("收货人姓名不允许留空，请重新填写，谢谢!");
                return;
            }
            var province = $("#province").val();
            if (province === "") {
                alert("添加的地址省份不允许留空，请重新选择，谢谢!");
                return;
            }
            var city = $("#city").val();
            if (city === "" ) {
                alert("添加的地址城市不允许留空，请重新选择，谢谢!");
                return;
            }
            var userAddr = $("#userAddr").val();
            if (userAddr === "" ) {
                alert("收货人地址不允许留空，请重新填写，谢谢!");
                return;
            }
            var phone = $("#phone").val();
            if (phone === "") {
                alert("收货人联系方式不允许留空，请重新填写，谢谢!");
                return;
            }
            if (!isPhone(phone)) {
                alert("抱歉！您输入的联系方式格式有误，请重新填写，谢谢!");
                return
            }
            userInfo = userName + "." + province.split("-")[1] + "." + city.split("-")[1] + "." + userAddr + "." + phone;
            $.ajax({
                type: "POST",
                url: "/insertAddrToSql",
                dataType: "json",
                data: {"addr": userInfo, "option": $(val).text()},
                success: (dat) => {
                    if (dat["success"] !== "") {
                        // 添加成功隐藏添加地址页面，并清空刚刚添加的地址信息
                        $('#username').val("");
                        $('#province').val("");
                        $('#city').val("");
                        $('#userAddr').val("");
                        $('#phone').val("");
                        $("#emptyTishi").css("display", "none");
                        $('.address_send_con').css("display", "none");
                        getAjaxFunc("/getUserAddress", {}, getCallBackFunc);
                    } else {
                        alert(dat["errMsg"]);
                    }
                }
            });
            break;
        case "更新":
            userInfo = ""
            userName = $('#username').val();
            if (userName === "") {
                alert("收货人地址不允许留空，请重新填写，谢谢!");
                return;
            }
            province = $('#province').val();
            if (province === "") {
                alert("省份不允许留空，请重新选择，谢谢!");
                return;
            }
            city = $('#city').val();
            if (city === "" ) {
                alert("城市不允许留空，请重新选择，谢谢!");
                return;
            }
            userAddr = $('#userAddr').val();
            if (userAddr === "" ) {
                alert("收货人地址不允许留空，请重新填写，谢谢!");
                return;
            }
            phone = $('#phone').val();
            if (phone === "") {
                alert("收货人联系方式不允许留空，请重新填写，谢谢!");
                return;
            }
            if (!isPhone(phone)) {
                alert("抱歉！您输入的联系方式格式有误，请重新填写，谢谢!");
                return
            }
            userInfo = userName + "." + province.split("-")[1] + "." + city.split("-")[1] + "." + userAddr + "." + phone;
            $.ajax({
                type: "POST",
                url: "/insertAddrToSql",
                dataType: "json",
                data: {"addr": userInfo, "option": $(val).text()},
                success: (dat) => {
                    if (dat["success"] !== "") {
                        // 添加成功隐藏添加地址页面，并清空刚刚添加的地址信息
                        $('#username').val("");
                        $('#province').val("");
                        $('#city').val("");
                        $('#userAddr').val("");
                        $('#phone').val("");
                        $("#emptyTishi").css("display", "none");
                        $('.address_send_con').css("display", "none");
                        getAjaxFunc("/getUserAddress", {}, getCallBackFunc);
                    } else {
                        alert(dat["errMsg"]);
                    }
                }
            });
            break
        case "取消":
            $(".address_send_con").css("display", "none");
            break;
    }
}

// editUserAddr 编辑用户地址方法
function editUserAddr(val) {
    $(".addressConfirmBtn").empty().text("更新");
    getAjaxFunc("/getProvinceDat", {}, getCallBackFunc)
    var userName = $(val).siblings(".addrName").text();
    var prov = $(val).siblings(".addrProvince").text();
    var city = $(val).siblings(".addrCity").text();
    var addr = $(val).siblings(".addrInfo").text();
    var phone = $(val).siblings(".addrPhone").text();
    // todo 是否需要设置下拉框？
    $('#username').val(userName);
    $('#province').val(prov);
    $('#city').val(city);
    $('#userAddr').val(addr);
    $('#phone').val(phone);
    $(".address_send_con").css("display", "block");
}

// cLogout 退出登录按钮事件
function cLogout() {
    getAjaxFunc("/cLogout", {}, getCallBackFunc)
}

// 点击用户名显示隐藏个人菜单
function showHideMenu() {
    if ($('.userMenuPanel').is(':hidden')) {
        $(".userMenuPanel").css("display", "block");
    } else {
        $(".userMenuPanel").css("display", "none");
    }
}

// ajax 发起 get 请求方法
function getAjaxFunc(addr, dat, callBack) {
    $.ajax({
        type: "GET",
        url: addr,
        dataType: "json",
        data: dat,
        success: (dat) => {
            callBack(addr, dat)
        },
    });
}

function getCallBackFunc(addr, dat) {
    // console.log(dat["success"])
    if (dat["success"] !== "") {
        switch (addr) {
            case "/getMaterDat":
                // 渲染添加设备弹框页面的设备名称下拉框数据
                var materHt = `<option value=""></option>`
                // todo ===========================================================================================================================================
                for (var mat in dat["success"]) {
                    //  + dat["success"][mat]["mname"] + "_" + dat["success"][mat]["brand"] + "_" + dat["success"][mat]["model"] + `   dat["success"][mat]["mid"]
                    materHt += `<option value="`+ dat["success"][mat]["mid"] + "_" + dat["success"][mat]["mname"] + "_" + dat["success"][mat]["model"] + `"></option>`
                }
                $("#deviceMater").empty().append(materHt);
                // todo ============================================================================================================================================
                break;
            case "/getMaterType":
                // 渲染添加设备弹框页面的设备类型下拉框数据
                var mtHtml = `<option value=""></option>`
                for (var mater in dat["success"]) {
                    mtHtml += `
                    <option value="` + dat["success"][mater]["mt_id"] + "-" + dat["success"][mater]["MaterTypeName"] + `">` + dat["success"][mater]["MaterTypeName"] + `</option>
                    `
                }
                $("#deviceType").empty().append(mtHtml);
                break;
            case "/getAllWuLiu":
                var wuHtml = `<option value=""></option>`
                for (var log in dat["success"]) {
                    wuHtml += `
                    <option value="` + dat["success"][log]["lid"] + `">` + dat["success"][log]["lname"] + `</option>
                    `
                }
                $(".input > #wuliu").empty().append(wuHtml);
                // 渲染创建工单页面的物流信息
                break;
            case "/getAllSale":
                // 渲染创建工单页面的销售信息
                var saHtml = `<option value=""></option>`
                for (var sale in dat["success"]) {
                    saHtml += `
                    <option value="` + dat["success"][sale]["uid"] + `-` + dat["success"][sale]["user_name"] + `">` + dat["success"][sale]["user_name"] + `</option>
                    `
                }
                $(".chooseInput > #saleName").empty().append(saHtml);
                break;
            case "/getCityDat":
                // 开始渲染添加地址弹框页面的城市下拉选择框
                var cityMl = `<option value=""></option>`
                for (var city in dat["success"]) {
                    cityMl += `
                    <option value="` + dat["success"][city]["CityId"] + "-" + dat["success"][city]["CityName"] + `">`
                        + dat["success"][city]["CityName"] + `
                    </option>
                    `
                }
                $("#city").empty().append(cityMl);
                break;
            case "/getProvinceDat":
                // 开始渲染添加地址弹框的省份下拉菜单数据
                var proHtl = `<option value=""></option>`
                for (var pro in dat["success"]) {
                    proHtl += `
                    <option value="` + dat["success"][pro]["ProId"] + "-" + dat["success"][pro]["ProName"] + `">`
                        + dat["success"][pro]["ProName"] + `
                    </option>
                    `
                }
                $("#province").empty().append(proHtl);
                break;
            case "/getUserAddress":
                if (dat["success"] !== null ) {
                    var userHml = ``;
                    for (var address in dat["success"]) {
                        userHml += `
                        <p class="userAddAddr">
                            <b class="addrName">` + dat["success"][address].split(".")[0] + `</b>
                            <b class="addrProvince">`+ dat["success"][address].split(".")[1] + `</b>
                            <b class="addrCity">` + dat["success"][address].split(".")[2] + `</b>
                            <b class="addrInfo">` + dat["success"][address].split(".")[3] + `</b>
                            <b class="addrPhone">` + dat["success"][address].split(".")[4] + `</b>
                            <span class="editUser" style="display: none" onclick="editUserAddr(this)">编辑</span>
                        </p>
                    `
                    }
                    $("#user_name").empty().append(userHml);
                    $("#emptyTishi").css("display", "none");
                } else {
                    $("#emptyTishi").css("display", "block");
                }
                break;
            case "/getUserInfo":
                $("#username").val(dat["success"]["name"]);
                $("#phone").val(dat["success"]["phone"]);
                break;
            case "/cLogout":
                $.removeCookie('LIANLISESSIONID20220603',{ path: '/'});
                window.location.href = "/login";
                break;
        }
    } else {
        switch (addr) {
            case "/getUserAddress":
                // 没有地址则显示添加地址提示
                $("#emptyTishi").css("display", "block");
                // alert(dat["errMsg"]);
                break;
            default:
                alert(dat["errMsg"]);
        }
    }
}

// 工单类型选择按钮事件
function orderType(val) {
    switch ($(val).text()) {
        case "售后工单":
            $(val).css("background-color","#ccc").siblings("li").css("background-color", "#eee");
            // $(val).siblings("li").css("background-color","#eee");
            $(".saleWorkSpan").css("display", "block");
            $(".repairWorkOrder,.repairWorkTable").css("display", "none");
            return;
        case "维修工单":
            $(val).css("background-color","#ccc").siblings("li").css("background-color", "#eee");
            $(".repairWorkOrder").css("display", "block");
            $(".saleWorkSpan,#myWorkTable").css("display", "none");
            return;
    }
}

// 维修工单工单支付状态选择按钮数据
chooseBtn = (val) => {
    var orderType = 0;
    switch ($(val).text()) {
        case "待付款":
            orderType = 1;
            break;
        case "已付款":
            orderType = 2;
            break;
    }
    $.ajax({
        type: "GET",
        url: "/getRepairWork",
        dataType: "json",
        data: {"orderType": orderType},
        success: (val) => {
            let repairHtml = ``;
            if (val["success"] !== "" && val["success"] !== null) {
                $(".tishi").css("display", "none");
                for (let index in val["success"]) {
                    if (val["success"][index]["amount"] === null) {
                        repairHtml += `
                            <tr>
                                <td style="display: none;">`+val["success"][index]["pay_id"]+`</td>
                                <td>`+val["success"][index]["log_num"]+`</td>
                                <td>`+val["success"][index]["mname"]+`</td>
                                <td>`+val["success"][index]["work_state"]+`</td>
                                <td>`+val["success"][index]["create_date"]+`</td>
                                <td style="color: palevioletred">待报价</td>
                                <td id="payBtn">---</td>
                            </tr>
                        `;
                    } else if (val["success"][index]["amount"] !== null && val["success"][index]["pay_stat"] === "待付款") {
                        repairHtml += `
                            <tr>
                                <td style="display: none;">`+val["success"][index]["pay_id"]+`</td>
                                <td>`+val["success"][index]["log_num"]+`</td>
                                <td>`+val["success"][index]["mname"]+`</td>
                                <td>`+val["success"][index]["work_state"]+`</td>
                                <td>`+val["success"][index]["create_date"]+`</td>
                                <td style="color: palevioletred">`+val["success"][index]["amount"]+`</td>
                                <td id="payBtn" onclick="payBtn(this)">支付</td>
                            </tr>
                        `;
                    } else if (val["success"][index]["amount"] !== null && val["success"][index]["pay_stat"] === "已付款") {
                        repairHtml += `
                            <tr>
                                <td style="display: none;">`+val["success"][index]["pay_id"]+`</td>
                                <td>`+val["success"][index]["log_num"]+`</td>
                                <td>`+val["success"][index]["mname"]+`</td>
                                <td>`+val["success"][index]["work_state"]+`</td>
                                <td>`+val["success"][index]["create_date"]+`</td>
                                <td style="color: palevioletred">`+val["success"][index]["amount"]+`</td>
                                <td id="payBtn">已支付</td>
                            </tr>
                        `;
                    }
                }
                $("#myWorkTable").css("display", "none");
                $(".repairWorkTable").css("display", "block");
                $(".repairWorkTable > div > .tbody > .table_data").empty().append(repairHtml);
            } else if (val["errMsg"] !== "") {
                $(".tishi").css("display", "block");
                alert(val["errMsg"]);
            } else {
                $(".repairWorkTable").css("display", "none");
                $(".tishi").css("display", "block");
            }
        }
    });
}

// 获取日期
function getNow(s) {
    return s < 10 ? '0' + s: s;
}

// 维修工单点击支付按钮事件
payBtn = (val) => {
    $(".msg_body").css("display", "none");
    $(".payPanel").css("display", "block");
    var nowtimes=Math.round(new Date().getTime()/1000);

    var myDate = new Date();
    var year=myDate.getFullYear();        //获取当前年
    var month=myDate.getMonth()+1;   //获取当前月
    var date=myDate.getDate();            //获取当前日
    var h=myDate.getHours();              //获取当前小时数(0-23)
    var m=myDate.getMinutes();          //获取当前分钟数(0-59)
    var s=myDate.getSeconds();

    var now=year+'-'+getNow(month)+"-"+getNow(date)+" "+getNow(h)+':'+getNow(m)+":"+getNow(s);

    $("#orderDate").empty().text("ll"+nowtimes + $(val).siblings("td").eq(0).text());
    $("#createDate").empty().text(now);
    $("#goodsName").empty().text($(val).siblings("td").eq(2).text());
    $("#gas").empty().text($(val).siblings("td").eq(5).text());
}

// 支付页面返回按钮事件
payReturnBtn = () => {
    $(".msg_body").css("display", "block");
    $(".payPanel").css("display", "none");
}

// 支付方法按钮事件
payMethod = (val) => {
    var paymethod = $(val).text();
    var orderNo = $("#orderDate").text();
    var createDate = $("#createDate").text();
    var goods = $("#goodsName").parent("span").text();
    var gas = $("#gas").text();
    switch (paymethod) {
        case "支付宝支付":
            $.ajax({
                type: "GET",
                url: "/alipay",
                dataType: "json",
                data: {
                    "order": orderNo,
                    "create": createDate,
                    "goods": goods,
                    "gas": gas,
                    "url": SourceAddr,
                },
                success: (dat) => {
                    console.log(dat["success"])
                    if (dat["success"] !== "") {
                        window.location.href = dat["success"];
                    } else {
                        $("#errorsTishi").empty().text(dat["errMsg"]).delay(6000).fadeOut();
                    }
                }
            });
            break;
        case "微信支付":
            console.log("微信支付============================");
            break;
    }
}