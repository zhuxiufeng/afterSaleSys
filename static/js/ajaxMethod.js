// get 请求获取所有数据 ajax 方法封装 =========================
function requestGetAllData(url, callBackFunc) {
    // let data = {};
    $.ajax({
        type: "GET",
        url: url,
        data: {},
        dataType: "json",
        success: function (dat) {
            callBackFunc(url, dat);
        },
    });
}

function getCallBackFunc(url, dat) {
    if (dat["success"] !== "") {
        switch (url) {
            case "/admin/getAllUser":
                // 开始渲染用户列表
                var htmlStr = `<tr>`
                for (let user in dat["success"]) {
                    htmlStr += `<td>` + dat["success"][user]["uid"] + `</td>` +
                        `<td>` + dat["success"][user]["account"] + `</td>` +
                        `<td>` + dat["success"][user]["user_name"] + `</td>` +
                        `<td>` + dat["success"][user]["type_name"] + `</td>` +
                        `<td><span class="change_tab" onclick="change_user(this)">修改</span> | <span
                    class="del_tab" onclick="del_user(this)">删除</span></td>
            </tr>
                `
                }
                $(".user_exhibit > #scrollTable > div > .tbody > .table_data").empty().append(htmlStr);
                break;
            case "/admin/getAllClient":
                var htmlS = `<tr>`
                for (var client in dat["success"]) {
                    htmlS += `
                        <td>` + dat["success"][client]["cid"] + `</td>
                        <td>` + dat["success"][client]["client_name"] + `</td>
                        <td>` + dat["success"][client]["phone"] + `</td>
                        <td>` + dat["success"][client]["addr"] + `</td>
                        <td><span class="change_tab" onclick="change_client(this)">修改</span> | <span
                                class="del_tab" onclick="del_client(this)">删除</span></td>
                    </tr>
                    `
                }
                $(".client_exhibit > #client_scrollTable > div > .tbody > .table_data").empty().append(htmlS);
                break;
            case "/admin/getAllWuLiu":
                var htlmStr = ``
                for (var log in dat["success"]) {
                    htlmStr += `
                        <tr>
                            <td>` + dat["success"][log]["lid"] + `</td>
                            <td>` + dat["success"][log]["lname"] + `</td>
                            <td>` + dat["success"][log]["butt"] + `</td>
                            <td>` + dat["success"][log]["phone"] + `</td>
                            <td><span class="change_tab" onclick="change_wuLiu(this)">修改</span> | <span
                                    class="del_tab" onclick="del_wuLiu(this)">删除</span></td>
                        </tr>
                    `
                }
                $(".wuLiu_exhibit > #wuliu_scrollTable > div > .tbody > .table_data").empty().append(htlmStr);
                break;
            case "/admin/getBomName":
                var htlm = ``
                for (var bom in dat["success"]) {
                    htlm += `
                       <tr>
                            <td>` + dat["success"][bom]["bn_id"] + `</td>
                            <td>` + dat["success"][bom]["b_name"] + `</td>
                            <td><span class="change_tab" onclick="change_bomName(this)">修改</span> | <span
                                    class="del_tab" onclick="del_bomName(this)">删除</span></td>
                        </tr>
                    `
                }
                $(".bomName_exhibit > #bomName_scrollTable > div > .tbody > .table_data").empty().append(htlm);
                break;
            case "/admin/getAllMater":
                var htlmSs = ``
                for (var mater in dat["success"]) {
                    htlmSs += `
                    <tr>
                        <td>` + dat["success"][mater]["mid"] + `</td>
                        <td>` + dat["success"][mater]["mname"] + `</td>
                        <td>` + dat["success"][mater]["brand"] + `</td>
                        <td>` + dat["success"][mater]["model"] + `</td>
                        <td>` + dat["success"][mater]["mater_type_name"] + `</td>
                        <td><span class="change_tab" onclick="change_mater(this)">修改</span> | <span
                                class="del_tab" onclick="del_mater(this)">删除</span></td>
                    </tr>
                    `
                }
                $(".mater_exhibit > #mater_scrollTable > div > .tbody > .table_data").empty().append(htlmSs);
                break;
            case "/admin/getAllPrinter":
                var phtml = ``
                for (var print in dat["success"]) {
                    phtml += `
                        <tr>
                            <td>` + dat["success"][print]["pid"] + `</td>
                            <td>` + dat["success"][print]["p_name"] + `</td>
                            <td>` + dat["success"][print]["p_net_addr"] + `</td>
                            <td>` + dat["success"][print]["bt_ser_addr"] + `</td>
                            <td><span class="change_tab" onClick="change_print(this)">修改</span> | <span
                                class="del_tab" onClick="del_printer(this)">删除</span></td>
                        </tr>
                    `
                }
                $(".print_exhibit > #print_scrollTable > div > .tbody > .table_data").empty().append(phtml);
                break;
            case "/admin/getAllMaterType":
                var mtHtml = `
                    <option value="" disabled selected hidden>若添加的物料为成品或半成品请选择配置名称，基础物料则无需选择</option>
                    <option value=""></option>
                `
                for (var mate in dat["success"]) {
                    mtHtml += `
                        <option value="` + dat["success"][mate]["mt_id"] + `">` + dat["success"][mate]["MaterTypeName"] + `</option>
                    `
                }
                $("#che_bom_name_mater").empty().append(mtHtml);
                break;
            case "/admin/getAllMaterIdName":
                var htmml = `
                    <option value=""></option>
                `
                for (mate in dat["success"]) {
                    htmml += `
                    <option value="` + dat["success"][mate]["mid"] + "-" + dat["success"][mate]["mname"] + `"></option>
                    `
                }
                $("#productName").empty().append(htmml);
                break;
            case "/admin/getAllBomName":
                var bhtml = `<option value=""></option>`
                for (bom in dat["success"]) {
                    bhtml += `
                        <option value="` + dat["success"][bom]["bn_id"] + "-" + dat["success"][bom]["b_name"] + `"></option>
                    `
                }
                $("#proBomName").empty().append(bhtml);
                break;
            case "/admin/getMaterNotInOneAndTwo":
                var hhtml = ``
                for (var i = 0; i < Number(dat["success"]["length"]); i++) {
                    hhtml += `
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" list="mater" id="selectMater" class="table_select" onclick="getSelVal(this)" style="background-color: lightyellow;">
                                <datalist id="mater">
                                    <option value=""></option>
                    `
                    for (var mat in dat["success"]["mates"]) {
                        hhtml += `
                                        <option value="` + dat["success"]["mates"][mat]["mid"] + " " + dat["success"]["mates"][mat]["mname"] + " " + dat["success"]["mates"][mat]["model"] + " " + "【" + dat["success"]["mates"][mat]["mater_type_name"] + "】" + `"></option>
                        `
                    }
                    hhtml += `
                                </datalist>
                            </td>
                            <td></td>
                            <td>
                                <input type="text" name="number" placeholder="请输入用量" style="border: 0;text-indent: 20px;" />
                            </td>
                        </tr>
                    `
                }
                $(".child > #product_scrollTable > div > .tbody > .table_data").empty().append(hhtml);
                break;
            case "/admin/PrintGetAllPrinter":
                var pcpdehtml = `
                        <option value=""></option>
                    `
                for (var printer in dat["success"]) {
                    pcpdehtml += `
                        <option value="` + dat["success"][printer]["pid"] + `">` +
                        dat["success"][printer]["p_name"] + " - " + "【" + dat["success"][printer]["p_net_addr"] + "】" + `
                        </option>
                    `
                }
                $("#printer").empty().append(pcpdehtml);
                break;
            case "/admin/PrintGetAllClient":
                var clHtml = `
                    <option value=""></option>
                `
                for (var cli in dat["success"]) {
                    clHtml += `
                        <option value="` + dat["success"][cli]["cid"] + "-" + dat["success"][cli]["client_name"] + `"></option>
                    `
                }
                $("#client").empty().append(clHtml);
                break;
            case "/admin/PrintGetAllUser":
                var usrHtml = `
                    <option value=""></option>
                `
                for (var usr in dat["success"]) {
                    usrHtml += `
                        <option value="` + dat["success"][usr]["uid"] + `">` + dat["success"][usr]["user_name"] + `</option>
                    `
                }
                $("#sale").empty().append(usrHtml);
                break;
            case "/admin/PrintGetAllVersion":
                var verHtml = `
                    <option value=""></option>
                `
                for (var ver in dat["success"]) {
                    verHtml += `
                        <option value="` + dat["success"][ver]["bn_id"] + '-' + dat["success"][ver]["b_name"] + `"></option>
                    `
                }
                $("#cfgName").empty().append(verHtml);
                break;
            case "/admin/getUpcoming":
                var upHtml = ``;
                for (var up in dat["success"]) {
                    if (dat["success"][up]["process"] === 0) {
                        upHtml += `
                            <li class="list-group-item list-group-item-default">
                                <b>` + dat["success"][up]["wid"] + `</b>
                                【<strong>` + dat["success"][up]["client_name"] + `</strong>】
                                 创建了一个: <strong>` + dat["success"][up]["up_name"] + `</strong>
                                 待处理，
<!--                                 运单号：<strong style="color: #0066FF">` + dat["success"][up]["log_num"] + `</strong>-->
                                <i onclick="detailBtn(this)">详情</i>
                            </li>
                        `
                    } else if (dat["success"][up]["process"] === 1) {
                        upHtml += `
                            <li class="list-group-item list-group-item-success">
                                <b>` + dat["success"][up]["wid"] + `</b>
                                【<strong>` + dat["success"][up]["client_name"] + `</strong>】
                                 的: <strong>` + dat["success"][up]["up_name"] + `</strong> 工单
<!--                                 运单号：<strong style="color: #0066FF">` + dat["success"][up]["log_num"] + `</strong> -->
                                 已成功处理
                                <i onclick="detailBtn(this)">详情</i>
                            </li>
                        `
                    }
                }
                $(".all_table > .table_info > .all_list-group").empty().append(upHtml);
                break;
            case "/admin/getAllVersion":  // todo 条码绑定获取所有 bom 名称
                var verHTml = `
                    <option value="" disabled selected hidden>请先确认需要绑定条码的成品配置并正确选择</option>
                    <option value=""></option>
                `
                for (var version in dat["success"]) {
                    verHTml += `
                        <option value="` + dat["success"][version]["bn_id"] + '-' + dat["success"][version]["b_name"] + `"></option>
                    `
                }
                $("#choose_select_version").empty().append(verHTml);
                break;
            case "/admin/getAllBusWorkOrder":
                var busWork = ``
                for (var work in dat["success"]) {
                    busWork += `
                        <tr>
                            <td>` + dat["success"][work]["wid"] + `</td>
                            <td>` + dat["success"][work]["mname"] + `</td>
                            <td>` + dat["success"][work]["model"] + `</td>
                            <td>` + dat["success"][work]["issue_describe"] + `</td>
                            <td>` + dat["success"][work]["mater_num"] + `</td>
                            <td>` + dat["success"][work]["create_date"] + `</td>
                            <td>` + dat["success"][work]["log_num"] + `</td>
                            <td>` + dat["success"][work]["work_type"] + `</td>
                            <td>` + dat["success"][work]["client_name"] + `</td>
                            <td>` + dat["success"][work]["user_name"] + `</td>
                            <td>` + dat["success"][work]["work_state"] + `</td>`
                            if (dat["success"][work]["work_type"] === "返修工单") {
                                busWork += `<td>---</td>`
                            } else {
                                busWork += `<td>` + dat["success"][work]["pay_status"] + `</td>`
                            }
                            busWork += `
                            <td>` + dat["success"][work]["out_firm_name"] + `</td>
                            <td>` + dat["success"][work]["o_number"] + `</td>
                            <td>` + dat["success"][work]["crate_date"] + `</td>
                            <td>` + dat["success"][work]["express_num"] + `</td>`
                    if (dat["success"][work]["state"] === 0) {
                        busWork += `<td><span class="change_tab" onclick="etid_btn(this)">接收</span> | <span
                                                            class="del_tab" onclick="del_work(this)">删除</span></td>`
                    } else if (dat["success"][work]["state"] === 1) {
                        busWork += `<td><span class="change_tab" onclick="etid_btn(this)">派单</span> | <span
                                                            class="del_tab" onclick="del_work(this)">删除</span></td>`
                    } else if (dat["success"][work]["state"] === 2 || dat["success"][work]["state"] === 3) {
                        busWork += `<td><span class="change_tab" onclick="etid_btn(this)">退仓</span> | <span
                                                            class="del_tab" onclick="del_work(this)">删除</span></td>`
                    } else if (dat["success"][work]["state"] === 4) {
                        busWork += `<td><span class="change_tab" onclick="etid_btn(this)">发货</span> | <span
                                                            class="del_tab" onclick="del_work(this)">删除</span></td>`
                    } else {
                        busWork += `<td><span class="change_tab" style="color: #999;">已完结</span> <span></span></td>`
                    }
                    busWork += `</tr>`
                }
                $(".all_work > div > .tbody > .table_data").empty().append(busWork);
                break;
            default:
                alert(dat["非法操作~~~~！"]);
        }
    } else {
        alert(dat["errMsg"]);
    }
}

// delete 请求 ajax 封装 ===================================
function delDataEdit(ul, dat, callback) {
    $.ajax({
        type: "POST",
        url: ul,
        dataType: "json",
        data: dat,
        success: function (ret) {
            callback(ret, ul);
        },
    });
}

// 删除数据 delete 请求 callback 方法
function deleteCallBack(msg, url) {
    if (msg["success"] !== "") {
        switch (url) {
            case ("/admin/del_client"):
                // 重新获取所有客户数据并渲染？
                window.location.href = "/admin";
                break;
            case ("/admin/del_mater"):
                // 重新获取所有物料数据并渲染？
                window.location.href = "/admin";
                break;
            case ("/admin/del_user"):
                // alert(msg["success"])
                // 重新获取所有客户数据并渲染？
                window.location.href = "/admin";
                break;
            case ("/admin/del_printer"):
                // 重新获取所有打印机数据并渲染？
                window.location.href = "/admin";
                break;
            case ("/admin/del_wuLiu"):
                // 重新获取所有物流数据并渲染？
                window.location.href = "/admin";
                break;
            case ("/admin/del_bomName"):
                window.location.href = "/admin";
                break;
            default:
                alert("非法操作~~~!");
        }
    } else {
        alert(msg["errMsg"]);
    }
}

// put 更改数据 ajax 方法封装 ===============================
function putFunc(ul, msg, callback) {
    $.ajax({
        type: "PUT",
        url: ul,
        data: msg,
        dataType: "json",
        success: function (msg) {
            callback(ul, msg);
        },
    });
}

// 修改数据 put 请求 callback 方法
function putCallback(url, msg) {
    if (msg["success"] !== "") {
        switch (url) {
            case ("/admin/change_user"):
                // 重新获取所有用户信息，并重新渲染?
                window.location.href = "/admin";
                break;
            case ("/admin/change_client"):
                // 重新获取所有客户信息，并重新渲染?
                window.location.href = "/admin";
                break;
            case ("/admin/change_mater"):
                // 重新获取所有物料信息，并重新渲染?
                window.location.href = "/admin";
                break;
            case ("/admin/changeWuLiu"):
                // 重新获取所有物流信息，并重新渲染?
                window.location.href = "/admin";
                break;
            case ("/admin/changeBomName"):
                window.location.href = "/admin";
                break;
            case ("/admin/change_print"):
                window.location.href = "/admin";
                break;
            default:
                alert("非法操作~~~~~！")
                break;
        }
    } else {
        switch (url) {
            case ("/admin/change_user"):
                $("#change_user_error").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/change_client"):
                $("#change_client_error").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/change_mater"):
                $("#change_mater_error").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/changeWuLiu"):
                $("#change_wuLiu_error").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/changeBomName"):
                $("#change_bomName_error").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/change_print"):
                $("#change_print_error").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            default:
                alert("非法操作~~~~~！")
                break;
        }
    }
}

// post 提交数据到后端 ajax 方法封装 =========================
function postSubmit(addr, msg, callback) {
    $.ajax({
        type: "POST",
        url: addr,
        // contentType: "application/json",
        dataType: "json",
        data: msg,
        success: function (dat) {
            callback(addr, dat);
        },
    });
}

// 添加数据 post 请求 callback(回调函数) 方法
function postCallFunc(url, msg) {
    if (msg["success"] !== "") {
        switch (url) {
            case ("/admin/add_user"):
                // 提交成功清空输入框中的内容
                $("#login_account").val("");
                $("#userName").val("");
                $("#user_passwd").val("");
                $("#user_repasswd").val("");
                // $(".user_send_con").css("display", "none");
                // 重新获取所有用户信息？
                // window.location.href = "/admin";
                // $(".user_exhibit").css("display", "block");
                break;
            case ("/admin/add_client"):
                $("#clientName").val("");
                $("#client_phone").val("");
                $("#client_addr").val("");
                // $(".client_send_con").css("display", "none");
                // 重新获取所有客户信息？
                // window.location.href = "/admin";
                // $(".client_exhibit").css("display", "block");
                break;
            case ("/admin/add_mater"):
                $("#materName").val("");
                $("#materModel").val("");
                $("#brand").val("");
                $(".mater_send_con").css("display", "none");
                // 重新获取所有物料信息，并隐藏添加物料界面重新显示渲染物料内容界面
                // window.location.href = "/admin";
                // $(".mater_exhibit").css("display", "block");
                break;
            case ("/admin/add_printer"):
                $("#printName").val("");
                $("#printAddr").val("");
                $(".print_send_con").css("display", "none");
                // 重新获取所有物料信息，并隐藏添加物料界面重新显示渲染物料内容界面
                // window.location.href = "/admin";
                // $(".print_exhibit").css("display", "block");
                break;
            case ("/admin/print_code"):
                var codeSubBtn = $(".sub_btn_print")
            function checkSonMenu(val) {
                if ($(val).text() === "条码打印") {
                    $(".user_mgr, .client_mgr, .mater_add, .mater_change, .mater_mgr, .print_add, .print_change, .print_mgr").css("display", "none");
                    $(".code_mgr").css("display", "none").css("display", "block");
                    $(".choose").children("input[name='chose']").attr("checked", false);
                    codeSubBtn.siblings().children("input[name='print_version']").val("");
                    codeSubBtn.siblings().children("input[name='mode']").val("");
                    codeSubBtn.siblings().children("input[name='client']").val("");
                    codeSubBtn.siblings().children("select[name='sale']").val("");
                    codeSubBtn.siblings().children("input[name='createDate']").val("");
                    codeSubBtn.siblings().children("input[name='printNum']").val("");
                    codeSubBtn.siblings().children("select[name='printer']").val("");
                }
            }

                $(document).ready(function () {
                    checkSonMenu("#CodeMenu");
                });
                break;
            case ("/admin/goods_up"):
                // window.location.href = "/admin"
                $("#product").val("");
                $("#product_model").val("");
                $(".table_select").val("");
                var tbody = $("#product_scrollTable").children("div").children(".tbody").children(".table_data");
                tbody.children("tr").children("td").children("input[name='number']").val("");
                tbody.children("tr").children("td").children("select").val("");  // todo 清除不了
                break;
            case ("/admin/addWuLiu"):
                $("#wuLiu_name").val("");
                $("#butt_people").val("");
                $("#butt_phone").val("");
                break;
            case ("/admin/addBomName"):
                $("#addBomName").val("");
                break;
            default:
                alert("非法操作~~~~888!");
        }
    } else {
        switch (url) {
            case ("/admin/add_user"):
                // alert("=====================")
                errShow("#add_user_error", msg["errMsg"]);
                // $("#add_user_error").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/add_client"):
                errShow("#add_client_error", msg["errMsg"]);
                // $("#add_client_error").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/add_mater"):
                errShow("#add_mater_error", msg["errMsg"]);
                // $("#add_mater_error").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/add_printer"):
                errShow("#add_print_error", msg["errMsg"]);
                // $("#add_print_error").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/print_code"):
                errShow("#print_code_err", msg["errMsg"]);
                // $("#print_code_err").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/addWuLiu"):
                errShow("#add_wuLiu_error", msg["errMsg"]);
                // $("#print_code_err").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            case ("/admin/goods_up"):
                errShow("#materBindErr", msg["errMsg"]);
                // $("#print_code_err").css("display", "block").empty().html(msg["errMsg"]).delay(6000).fadeOut();
                break;
            default:
                alert("非法操作~~~~!");
        }
    }
}

function errShow(erid, errMsg) {
    $(erid).css("display", "block").empty().html(errMsg).delay(6000).fadeOut();
}
