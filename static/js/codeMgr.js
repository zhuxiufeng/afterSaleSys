function getRadioValue() {
    let choseInput = $(".choose").children("input[name='chose']");
    let radioVal = "";
    for (i = 0; i < choseInput.length; i++) {
        if (choseInput[i].checked) {
            radioVal = choseInput[i].value;
        }
    }
    return radioVal;
}

// 监听配置选择框成品获取选择的内容，然后发起 get 请求获取关联的物料信息
$(() => {
    $("#print_version").change(() => {
        // console.log($('#print_version').val())
        $.ajax({
            type: "GET",
            url: "/admin/PrintGetAllMater",
            dataType: "json",
            data: {
                "ver_id": $('#print_version').val().split("-")[0],
            },
            success: (dat) => {
                if (dat["success"] !== "") {
                    var pcpdehtml = `
                        <option value=""></option>
                    `
                    for (var mates in dat["success"]) {
                        pcpdehtml += `
                        <option value="` + dat["success"][mates]["mid"] + "-"+ dat["success"][mates]["mname"] + "-" + dat["success"][mates]["model"] + `"></option>
                    `
                    }
                    $("#modeName").empty().append(pcpdehtml);
                } else {
                    printCodeErrShow(dat["errMsg"])
                }
            }
        });
    });
});

function sub_print(val) {
    var codeHead = getRadioValue();
    if (codeHead === "") {
        printCodeErrShow("打印失败，您尚未选择序号头字母~~！");
        return;
    }
    var mater = $(val).siblings().children("input[name='mode']").val().split("-")[0];
    if (mater === "") {
        printCodeErrShow("打印失败，您尚未选择关联的物料信息~~！");
        return;
    }
    var ver = $(val).siblings().children("input[name='print_version']").val().split("-")[0];
    if (ver === "") {
        printCodeErrShow("打印失败，您尚未选择成品的配置信息~~！");
        return;
    }
    var client = $(val).siblings().children("input[name='client']").val().split("-")[0];
    if (client === "") {
        printCodeErrShow("打印失败，您尚未选择关联的客户信息~~！");
        return;
    }
    var sale = $(val).siblings().children("select[name='sale']").val();
    if (sale === "") {
        printCodeErrShow("打印失败，您尚未选择关联的销售/业务信息~~！");
        return;
    }
    var date = $(val).siblings().children("input[name='createDate']").val();
    if (date === "") {
        printCodeErrShow("打印失败，您尚未选择出厂日期信息~~！");
        return;
    }
    var num = $(val).siblings().children("input[name='printNum']").val();
    if (num === "") {
        printCodeErrShow("打印失败，您尚未指定打印份数~~！");
        return;
    }
    var printer = $(val).siblings().children("select[name='printer']").val();
    if (printer === "") {
        printCodeErrShow("打印失败，您尚未指定打印机哦~~！");
        return;
    }
    postSubmit("/admin/print_code", {
        "codeHead": codeHead,
        "cfgVer": ver,
        "mater": mater,
        "client": client,
        "sale": sale,
        "date": date,
        "num": num,
        "printer": printer,
    }, postCallFunc)
}

function printCodeErrShow(msg) {
    $("#print_code_err").css("display", "block").empty().html(msg).delay(6000).fadeOut();
}