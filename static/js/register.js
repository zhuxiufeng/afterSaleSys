// 使用正则验证用书输入的手机号格式是否正确
function isPhone(value) {
    var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
    return !(value === "" || value.length !== 11 || !mobile.test(value));
}

function registerBtn() {
    var username = $(".login_box > p > .input_box[name='username']").val();
    var phone = $(".login_box > p > .input_box[name='phone']").val();
    if (!isPhone(phone)) {
        alert("抱歉！您输入的联系方式格式有误，请重新填写，谢谢!");
        return
    }
    $.ajax({
        type: "POST",
        url: "/registry",
        data: {
            "username": username,
            "phone": phone,
        },
        success: function (dat) {
            if (dat["success"]) {
                window.location.href = "/login";
            }
            if (dat["errMsg"]) {
                $(".err_box > span").css("display", "block").empty().html(dat["errMsg"]).delay(6000).fadeOut();
            }
        },
    });
}
