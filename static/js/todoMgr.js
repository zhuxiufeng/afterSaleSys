// 待办事项状态选择按钮事件方法
function choose_todo(val) {
    switch ($(val).text()) {
        case "待审核":
            $(".finish_table, .all_table").css("display", "none");
            $(".undone_table").css("display", "block");
            // 发 get 请求获取数据
            getUpcomingData(0, upcomingBackFunc);
            break;
        case "已审核":
            $(".undone_table, .all_table").css("display", "none");
            $(".finish_table").css("display", "block");
            getUpcomingData(1, upcomingBackFunc);
            break;
        case "所    有":
            $(".undone_table, .finish_table").css("display", "none");
            $(".all_table").css("display", "block");
            getUpcomingData("", upcomingBackFunc);
            break;
        default:
            alert("非法操作====!");
            break;
    }
}

function getUpcomingData(statId, upcomingCallBackFunc) {
    $.ajax({
        type: "GET",
        url: "/admin/getUpcoming",
        contentType: "application/json",
        dataType: "json",
        data: {"statId": statId},
        success: function (dat) {
            if (dat["success"] !== "") {
                upcomingCallBackFunc(statId, dat)
            } else {
                alert(dat["errMsg"]);
            }
        },
    })
}

function upcomingBackFunc(statId, ret) {
    var upHtml = ``;
    if (statId === 0) {
        // 开始渲染未完成事项 .undone_list-group 类下的数据 li 标签
        for (var up in ret["success"]) {
            upHtml += `
                <li class="list-group-item list-group-item-default">
                    <b>` + ret["success"][up]["wid"] + `</b>
                    【<strong>` + ret["success"][up]["client_name"] + `</strong>】
                     创建了一个: <strong>` + ret["success"][up]["up_name"] + `</strong>
                     待处理，
<!--                     运单号：<strong style="color: #0066FF">` + ret["success"][up]["log_num"] + `</strong>-->
                    <i onclick="detailBtn(this)">详情</i>
                </li>
            `
        }
        $(".undone_table > .table_info > .undone_list-group").empty().append(upHtml);
    } else if (statId === 1) {
        // 开始渲染已完成事项 .finish_list-group 类下的数据 li 标签
        for (var upcom in ret["success"]) {
            upHtml += `
                <li class="list-group-item list-group-item-success">
                    <b>` + ret["success"][upcom]["wid"] + `</b>
                     客户:【<strong>` + ret["success"][upcom]["client_name"] + `</strong>】
                     的: <strong>` + ret["success"][upcom]["up_name"] + `</strong> 工单
                     运单号：<strong>` + ret["success"][upcom]["log_num"] + `</strong> 已被处理
                    <i onclick="detailBtn(this)">详情</i>
                </li>
            `
        }
        $(".finish_table > .table_info > .finish_list-group").empty().append(upHtml);
    } else {
        var allHTml = ``
        // 开始渲染所有事项 .all_list-group 类下的数据 li 标签到页面，并且根据判断给 li 标签添加不同的类
        // console.log(ret["success"]);
        for (var allUp in ret["success"]) {
            // console.log(ret["success"][allUp]["process"])
            if (ret["success"][allUp]["process"] === 0) {
                allHTml += `
                    <li class="list-group-item list-group-item-default">
                        <b>` + ret["success"][allUp]["wid"] + `</b>
                        【<strong>` + ret["success"][allUp]["client_name"] + `</strong>】
                         创建了一个: <strong>` + ret["success"][allUp]["up_name"] + `</strong>
                         待处理，
<!--                         运单号：<strong style="color: #0066FF">` + ret["success"][allUp]["log_num"] + `</strong>-->
                        <i onclick="detailBtn(this)">详情</i>
                    </li>
                `
            } else if (ret["success"][allUp]["process"] === 1) {
                allHTml += `
                    <li class="list-group-item list-group-item-success">
                        <b>` + ret["success"][allUp]["wid"] + `</b>
                        【<strong>` + ret["success"][allUp]["client_name"] + `</strong>】
                         的: <strong>` + ret["success"][allUp]["up_name"] + `</strong> 工单
<!--                         运单号：<strong style="color: #0066FF">` + ret["success"][allUp]["log_num"] + `</strong> -->
                         已被处理
                        <i onclick="detailBtn(this)">详情</i>
                    </li>
                `
            }
        }
        $(".all_table > .table_info > .all_list-group").empty().append(allHTml);
    }
}

// 获取事件详细信息按钮事件
function detailBtn(val) {
    if ($(val).siblings("b").text() === "") {
        $("#upcoming_err").empty().css("display", "block").html("非法操作~~~").delay(6000).fadeOut();
        return;
    }
    // 发送 ajax 请求获取事件相信信息
    $.ajax({
        type: "GET",
        url: "/admin/getUpcomingDetail",
        contentType: "application/json",
        dataType: "json",
        data: {"link_Id": $(val).siblings("b").text()},
        success: function (ret) {
            if (ret["success"] !== "") {
                var detailHt = ``
                // 0待收货，1已收货，2维修中，3外发中，4返途中，5已完成
                for (var detail in ret["success"]) {
                    // 开始往 $(".del_upcoming_info > ul") 中渲染数据
                    detailHt += `
                        <li class="list-group-item list-group-item-default"><b>工单号：</b>` + ret["success"][detail]["wid"] + `</li>
                        <li class="list-group-item list-group-item-default"><b>工单类型：</b>` + ret["success"][detail]["up_name"] + `</li>
                        <li class="list-group-item list-group-item-default"><b>客户名称：</b>` + ret["success"][detail]["client_name"] +`</li>
                        <li class="list-group-item list-group-item-default"><b>设备名称：</b>` + ret["success"][detail]["mname"] + `</li>
                        <li class="list-group-item list-group-item-default"><b>设备数量：</b>` + ret["success"][detail]["mater_num"] + `</li>
                        <li class="list-group-item list-group-item-default"><b>故障原因：</b>` + ret["success"][detail]["issue_describe"] + `</li>
                        <li class="list-group-item list-group-item-default"><b>快递单号：</b>` + ret["success"][detail]["log_num"] + `</li>
                        <li class="list-group-item list-group-item-default"><b>关联业务：</b>` + ret["success"][detail]["user_name"] + `</li>
                    `
                    switch (ret["success"][detail]["state"]) {
                        case 0:
                            detailHt += `
                                <li class="list-group-item list-group-item-default"><b>当前状态：</b>待收货</li>
                            `
                            break;
                        case 1:
                            detailHt += `
                                <li class="list-group-item list-group-item-default"><b>当前状态：</b>已收货，但尚未指定维修人员维修</li>
                            `
                            break;
                        case 2:
                            detailHt += `
                                <li class="list-group-item list-group-item-default"><b>当前状态：</b>内部人员维修中</li>
                            `
                            break;
                        case 3:
                            detailHt += `
                                <li class="list-group-item list-group-item-default"><b>当前状态：</b>外发给外单位维修中</li>
                            `
                             break;
                        case 4:
                            detailHt += `
                                <li class="list-group-item list-group-item-default"><b>当前状态：</b>已发出给客户</li>
                            `
                            break;
                        case 5:
                            detailHt += `
                                <li class="list-group-item list-group-item-default"><b>当前状态：</b>当前工单已完结</li>
                            `
                            break;
                    }
                }
                $(".del_upcoming_info > ul").empty().append(detailHt);
            } else {
                // 展示错误信息
                $("#upcoming_err").empty().css("display", "block").html(ret["errMsg"]).delay(6000).fadeOut();
            }
        }
    });
    // 显示弹框
    $(".upcoming_send_con").css("display", "block");
}

// 事件详细信息审核按钮事件
function audit_btn(val) {
    var workId = $(val).siblings(".del_upcoming_info").children("ul").children("li").eq(0).text()
    if (workId === "") {
        $("#upcomingErr").empty().text("错误，工单数据异常请检查后重试~!").delay(6000).fadeOut();
        return;
    }
    $.ajax({
        type: "PUT",
        url: "/admin/changeWorkProcessStat",
        dataType: "json",
        data: {
            "workId": workId.split("：")[1],
        },
        success: (ret) => {
            if (ret["success"] !== "") {
                // todo 审批成功直接隐藏弹框,后返回主页?
                $(".upcoming_send_con").css("display", "none");
                window.location.href="/admin";
            }else {
                $("#upcomingErr").empty().text("错误，工单数据异常请检查后重试~!").delay(6000).fadeOut();
            }
        }
    });
}