// 产品组装资料表下拉选择框监听事件
function getSelVal(val) {
    $(val).change(() => {
        if ($(val).val() !== "") {
            $(val).parent().siblings("td").eq(0).text($(val).val().split(" ")[0]);
            $(val).parent().siblings("td").eq(1).text($(val).val().split(" ")[2]);
        } else {
            $(val).parent().siblings("td").eq(0).text("");
            $(val).parent().siblings("td").eq(1).text("");
        }
    });
}

// 组装产品提交按钮事件
function submitBtn(val) {
    var dict = {};
    var childrenId = [];
    var childrenNum = [];
    // 获取成品信息
    dict["productId"] = $(val).parent().siblings(".host").children("div").eq(0).children("div").children("#product").val().split("-")[0]; // todo

    dict["productModel"] = $(val).parent().siblings(".host").children("div").eq(1).children("div").children("input[name='product_model']").val();
    dict["bomName"] = $(val).parent().siblings(".host").children("div").eq(2).children("div").children("#pro_bom_name").val().split("-")[0];
    if (dict["productName"] === "" || dict["productModel"] === "" || dict["bomName"] === "") {
        alert("成品名称、规格型号、配置版本不允许留空，请返回检查并重新填写！");
        return;
    }
    // 获取配件信息
    var tbody = $(val).siblings("#product_scrollTable").children("div").children(".tbody").children(".table_data");
    var trLabel = $(val).siblings("#product_scrollTable").children("div").children(".tbody").children(".table_data").children("tr");
    for (i = 0; i < trLabel.length; i++) {
        // 此处应判断某个选择框是否为空，为空则跳过
        if (tbody.children("tr").eq(i).children("td").eq(0).text() !== "") {
            childrenId.push(tbody.children("tr").eq(i).children("td").eq(0).text());
            childrenNum.push(tbody.children("tr").eq(i).children("td").children("input[name='number']").val());
        }
    }
    dict["childrenIds"] = childrenId;
    dict["childrenNums"] = childrenNum;
    var dats =  JSON.stringify(dict);
    postSubmit("/admin/goods_up", dats, postCallFunc);
}

// 监听下拉选择框事件
$(()=>{
    $("#product").change(() => {
        var productId = $('#product').val().split("-")[0];
        if (productId !== "") {
            // $("#product_model").val("这是伪数据");  // 伪代码
            // 选择住产品后，根据主产品去数据库获取相应的其他信息，并填入给一个型号和配置版本输入框
            $.ajax({
                type: "GET",
                url: "/admin/getProductInfo",
                contentType: "application/json",
                dataType: "json",
                data: {
                    "product_id":productId,
                },
                success: (ret) => {
                    if (ret["success"] !== "") {
                        $("#product_model").empty().val(ret["success"]);  // 伪代码
                        // 开始渲染产品商品组装主产品的其他属性
                    } else {
                        alert(ret["errMsg"])
                    }
                }
            })
        }
    });
});