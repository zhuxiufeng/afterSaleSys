package utils

import (
	"asystem/cfg"
	"errors"
	"fmt"
	"time"
)

const (
	DATEFORMAT  = "2006-01-02"
	DATEFORMAT2 = "2006-01-02 03:04:05"
)

// StrToUnix 字符串转时间戳方法
func StrToUnix(str string) (int, error) {
	timeFot, err := time.Parse(DATEFORMAT, str)
	if err != nil {
		return 0, errors.New(fmt.Sprintf("提交的时间格式不正确，错误代码：%v", err))
	}
	return int(timeFot.Unix()), nil
}

func StrToUnix2(str string) (int, error) {
	timeFot, err := time.Parse(DATEFORMAT2, str)
	if err != nil {
		return 0, errors.New(fmt.Sprintf("提交的时间格式不正确，错误代码：%v", err))
	}
	return int(timeFot.Unix()), nil
}

// UnixToTime 字符串转 time.Time 类型
func UnixToTime(unix string) (time.Time, error) {
	var tt time.Time
	intTime, err := StrToInt(unix)
	if err != nil {
		return tt, errors.New(fmt.Sprintf("提交的时间格式不正确，错误代码：%v", err))
	}
	return time.Unix(int64(intTime), 0), nil
}

// StrToTimeFat 将字符串类型的时间戳转换为指定格式的时间数据
func StrToTimeFat(val string) (timeFat string, err error) {
	timeStamp, err := UnixToTime(val)
	if err != nil {
		return "", errors.New(fmt.Sprintf("提交的时间格式不正确，错误代码：%v", err))
	}
	return timeStamp.Format("2006-01-02 03:04:05"), nil
}

// CalTimeEquation 保修期计算
func CalTimeEquation(unix string) (bool, error) {
	oldTime, err := UnixToTime(unix)
	if err != nil {
		return false, errors.New(fmt.Sprintf("计算保修期失败，错误代码：%v", err))
	}
	codeCfg, err := cfg.LoadCfg(cfg.FILEPATH)
	if err != nil {
		return false, errors.New(fmt.Sprintf("计算保修期失败，错误代码：%v", err))
	}
	// 当前时间减去条码上的时间，如果大于一年，则说明以过保修期
	if time.Now().Sub(oldTime).Hours() <= codeCfg.CodeCfg.WarrantyPeriod*24 {
		return true, nil
	} else {
		return false, nil
	}
}
