package utils

import (
	"strings"
)

var codeHead []string = []string{"LC", "LF", "LN", "LD", "LX", "LB", "LP", "LZ", "WM", "DP"}

// IsCodeHead 判断条码是否已xx开头方法
func IsCodeHead(code string) bool {
	for _, val := range codeHead {
		if strings.HasPrefix(code, val) {
			return true
		}
	}
	return false
}
