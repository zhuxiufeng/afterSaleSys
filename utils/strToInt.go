package utils

import (
	"errors"
	"fmt"
	"strconv"
)

// StrToInt 转换string 类型到 int
func StrToInt(str string) (val int, err error) {
	if val, err = strconv.Atoi(str); err != nil {
		return 0, errors.New(fmt.Sprintf("提交的数据类型有误操作失败，错误代码：%v", err))
	}
	return
}
