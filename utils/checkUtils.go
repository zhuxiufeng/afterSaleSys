package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"regexp"
)

type UsersJsonData struct {
	UserName string `json:"userName"`
	Tid      int    `json:"tid"`
}

// CheckPhone 检查手机号码格式
func CheckPhone(phone string) error {
	phoneReg := "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$"
	reg := regexp.MustCompile(phoneReg)
	if !reg.MatchString(phone) {
		return errors.New(fmt.Sprintf("输入的手机号码格式不正确，请更改正确后重试!"))
	}
	return nil
}

// CheckPermission 检查用户操作权限
func CheckPermission(c *gin.Context) error {
	se := InitSession(c, "admin")
	data := se.GetSession(se.SesKey.SerSessKey)
	//data, er := db.Rdb.GetRedisData("username")
	//if er != nil {
	//	return errors.New(fmt.Sprintf("抱歉！您当前尚未登录，错误代码: %v", er))
	//}
	usrObj, er := UnJsonMarshal(data)
	if er != nil {
		return errors.New(fmt.Sprintf("删除失败，给您带来不便敬请谅解，请将该错误代码: %v 上报给系统管理员", er))
	}
	if usrObj.Tid != 1 {
		return errors.New(fmt.Sprintf("抱歉~！您没有权限进行该操作，请切换拥有系统管理员权限的用户后操作~谢谢！"))
	}
	return nil
}

// JsonMarshal 序列化数据
func JsonMarshal(data interface{}) ([]byte, error) {
	dataByte, err := json.Marshal(data)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("redis 操作前序列化数据错误，错误代码：%v", err))
	}
	return dataByte, nil
}

// UnJsonMarshal 反序列化数据
func UnJsonMarshal(data interface{}) (userInfo *UsersJsonData, err error) {
	if ret, ok := data.([]byte); ok {
		err = json.Unmarshal(ret, &userInfo)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("session 操作前反序列化数据错误，错误代码：%v", err))
		}
	} else {
		return nil, errors.New(fmt.Sprintf("session 操作前获取到的 session 数据类型断言错误，错误代码：%v", err))
	}
	return userInfo, nil
}
