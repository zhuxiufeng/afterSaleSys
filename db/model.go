package db

import (
	"asystem/cfg"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"time"
)

var db *sqlx.DB

// InitDb 初始化数据库连接
func InitDb() error {
	// 读取配置文件
	sqlCfg, err := cfg.LoadCfg(cfg.FILEPATH)
	if err != nil {
		return err
	}
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True", sqlCfg.Mysql.User, sqlCfg.Mysql.Passwd, sqlCfg.Mysql.URL, sqlCfg.Mysql.DbName)
	db, err = sqlx.Connect("mysql", dsn)
	if err != nil {
		return err
	}
	db.SetMaxOpenConns(200)
	db.SetMaxIdleConns(100)
	db.SetConnMaxLifetime(5 * time.Minute)
	return nil
}

// Model 汇总
type Model struct {
	User       *Users      // 用户表
	UsrTYpe    *UserType   // 用户类型表
	Client     *Client     // 客户表
	ClientAddr *ClientAddr // 客户地址记录表
	Mater      *Mater      // 物料表
	OutFirm    *OutFirm    // 外发单位记录表
	Printer    *Printer    // 打印机记录表
	Logistics  *Logistics  // 物流表
	MaterBom   *MaterBom   // 物料BOM
	Work       *Work       // 工单任务表
	UpComing   *UpComing   // 待办事项表
	Code       *Code       // 条码记录表
	CodeBom    *CodeBom    // 条码BOM
	BomNameTab *BomNameTab // bom名称表
	MaterType  *MaterType  // 物料类型表
	Province   *Province   // 省份表
	City       *City       // 城市表
	Pay        *Pay        // 支付信息记录表
	NewProduct *NewProduct //新品推荐表
}

type Users struct {
	Uid      int    `db:"uid" json:"uid"`             // 欧诺个户Id
	TId      int    `db:"t_id" json:"t_id"`           // 关联的用户类型Id
	UserName string `db:"user_name" json:"user_name"` // 用户姓名
	Account  string `db:"account" json:"account"`     // 登录账户
	Passwd   string `db:"passwd" json:"passwd"`       // 登录密码
	TypeName string `db:"type_name" json:"type_name"` // 用户类型属于用户表中的字段
}

type UserType struct {
	UtId     int    `db:"ut_id"`                      // 用户类型Id
	TypeName string `db:"type_name" json:"type_name"` // 用户类型名称
}

type Client struct {
	Cid        int     `db:"cid" json:"cid"`                 // 客户Id
	UId        int     `db:"u_id" json:"u_id"`               // 关联的销售Id
	ClientName string  `db:"client_name" json:"client_name"` // 客户姓名
	Phone      string  `db:"phone" json:"phone"`             // 联系方式
	Addr       *string `db:"addr" json:"addr"`               // 联系地址
	UserName   string  `db:"user_name" json:"user_name"`     //关联的业务人员
}

type ClientAddr struct {
	AddrId   int    `db:"addr_id" json:"addr_id"`     // 地址Id
	ClientId int    `db:"client_id" json:"client_id"` // 关联的客户Id
	Addr     string `db:"addr" json:"addr"`           // 客户地址
}

type Mater struct {
	Mid           int    `db:"mid" json:"mid"`                         // 物料Id
	Mname         string `db:"mname" json:"mname"`                     // 物料名称
	Brand         string `db:"brand" json:"brand"`                     // 品牌
	Model         string `db:"model" json:"model"`                     // 规格型号
	MTypeId       int    `db:"mt_id" json:"mt_id"`                     // 物料类型Id
	MaterTypeName string `db:"mater_type_name" json:"mater_type_name"` // 物料类型
}

type OutFirm struct {
	Oid         int    `db:"oid" json:"oid"`                     // 外发公司Id
	OutFirmName string `db:"out_firm_name" json:"out_firm_name"` // 公司名称
	People      string `db:"people" json:"people"`               // 对接人
	Phone       string `db:"phone" json:"phone"`                 // 联系方式
	OAddr       string `db:"oaddr" json:"oaddr"`                 // 联系地址
}

type Printer struct {
	Pid       int    `db:"pid" json:"pid"`                 // 打印机Id
	Pname     string `db:"p_name" json:"p_name"`           // 打印机名
	PNetAddr  string `db:"p_net_addr" json:"p_net_addr"`   // 打印机地址
	BTSerAddr string `db:"bt_ser_addr" json:"bt_ser_addr"` // bt 软件服务地址
}

type Logistics struct {
	Lid   int    `db:"lid" json:"lid"`     // 物流Id
	Lname string `db:"lname" json:"lname"` // 物流名称
	Butt  string `db:"butt" json:"butt"`   // 对接人
	Phone string `db:"phone" json:"phone"` // 联系方式
}

type MaterBom struct {
	Mbid      int `db:"mbid" json:"mbid"`               // 组装Id
	MId       int `db:"m_id" json:"m_id"`               // 物料Id
	FmId      int `db:"fm_id" json:"fm_id"`             // 父级物料Id
	MaterLv   int `db:"mater_lv" json:"mater_lv"`       // 层级
	Dosage    int `db:"dosage" json:"dosage"`           // 用量
	BomNameId int `db:"bom_name_id" json:"bom_name_id"` // 关联的Bom 名称Id
}

// MaterType 物料类型
type MaterType struct {
	MtId          int    `db:"mt_id" json:"mt_id"`                      // 物料类型Id
	MaterTypeName string `db:"mater_type_name", json:"mater_type_name"` // 物料类型名称
}

// Work 工单记录表，做数据库操作时应注意该表中的内容关联的 Id 需要分清楚
type Work struct {
	Wid           int    `db:"wid" json:"wid"`                       // 工单Id
	Process       int    `db:"process" json:"process"`               // 流程控制Id
	State         int    `db:"state" json:"state"`                   // 状态控制Id
	PayStatus     int    `db:"pay_status" json:"pay_status"`         // 付款状态
	MId           int    `db:"m_id" json:"m_id"`                     // 关联的物料Id
	UId           int    `db:"u_id" json:"u_id"`                     // 关联的销售人员Id
	WuId          int    `db:"wu_id" json:"wu_id"`                   // 关联的维修人员Id
	CId           int    `db:"c_id" json:"c_id"`                     // 关联的客户Id
	LId           int    `db:"l_id" json:"l_id"`                     // 关联的物流Id
	MaterNum      int    `db:"mater_num" json:"mater_num"`           // 客户返回的物料数量
	OId           int    `db:"o_id" json:"o_id"`                     // 关联的外发公司Id
	WorkType      string `db:"work_type" json:"work_type"`           // 工单类型
	IssueDescribe string `db:"issue_describe" json:"issue_describe"` // 问题描述
	LogNum        string `db:"log_num" json:"log_num"`               // 快递单号
	CreateDate    string `db:"create_date" json:"create_date"`       // 创建时间
	MaterName     string `db:"mname" json:"mname"`                   // 物料名称
	ONumber       string `db:"o_number" json:"o_number"`             // 外发维修物流单号
}

// ReturnWork 返回工单记录数据表
type ReturnWork struct {
	RwId       int    `db:"rwid" json:"rwId"`               // id
	ExpressId  int    `db:"express_id" json:"express_id"`   // 关联的快递公司Id
	WorkId     int    `db:"work_id" json:"work_id"`         // 关联的工单Id
	RetNum     int    `db:"ret_num" json:"ret_num"`         // 返客数量
	ExpressNum string `db:"express_num" json:"express_num"` // 返回的快递单号
	CrateDate  string `db:"crate_date" json:"crate_date"`   // 返回时间
}

type UpComing struct {
	UpId   int    `db:"upid" json:"upid"`       // 待办事项Id
	WID    int    `db:"w_id" json:"w_id"`       // 关联的工单Id
	UID    int    `db:"u_id" json:"u_id"`       // 关联的人员Id
	MID    int    `db:"m_id" json:"m_id"`       // 关联的物料Id
	StatId int    `db:"stat_id" json:"stat_id"` // 事件状态Id
	UpName string `db:"up_name" json:"up_name"` // 待办事项名称
}

type Code struct {
	Bid          int    `db:"bid" json:"bid"`                       // 条码Id
	CId          int    `db:"c_id" json:"c_id"`                     // 关联的客户Id
	UId          int    `db:"u_id" json:"u_id"`                     // 关联的用户Id
	MId          int    `db:"m_id" json:"m_id"`                     // 关联的物料Id
	OutLibUserId int    `db:"outlib_user_id" json:"outlib_user_id"` // 出库操作人员Id
	BomNamId     int    `db:"bom_name_id" json:"bom_name_id"`       // 关联的Bom 名称Id
	WarrantyTime int    `db:"warranty_time" json:"warranty_time"`   // 保修期
	BarCode      string `db:"barcode" json:"barcode"`               // 条码
	OutLibDate   string `db:"outlib_date" json:"outlib_date"`       // 出库时间，用来计算保修期
	LinkOrder    string `db:"link_order" json:"link_order"`         // 关联的快递单号或生产单号
}

type CodeBom struct {
	BbId   int `db:"bbid" json:"bbid"`       // 条码 Bom Id
	BId    int `db:"b_id" json:"b_id"`       // 关联的条码Id
	ParId  int `db:"par_id" json:"par_id"`   // 父级条码Id
	CodeLv int `db:"code_lv" json:"code_lv"` // 层级
}

type BomNameTab struct {
	BnId  int    `db:"bn_id" json:"bn_id"`   // id
	BName string `db:"b_name" json:"b_name"` // bom 名称
}

// Province 省份表
type Province struct {
	ProId   int    `db:"ProId" json:"ProId"`     // Id
	ProName string `db:"ProName" json:"ProName"` // 省份名称
}

// City 城市表
type City struct {
	CityId   int    `db:"CityId" json:"CityId"`     // Id
	ProID    int    `db:"ProID" json:"ProID"`       // 关联的身份ID
	CityName string `db:"CityName" json:"CityName"` // 城市名称
}

// Pay 支付信息记录表
type Pay struct {
	PayId     int    `db:"pay_id" json:"pay_id"`
	Amount    int    `db:"amount" json:"amount"`         //金额',
	WorkId    int    `db:"work_id" json:"work_id"`       //关联的工单Id',
	SaleId    int    `db:"sale_id" json:"sale_id"`       // 关联的销售人员Id',
	ClientId  int    `db:"client_id" json:"client_id"`   // 关联的客户Id',
	MaterId   int    `db:"mater_id" json:"mater_id"`     // 关联的物料Id',
	PayStat   int    `db:"pay_stat" json:"pay_stat"`     // 支付状态,0位未支付，1 为已支付',
	PayMethod string `db:"pay_method" json:"pay_method"` // 支付方式',
	PayDate   string `db:"pay_date" json:"pay_date"`     // 支付时间',
}

// PayCarousel 支付页面图片轮播表
type PayCarousel struct {
	CarouselId int    `db:"carousel_id" json:"carousel_id"` // Id
	LinkUrl    string `db:"link_url" json:"link_url"`       // 图片链接的地址
	ImgUrl     string `db:"img_url" json:"img_url"`         // 图片地址
}

// WorkTmp 临时表
type WorkTmp struct {
	State         int     `db:"state" json:"state"`                   // 流程Id
	WorkType      string  `db:"work_type" json:"work_type"`           // 工单类型名称
	Matename      string  `db:"mname" json:"mname"`                   // 物料名称
	LogIsNum      string  `db:"log_num" json:"log_num"`               // 客户发送的快递单号
	IssueDescribe string  `db:"issue_describe" json:"issue_describe"` // 问题描述
	CreateDate    string  `db:"create_date" json:"create_date"`       // 客户提交的工单的数据
	ExpressNum    *string `db:"express_num" json:"express_num"`       // 返回给客户的快递单号
	CateDate      *string `db:"crate_date" json:"cate_date"`          // 返回给客户的时间
}

// UpcomingTmp 待办事项临时表
type UpcomingTmp struct {
	Wid           int    `db:"wid" json:"wid"`                       // '工单Id',
	UpcomingName  string `db:"up_name" json:"up_name"`               // '事件名称',
	ClientName    string `db:"client_name" json:"client_name"`       // '客户姓名',
	Mname         string `db:"mname" json:"mname"`                   // '设备名称',
	MaterNum      string `db:"mater_num" json:"mater_num"`           // '物品数量',
	CreateDate    string `db:"create_date" json:"create_date"`       // 创建时间
	Process       int    `db:"process" json:"process"`               // '审核状态',
	State         int    `db:"state" json:"state"`                   // 工单流程状态
	LogNum        string `db:"log_num" json:"log_num"`               // '快递单号',
	UserName      string `db:"user_name" json:"user_name"`           // '销售/业务名'
	IssueDescribe string `db:"issue_describe" json:"issue_describe"` // 问题描述
}

// BusWorkOrderTmp 业务工单查询临时映射结构体
type BusWorkOrderTmp struct {
	WorkId        int     `db:"wid" json:"wid"`                       // 工单号
	MaterNum      int     `db:"mater_num" json:"mater_num"`           // 返厂数量
	IsState       int     `db:"state" json:"state"`                   // 用来渲染时判断的工单状态无需渲染到页面
	PayStatus     string  `db:"pay_status" json:"pay_status"`         // 付款状态
	WorkState     string  `db:"work_state" json:"work_state"`         // 工单状态
	MaterName     string  `db:"mname" json:"mname"`                   // 设备名称
	Model         string  `db:"model" json:"model"`                   // 设备型号
	UserName      string  `db:"user_name" json:"user_name"`           // 业务人员
	ClientName    string  `db:"client_name" json:"client_name"`       // 客户名称
	LogNum        string  `db:"log_num" json:"log_num"`               // 返厂单号
	OutFirmName   *string `db:"out_firm_name" json:"out_firm_name"`   // 外发公司
	IssueDescribe string  `db:"issue_describe" json:"issue_describe"` // 问题描述
	CreateDate    string  `db:"create_date" json:"create_date"`       // 返厂时间
	CrateDate     *string `db:"crate_date" json:"crate_date"`         // 返客时间
	ExpressNum    *string `db:"express_num" json:"express_num"`       // 返回的快递单号
	WorkType      string  `db:"work_type" json:"work_type"`           // 工单类型
	ONumber       *string `db:"o_number" json:"o_number"`             // 外发维修快递单号
	// todo 后续可能需要添加一个维修人员字段
}

// SonDosage 子件用量临时表用结构
type SonDosage struct {
	Dosage        int    `db:"dosage" json:"dosage"`
	MaterTypeName string `db:"mater_type_name" json:"mater_type_name"`
	MaterName     string `db:"mname" json:"mname"`
}

// CodeCheckTmpTable 使用条码查询配置临时表
type CodeCheckTmpTable struct {
	Mid        int     `db:"mid" json:"mid"`
	UserId     int     `db:"u_id" json:"u_id"`
	ClientId   int     `db:"c_id" json:"c_id"`
	BomId      int     `db:"bom_name_id" json:"bom_name_id"`
	MaterName  string  `db:"mname" json:"mname"`
	Brand      string  `db:"brand" json:"brand"`
	Model      string  `db:"model" json:"model"`
	BarCode    string  `db:"barcode" json:"barcode"`
	OutLibDate *string `db:"outlib_date" json:"outlib_date"`
	ProTime    string  `db:"pro_time" json:"pro_time"`
}

// ClientGetWorkDatTmp 客户手机端获取工单临时表
type ClientGetWorkDatTmp struct {
	PayId      int    `db:"pay_id" json:"pay_id"`
	Mid        int    `db:"mid" json:"mid"`
	Amount     int    `db:"amount" json:"amount"`
	Mname      string `db:"mname" json:"mname"`
	WorkType   string `db:"work_type" json:"work_type"`
	CreateDate string `db:"create_date" json:"create_date"`
	LogNum     string `db:"log_num" json:"log_num"`
	PayStat    string `db:"pay_stat" json:"pay_stat"`
	WorkState  string `db:"work_state" json:"work_state"`
}

type ClientGetWorkDatsTmp struct {
	PayId      int     `db:"pay_id" json:"pay_id"`
	Mid        int     `db:"mid" json:"mid"`
	Amount     *int    `db:"amount" json:"amount"`
	Mname      string  `db:"mname" json:"mname"`
	WorkType   string  `db:"work_type" json:"work_type"`
	CreateDate string  `db:"create_date" json:"create_date"`
	LogNum     string  `db:"log_num" json:"log_num"`
	PayStat    *string `db:"pay_stat" json:"pay_stat"`
	WorkState  string  `db:"work_state" json:"work_state"`
}

// WarehouseDatTmp 出货统计临时表
type WarehouseDatTmp struct {
	Mid          int     `db:"mid" json:"mid"`
	Mname        string  `db:"mname" json:"mname"`
	Brand        string  `db:"brand" json:"brand"`
	Model        string  `db:"model" json:"model"`
	BarCode      string  `db:"barcode" json:"barcode"`
	OutlibDate   *string `db:"outlib_date" json:"outlib_date"`
	WarrantyTime *string `db:"warranty_time" json:"warranty_time"`
	LinkOrder    *string `db:"link_order" json:"link_order"`
}

// NewProduct 新品推荐表
type NewProduct struct {
	Nid     int     `db:"nid" json:"nid"`
	ImgUrl  string  `db:"img_url" json:"img_url"`
	JumpUrl string  `db:"jump_url" json:"jump_url"`
	Remark  *string `db:"remark" json:"remark"`
}
