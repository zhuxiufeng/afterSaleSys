module asystem/db

go 1.17

require (
	asystem/cfg v0.0.0-00010101000000-000000000000
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jmoiron/sqlx v1.3.5
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/deckarep/golang-set v1.8.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

replace asystem/cfg => ../cfg
