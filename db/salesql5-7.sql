-- MySQL dump 10.13  Distrib 8.0.29, for Linux (x86_64)
--
-- Host: localhost    Database: saletest
-- ------------------------------------------------------
-- Server version	8.0.29-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bom_name`
--

DROP TABLE IF EXISTS `bom_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `bom_name` (
  `bn_id` int NOT NULL AUTO_INCREMENT,
  `b_name` varchar(64) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'bom名称',
  PRIMARY KEY (`bn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `CityID` int NOT NULL,
  `CityName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ProID` int DEFAULT NULL COMMENT '关联的省份Id',
  PRIMARY KEY (`CityName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `cid` int NOT NULL AUTO_INCREMENT,
  `client_name` varchar(32) COLLATE utf8_general_ci NOT NULL COMMENT '客户姓名',
  `phone` varchar(32) COLLATE utf8_general_ci NOT NULL COMMENT '联系方式',
  `u_id` int DEFAULT NULL,
  PRIMARY KEY (`cid`),
  KEY `client_ibfk_1` (`u_id`),
  CONSTRAINT `client_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='客户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clientAddr`
--

DROP TABLE IF EXISTS `clientAddr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `clientAddr` (
  `addr_id` int NOT NULL AUTO_INCREMENT,
  `addr` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '客户地址',
  `client_id` int DEFAULT '0' COMMENT '关联的客户Id',
  PRIMARY KEY (`addr_id`),
  KEY `client_id` (`client_id`),
  CONSTRAINT `clientAddr_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `code`
--

DROP TABLE IF EXISTS `code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `code` (
  `bid` int NOT NULL AUTO_INCREMENT,
  `barcode` varchar(64) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '条码',
  `c_id` int DEFAULT NULL COMMENT '关联的客户Id',
  `u_id` int DEFAULT NULL COMMENT '关联的用户Id',
  `m_id` int DEFAULT NULL COMMENT '关联的物料Id',
  `bom_name_id` int DEFAULT NULL COMMENT '关联的bom名称Id',
  `ver_num` int DEFAULT NULL COMMENT '验证次数',
  PRIMARY KEY (`bid`),
  KEY `c_id` (`c_id`),
  KEY `u_id` (`u_id`),
  KEY `m_id` (`m_id`),
  KEY `code_FK` (`bom_name_id`),
  CONSTRAINT `code_FK` FOREIGN KEY (`bom_name_id`) REFERENCES `bom_name` (`bn_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `code_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `client` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `code_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `code_ibfk_3` FOREIGN KEY (`m_id`) REFERENCES `mater` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=730 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='二维码记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `code_bom`
--

DROP TABLE IF EXISTS `code_bom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `code_bom` (
  `bbid` int NOT NULL AUTO_INCREMENT,
  `b_id` int DEFAULT NULL COMMENT '条码Id',
  `par_id` int DEFAULT '0' COMMENT '父级条码Id',
  `code_lv` int NOT NULL DEFAULT '0' COMMENT '层级',
  PRIMARY KEY (`bbid`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='条码Bom';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logistics`
--

DROP TABLE IF EXISTS `logistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `logistics` (
  `lid` int NOT NULL AUTO_INCREMENT,
  `lname` varchar(16) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物流名称',
  `butt` varchar(32) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '对接人',
  `phone` varchar(16) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '对接人联系方式',
  PRIMARY KEY (`lid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='物流信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mater`
--

DROP TABLE IF EXISTS `mater`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `mater` (
  `mid` int NOT NULL AUTO_INCREMENT,
  `mname` varchar(64) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物料名称',
  `brand` varchar(16) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物料品牌',
  `model` varchar(64) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规格型号',
  `mater_type_id` int DEFAULT NULL COMMENT '物料类型',
  PRIMARY KEY (`mid`),
  KEY `mater_FK` (`mater_type_id`),
  CONSTRAINT `mater_FK` FOREIGN KEY (`mater_type_id`) REFERENCES `mater_type` (`mt_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='物料信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mater_bom`
--

DROP TABLE IF EXISTS `mater_bom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `mater_bom` (
  `mbid` int NOT NULL AUTO_INCREMENT,
  `m_id` int DEFAULT NULL COMMENT '物料Id',
  `fm_id` int DEFAULT NULL COMMENT '父级物料Id',
  `mater_lv` int DEFAULT NULL COMMENT '层级',
  `dosage` int DEFAULT NULL COMMENT '用量',
  `bom_name_id` int DEFAULT NULL COMMENT '关联的bom名称Id',
  PRIMARY KEY (`mbid`),
  KEY `i_bom_name_id` (`bom_name_id`),
  KEY `i_fm_id` (`fm_id`),
  CONSTRAINT `mater_bom_FK` FOREIGN KEY (`bom_name_id`) REFERENCES `bom_name` (`bn_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='物料BOM关系表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mater_type`
--

DROP TABLE IF EXISTS `mater_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `mater_type` (
  `mt_id` int NOT NULL AUTO_INCREMENT,
  `mater_type_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物料类型名',
  PRIMARY KEY (`mt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `out_firm`
--

DROP TABLE IF EXISTS `out_firm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `out_firm` (
  `oid` int NOT NULL AUTO_INCREMENT,
  `out_firm_name` varchar(32) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '外发公司名称',
  `people` varchar(16) COLLATE utf8_general_ci NOT NULL DEFAULT 'comment 对接人',
  `phone` varchar(16) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系方式',
  `oaddr` varchar(128) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系地址',
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='委外公司信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pay`
--

DROP TABLE IF EXISTS `pay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `pay` (
  `pay_id` int NOT NULL AUTO_INCREMENT,
  `amount` int NOT NULL DEFAULT '0' COMMENT '金额',
  `work_id` int DEFAULT NULL COMMENT '关联的工单Id',
  `sale_id` int DEFAULT NULL COMMENT '关联的销售人员Id',
  `client_id` int DEFAULT NULL COMMENT '关联的客户Id',
  `mater_id` int DEFAULT NULL COMMENT '关联的物料Id',
  `pay_stat` int NOT NULL DEFAULT '0' COMMENT '支付状态,0位未支付，1 为已支付',
  `pay_method` varchar(32) NOT NULL DEFAULT '' COMMENT '支付方式',
  `pay_date` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '支付时间',
  PRIMARY KEY (`pay_id`),
  KEY `work_id` (`work_id`),
  KEY `sale_id` (`sale_id`),
  KEY `client_id` (`client_id`),
  KEY `mater_id` (`mater_id`),
  CONSTRAINT `pay_ibfk_1` FOREIGN KEY (`work_id`) REFERENCES `work` (`wid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pay_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pay_ibfk_3` FOREIGN KEY (`client_id`) REFERENCES `client` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pay_ibfk_4` FOREIGN KEY (`mater_id`) REFERENCES `mater` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paycarousel`
--

DROP TABLE IF EXISTS `paycarousel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `paycarousel` (
  `carousel_id` int NOT NULL AUTO_INCREMENT,
  `link_url` varchar(256) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '轮播图片关联的页面地址',
  `img_url` varchar(256) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片地址',
  PRIMARY KEY (`carousel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `printer`
--

DROP TABLE IF EXISTS `printer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `printer` (
  `pid` int NOT NULL AUTO_INCREMENT,
  `p_name` varchar(32) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '打印机名',
  `p_net_addr` varchar(64) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '打印机网络地址',
  `bt_ser_addr` varchar(128) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'BT条码软件服务器地址',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='打印机信息记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `province`
--

DROP TABLE IF EXISTS `province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `province` (
  `ProID` int NOT NULL,
  `ProName` varchar(50) COLLATE utf8_general_ci NOT NULL COMMENT '省份名称',
  PRIMARY KEY (`ProID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `return_work`
--

DROP TABLE IF EXISTS `return_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `return_work` (
  `rwid` int NOT NULL AUTO_INCREMENT,
  `express_id` int DEFAULT NULL COMMENT '关联的快递公司名称Id',
  `express_num` varchar(64) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '返回快递单号',
  `crate_date` varchar(64) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '返回的时间',
  `work_id` int DEFAULT NULL COMMENT '关联的工单Id',
  `ret_num` int DEFAULT NULL COMMENT '返回数量',
  PRIMARY KEY (`rwid`),
  KEY `work_id` (`work_id`),
  KEY `express_id` (`express_id`),
  CONSTRAINT `return_work_ibfk_1` FOREIGN KEY (`work_id`) REFERENCES `work` (`wid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `return_work_ibfk_2` FOREIGN KEY (`express_id`) REFERENCES `logistics` (`lid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `upcoming`
--

DROP TABLE IF EXISTS `upcoming`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `upcoming` (
  `upid` int NOT NULL AUTO_INCREMENT,
  `up_name` varchar(128) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '待办事项名称',
  `w_id` int DEFAULT NULL COMMENT '关联的工单Id',
  `u_id` int DEFAULT NULL COMMENT '关联的人员Id',
  `m_id` int DEFAULT NULL COMMENT '关联的物料Id',
  `stat_id` int NOT NULL DEFAULT '0' COMMENT '事件状态id，0 为未完成待办，1为已完成',
  PRIMARY KEY (`upid`),
  KEY `w_id` (`w_id`),
  KEY `u_id` (`u_id`),
  KEY `m_id` (`m_id`),
  KEY `i_stat_id` (`stat_id`),
  CONSTRAINT `upcoming_ibfk_1` FOREIGN KEY (`w_id`) REFERENCES `work` (`wid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `upcoming_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `upcoming_ibfk_3` FOREIGN KEY (`m_id`) REFERENCES `mater` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='待办事项信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_type`
--

DROP TABLE IF EXISTS `user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `user_type` (
  `ut_id` int NOT NULL AUTO_INCREMENT,
  `type_name` varchar(32) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户类型名',
  PRIMARY KEY (`ut_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='用户类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `account` varchar(32) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户账户',
  `user_name` varchar(32) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户姓名',
  `passwd` varchar(32) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录密码',
  `t_id` int DEFAULT NULL COMMENT '关联的用户类型表编号',
  PRIMARY KEY (`uid`),
  KEY `t_id` (`t_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`t_id`) REFERENCES `user_type` (`ut_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `work`
--

DROP TABLE IF EXISTS `work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `work` (
  `wid` int NOT NULL AUTO_INCREMENT,
  `process` int NOT NULL DEFAULT '0' COMMENT '流程控制Id,0 待审核，1 审核完成',
  `state` int NOT NULL DEFAULT '0' COMMENT '状态控制Id,0待收货，1已收货，2维修中，3外发中，4待返客，5已完成',
  `m_id` int DEFAULT NULL COMMENT '关联的物料Id',
  `u_id` int DEFAULT NULL COMMENT '关联的业务/销售Id',
  `c_id` int DEFAULT NULL COMMENT '关联的客户Id',
  `l_id` int DEFAULT NULL COMMENT '关联的物流Id',
  `o_id` int DEFAULT NULL COMMENT '关联的外发公司Id',
  `work_type` varchar(16) COLLATE utf8_general_ci DEFAULT '' COMMENT '工单类型，1返修工单2 维修工单3 服务申请工单',
  `issue_describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '问题描述',
  `log_num` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '物流单号',
  `mater_num` int DEFAULT NULL COMMENT '返回的物料数量',
  `create_date` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建时间',
  `wu_id` int DEFAULT NULL COMMENT '关联的维修人员Id',
  `o_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '外发维修物流单号',
  PRIMARY KEY (`wid`),
  KEY `m_id` (`m_id`),
  KEY `u_id` (`u_id`),
  KEY `c_id` (`c_id`),
  KEY `l_id` (`l_id`),
  KEY `o_id` (`o_id`),
  KEY `work_FK` (`wu_id`),
  CONSTRAINT `work_FK` FOREIGN KEY (`wu_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `work_ibfk_1` FOREIGN KEY (`m_id`) REFERENCES `mater` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `work_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `work_ibfk_3` FOREIGN KEY (`c_id`) REFERENCES `client` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `work_ibfk_4` FOREIGN KEY (`l_id`) REFERENCES `logistics` (`lid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `work_ibfk_5` FOREIGN KEY (`o_id`) REFERENCES `out_firm` (`oid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='工单信息表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-11 16:59:17
