package db

import (
	"asystem/cfg"
	"context"
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	"strconv"
	"time"
)

type RedisFunc interface {
	SaveDataToRedis(key, val string) error
	GetRedisData(key string) (string, error)
	DelRedisData(key string) error
}

type RdbObj struct {
	ctx  context.Context
	RDB  *redis.Client
	Time time.Duration
	Err  error
}

var Rdb *RdbObj

// InitRdsObj 初始化 redis 连接
func InitRdsObj() *RdbObj {
	redCfg, err := cfg.LoadCfg(cfg.FILEPATH)
	if err != nil {
		fmt.Println("创建 redis 连接池时读取配置文件错误，错误代码:", err)
		return nil
	}
	return &RdbObj{
		ctx: context.Background(),
		RDB: redis.NewClient(&redis.Options{
			Addr:     redCfg.Redis.Host + ":" + strconv.Itoa(redCfg.Redis.Port),
			Password: redCfg.Redis.Password,
			DB:       redCfg.Redis.Db,
		}),
		Time: time.Duration(redCfg.Redis.Timer),
		Err:  nil,
	}
}

func ConnRedis() error {
	Rdb = InitRdsObj()
	_, err := Rdb.RDB.Ping(Rdb.ctx).Result()
	if err != nil {
		panic(err)
		return err
	}
	return nil
}

// GetRedisData 获取 redis 中的数据
func (r *RdbObj) GetRedisData(key string) (result interface{}, er error) {
	cmd := Rdb.RDB.Get(r.ctx, key)
	if cmd.Err() != nil {
		return "", cmd.Err()
	}
	return cmd.Val(), nil
}

// SaveDataToRedis 保存数据到 redis
func (r *RdbObj) SaveDataToRedis(key string, val interface{}) error {
	if r.Err = Rdb.RDB.Set(r.ctx, key, val, r.Time).Err(); r.Err != nil {
		r.Err = errors.New(fmt.Sprintf("保存数据到 redis 数据库错误，错误代码：%v", r.Err))
		return r.Err
	}
	return nil
}

// DelRedisData 删除 redis 数据
func (r *RdbObj) DelRedisData(key string) error {
	if r.Err = Rdb.RDB.Del(r.ctx, key).Err(); r.Err != nil {
		r.Err = errors.New(fmt.Sprintf("删除 redis 数据库错误，错误代码：%v", r.Err))
		return r.Err
	}
	return nil
}
